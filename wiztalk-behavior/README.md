
 
###  **技术选型** 

- 核心框架：Spring Boot
- 权限框架：Apache Shiro
- 模板引擎：Freemarker
- 持久层框架：MyBatis 和 MyBatis-plus
- 数据库连接池：Alibaba Druid
- 缓存框架：J2cache、Redis
- 日志管理：LogBack
- 工具类：Apache Commons、HuTools
- 视图框架：Spring MVC
- 定时器：Quartz
- 数据库连接池：Druid
- 日志管理：logback
- 页面交互：layui
- 下拉框：bootstrap-select
- 文件上传：Bootstrap File Input
- 热部署 jrebel
- 验证框架 hibernate-validator



### 
### **具有如下特点** 

###  **



<br>

###  **项目结构** 


:fa-minus: freeter-coding<br>
:fa-minus: freeter-admin 后台管理 <br>
:fa-minus: freeter-common 公共模块<br>
:fa-minus: freeter-good 商品模块<br>
:fa-minus: freeter-user 会员模块<br>
:fa-minus: freeter-job  定时任务模块<br>
:fa-minus: freeter-api 移动端接口模块<br>
:fa-minus: freeter-generator 独立代码生成器模块 

<br> 

 

###  **商城模块** 




频道列表： 可以理解为一级分类， 特殊的分类 底下可以不挂分类也可以挂视频，图片等等。

商品分类：有两级分类  商品可以修改二级分类 一级分类可以修改 因为一级分类绑定了规格而二级分类没有。

商品规格： 可以设置通用规格 也可以与一级分类绑定设置规格。商品规格可以重置 可以删除

商品基础功能全部完成。

会员管理

 **软件需求** 
- JDK1.8
- MySQL5.5+
- Tomcat8+
- Maven3.0+

<br>

### 本地部署




1.git下载https://gitee.com/xcOschina/freeter-admin.git项目,完成后导入到ide中 <br>
2.eclipse File import... Maven Existing Projects into Workspace 选择项目的根路径。<br>
3.IDE会下载maven依赖包，自动编译 如果有报错 请update project... jdk环境配置。<br>
4.执行doc/freeter.sql文件，初始化数据【按需导入表结构及数据】<br>
5.最后修改数据库连接参数,配置文件在src/main/resources/application.yml<br>
6.j2cache:
    config-location: /cache/j2cache-no.properties     
    open-spring-cache: true  

 :fa-star: j2cache-no.properties    就是不使用缓存 <br>
 :fa-star: j2cache-redis.properties redis 使用二级缓存<br>
 :fa-star:j2cache-caffeine.properties 使用一级缓存<br>

7.在freeter-coding目录下，执行mvn clean install
<br>
8、如何运行
- Eclipse或IDEA运行AdminApplication.java，则可启动项目【freeter-admin】
- freeter-admin访问路径：http://localhost:8080/freeter-admin/index.html
- 账号密码：admin/admin




<br>
 
 ** **后端项目演示**** 
- 演示地址：<a href="http://47.106.39.83:8080/freeter-admin/login.html"  target="_blank">
47.106.39.83:8080/freeter-admin/login.htm</a>
- 账号密码：admin/admin





<br>
<br>
<br>


**后端模块自动生成的实体类：**

    @TableName("cn_good") 
    @ApiModel(value = "Good")
    public class GoodEntity<T> implements Serializable {

	private static final long serialVersionUID = 1L;


	public GoodEntity() {
		
	}
	
	public GoodEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 商品ID
	 */
	
	@TableId 					
	@ApiModelProperty(value = "商品ID",hidden = true)
	private Integer goodId;
	
	/**
	 * 商品名称
	 */
				
	@NotBlank (message = "商品名称不能为空") 			
	@ApiModelProperty(value = "商品名称")
	private String goodName;
	

**mapper 有外键自动生成多表关联语句：**

`<select id="selectListView"  `

resultType="com.freeter.modules.pc.entity.view.StudentView"`>`

`SELECT  student.* FROM t_student  student `			   
        left join  t_professional  professional on `

 professional.professional_id = student.professional_id 	`	   
        left join t_school  school on  school.school_id = student.school_id   `
      
        <where> 1=1 ${ew.sqlSegment}</where>
	</select>`
	

