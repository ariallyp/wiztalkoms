package com.freeter.modules.wiztalk.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.MPUtil;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.ClientLogEntity;
import com.freeter.modules.wiztalk.entity.ReportDataModel;
import com.freeter.modules.wiztalk.entity.ReportTrackEntity;
import com.freeter.modules.wiztalk.entity.view.ClientLogView;
import com.freeter.modules.wiztalk.service.ClientLogService;
import com.freeter.modules.wiztalk.service.ReportTrackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 
 * 后端接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-04 11:20:41
 */
@RestController
@RequestMapping("wiztalk/clientlog")
@Api(tags="wiztalk接口")
public class ClientLogController {
    @Autowired
    private ClientLogService clientLogService;

    @Autowired
    private ReportTrackService reportTrackService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:clientlog:list")
    public R page(@RequestParam Map<String, Object> params,ClientLogEntity clientLog){

        EntityWrapper< ClientLogEntity> ew = new EntityWrapper< ClientLogEntity>();
      	ew.allEq(MPUtil.allEQMapPre( clientLog, "clientLog")); 
    	PageUtils page = clientLogService.queryPage(params, ew);
    
        return R.ok().put("page", page);
        
    }

	/**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:clientlog:list")
    public R list( ClientLogEntity clientLog){
       	EntityWrapper<  ClientLogEntity> ew = new EntityWrapper<  ClientLogEntity>();
      	ew.allEq(MPUtil.allEQMapPre( clientLog, "clientLog")); 
        return R.ok().put("data",  clientLogService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:clientlog:info")
    public R query(ClientLogEntity clientLog){
        EntityWrapper< ClientLogEntity> ew = new EntityWrapper< ClientLogEntity>();
 		ew.allEq(MPUtil.allEQMapPre( clientLog, "clientLog")); 
		ClientLogView  clientLogView =  clientLogService.selectView(ew);
		return R.ok("查询成功").put("data",  clientLogView);
    }
	
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:clientlog:info")
    public R info(@PathVariable("id") String id){
        ClientLogEntity clientLog = clientLogService.selectById(id);

        return R.ok().put("clientLog", clientLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:clientlog:save")
    public R save(@RequestBody ClientLogEntity clientLog){
    	ValidatorUtils.validateEntity(clientLog);
        clientLogService.insert(clientLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:clientlog:update")
    public R update(@RequestBody ClientLogEntity clientLog){
        ValidatorUtils.validateEntity(clientLog);
        clientLogService.updateAllColumnById(clientLog);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:clientlog:delete")
    public R delete(@RequestBody String[] ids){
        clientLogService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


    /**
     * 保存
     */
    @PostMapping("/save/json")
    @ApiOperation("埋点信息上报")
    public R saveJson(@RequestBody ReportDataModel reportTrack){
        String clientData =reportTrack.getClientData();
        List<ReportTrackEntity> reportTrackEntityList = new ArrayList<ReportTrackEntity>();
        List<ReportTrackEntity> clientList = com.alibaba.fastjson.JSONObject.parseArray(clientData, ReportTrackEntity.class);
        return R.ok();
    }






}
