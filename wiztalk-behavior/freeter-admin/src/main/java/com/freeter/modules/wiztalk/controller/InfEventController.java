package com.freeter.modules.wiztalk.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.*;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.user.entity.UserEntity;
import com.freeter.modules.wiztalk.entity.InfEventEntity;
import com.freeter.modules.wiztalk.entity.view.InfEventView;
import com.freeter.modules.wiztalk.service.InfEventService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * 自定义事件表
 * 后端接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 10:03:18
 */
@RestController
@RequestMapping("wiztalk/infevent")
public class InfEventController {/*
    @Autowired
    private InfEventService infEventService;

    @Autowired
    private RedisUtils redisUtils;




    *//**
     * 列表
     *//*
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,InfEventEntity infEvent){
        EntityWrapper< InfEventEntity> ew = new EntityWrapper< InfEventEntity>();
      	ew.allEq(MPUtil.allEQMapPre( infEvent, "infEvent")); 
    	PageUtils page = infEventService.queryPage(params, ew);
        return R.ok().put("page", page);
        
    }

	*//**
     * 列表
     *//*
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:infevent:list")
    public R list( InfEventEntity infEvent){
       	EntityWrapper<  InfEventEntity> ew = new EntityWrapper<  InfEventEntity>();
      	ew.allEq(MPUtil.allEQMapPre( infEvent, "infEvent")); 
        return R.ok().put("data",  infEventService.selectListView(ew));
    }

	 *//**
     * 查询
     *//*
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:infevent:info")
    public R query(InfEventEntity infEvent){
        EntityWrapper< InfEventEntity> ew = new EntityWrapper< InfEventEntity>();
 		ew.allEq(MPUtil.allEQMapPre( infEvent, "infEvent")); 
		InfEventView  infEventView =  infEventService.selectView(ew);
		return R.ok("查询自定义事件表成功").put("data",  infEventView);
    }
	
    *//**
     * 信息
     *//*
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        InfEventEntity infEvent = infEventService.selectById(id);
        return R.ok().put("infEvent", infEvent);
    }

    *//**
     * 保存
     *//*
    @RequestMapping("/save")
    public R save(@RequestBody InfEventEntity infEvent,@LoginUser UserEntity user){
    	ValidatorUtils.validateEntity(infEvent);
        EntityWrapper<InfEventEntity> ew = new EntityWrapper<InfEventEntity>();
        ew.eq(Constant.STATUS_COLUMN,Constant.INFEVENT_STATUS_ENABLE);
        ew.eq(Constant.APPID_COLUMN,infEvent.getAppId());
        ew.andNew();
        ew.eq(Constant.EVENTNAME_COLUMN,infEvent.getEventName());
        ew.or();
        ew.eq(Constant.ALIAS_COLUMN,infEvent.getAlias());
        List<InfEventEntity> list = infEventService.selectList(ew);
        if(list.isEmpty()){//先判断数据库中是否已有同一款产品下相同的事件，如果有则返回错误提示，否则新增然后刷新到缓存
            Date date=new Date();
            infEvent.setId(UUID.randomUUID().toString());
            infEvent.setCreateTime(date);
            infEvent.setUpdateTime(date);
            infEvent.setCreateUser(user.getUserId());
            infEvent.setStatus(Constant.INFEVENT_STATUS_ENABLE);
            infEventService.insert(infEvent);
            redisUtils.addOrUpdateEvents(infEvent);
            return R.ok();
        }else{
            return R.error("事件已存在,请勿重复添加");
        }
    }

    *//**
     * 修改
     *//*
    @RequestMapping("/update")
    public R update(@RequestBody InfEventEntity infEvent,@LoginUser UserEntity user){
        ValidatorUtils.validateEntity(infEvent);
        EntityWrapper<InfEventEntity> ew = new EntityWrapper<InfEventEntity>();
        ew.eq(Constant.STATUS_COLUMN,Constant.INFEVENT_STATUS_ENABLE);
        ew.ne(Constant.ID_COLUMN,infEvent.getId());
        ew.eq(Constant.APPID_COLUMN,infEvent.getAppId());
        ew.andNew();
        ew.eq(Constant.EVENTNAME_COLUMN,infEvent.getEventName());
        ew.or();
        ew.eq(Constant.ALIAS_COLUMN,infEvent.getAlias());
        List<InfEventEntity> list = infEventService.selectList(ew);
        if(list.isEmpty()){//先判断数据库中是否已有同一款产品下相同的事件，如果有则返回错误提示，否则新增然后刷新到缓存
            Date date=new Date();
            infEvent.setUpdateTime(date);
            infEvent.setUpdateUser(user.getUserId());
            infEvent.setStatus(Constant.INFEVENT_STATUS_ENABLE);
            infEventService.updateAllColumnById(infEvent);
            redisUtils.addOrUpdateEvents(infEvent);
            return R.ok();
        }else{
            return R.error("事件已存在,请勿重复添加");
        }
    }

    *//**
     * 删除
     *//*
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids,@LoginUser UserEntity user){
        EntityWrapper<InfEventEntity> ew = new EntityWrapper<InfEventEntity>();
        ew.in(Constant.APPID_COLUMN,ids);
        List<InfEventEntity> list = infEventService.selectList(ew);
        for(InfEventEntity infEvent:list){
            infEvent.setStatus(Constant.INFEVENT_STATUS_DISABLE);
            infEvent.setUpdateUser(user.getUserId());
            infEventService.updateAllColumnById(infEvent);
        }
        redisUtils.delEvents(list);
        //从缓存中移除
        return R.ok();
    }
*/
}
