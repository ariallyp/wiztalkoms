package com.freeter.modules.wiztalk.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.*;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.user.entity.UserEntity;
import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import com.freeter.modules.wiztalk.entity.view.InfModuleView;
import com.freeter.modules.wiztalk.service.InfModuleService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 * 后端接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-12 18:06:41
 */
@RestController
@RequestMapping("wiztalk/infmodule")
public class InfModuleController {/*
    @Autowired
    private InfModuleService infModuleService;
    @Autowired
    private RedisUtils redisUtils;

    *//**
     * 列表
     *//*
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,InfModuleEntity infModule){
        EntityWrapper< InfModuleEntity> ew = new EntityWrapper< InfModuleEntity>();
        if(StringUtils.isNotBlank(infModule.getAppId())){
            ew.eq(Constant.APPID_COLUMN,infModule.getAppId());
        }
        ew.eq(Constant.STATUS_COLUMN,Constant.INFMODULE_STATUS_ENABLE);
        ew.like(Constant.MODULENAME_COLUMN,infModule.getModuleName());
        ew.like(Constant.ALIAS_COLUMN,infModule.getAlias());
      	//ew.allEq(MPUtil.allEQMapPre( infModule, "infModule"));
    	PageUtils page = infModuleService.queryPage(params, ew);
        return R.ok().put("page", page);
    }

	*//**
     * 列表
     *//*
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:infmodule:list")
    public R list( InfModuleEntity infModule){
       	EntityWrapper<  InfModuleEntity> ew = new EntityWrapper<  InfModuleEntity>();
      	ew.allEq(MPUtil.allEQMapPre( infModule, "infModule")); 
        return R.ok().put("data",  infModuleService.selectListView(ew));
    }

	 *//**
     * 查询
     *//*
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:infmodule:info")
    public R query(InfModuleEntity infModule){
        EntityWrapper< InfModuleEntity> ew = new EntityWrapper< InfModuleEntity>();
 		ew.allEq(MPUtil.allEQMapPre( infModule, "infModule")); 
		InfModuleView  infModuleView =  infModuleService.selectView(ew);
		return R.ok("查询页面表，存储页面汉字名称，英文名称，对应的appId等信息。成功").put("data",  infModuleView);
    }
	
    *//**
     * 信息
     *//*
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        InfModuleEntity infModule = infModuleService.selectById(id);
        return R.ok().put("infModule", infModule);
    }

    *//**
     * 保存
     *//*
    @RequestMapping("/save")
    public R save(@RequestBody InfModuleEntity infModule,@LoginUser UserEntity user){
    	ValidatorUtils.validateEntity(infModule);
        EntityWrapper<InfModuleEntity> ew = new EntityWrapper<InfModuleEntity>();
        ew.eq(Constant.STATUS_COLUMN,Constant.INFMODULE_STATUS_ENABLE);
        ew.eq(Constant.APPID_COLUMN,infModule.getAppId());
        ew.eq(Constant.MODULENAME_COLUMN,infModule.getModuleName());
        //ew.eq(ALIAS_COLUMN,infEvent.getAlias());
        List<InfModuleEntity> list = infModuleService.selectList(ew);
        if(list.isEmpty()){//先判断数据库中是否已有同一款产品下相同的事件，如果有则返回错误提示，否则新增然后刷新到缓存
            Date date=new Date();
            infModule.setId(UUID.randomUUID().toString());
            infModule.setCreateTime(date);
            infModule.setUpdateTime(date);
            infModule.setCreateUser(user.getUserId());
            infModule.setStatus(Constant.INFMODULE_STATUS_ENABLE);
            infModuleService.insert(infModule);
            redisUtils.addOrUpdateModules(infModule);
            return R.ok();
        }else{
            return R.error("该路径已存在,请勿重复添加");
        }
    }

    *//**
     * 修改
     *//*
    @RequestMapping("/update")
    public R update(@RequestBody InfModuleEntity infModule,@LoginUser UserEntity user){
        ValidatorUtils.validateEntity(infModule);
        EntityWrapper<InfModuleEntity> ew = new EntityWrapper<InfModuleEntity>();
        ew.eq(Constant.STATUS_COLUMN,Constant.INFMODULE_STATUS_ENABLE);
        ew.ne(Constant.ID_COLUMN,infModule.getId());
        ew.eq(Constant.APPID_COLUMN,infModule.getAppId());
        ew.andNew();
        ew.eq(Constant.ALIAS_COLUMN,infModule.getAlias());
        ew.or();
        ew.eq(Constant.MODULENAME_COLUMN,infModule.getModuleName());
        List<InfModuleEntity> list = infModuleService.selectList(ew);
        if(list.isEmpty()){//先判断数据库中是否已有同一款产品下相同的事件，如果有则返回错误提示，否则新增然后刷新到缓存
            Date date=new Date();
            infModule.setUpdateTime(date);
            infModule.setUpdateUser(user.getUserId());
            infModule.setStatus(Constant.INFMODULE_STATUS_ENABLE);
            infModuleService.updateAllColumnById(infModule);
            redisUtils.addOrUpdateModules(infModule);
            return R.ok();
        }else{
            return R.error("事件已存在,请勿重复添加");
        }
    }

    *//**
     * 删除
     *//*
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids,@LoginUser UserEntity user){
        EntityWrapper<InfModuleEntity> ew = new EntityWrapper<InfModuleEntity>();
        ew.in(Constant.ID_COLUMN,ids);
        List<InfModuleEntity> list = infModuleService.selectList(ew);
        for(InfModuleEntity infModule:list){
            infModule.setStatus(Constant.INFEVENT_STATUS_DISABLE);
            infModule.setUpdateUser(user.getUserId());
            infModuleService.updateAllColumnById(infModule);
        }
        redisUtils.delModules(list);
        //从缓存中移除
        return R.ok();
    }
*/
}
