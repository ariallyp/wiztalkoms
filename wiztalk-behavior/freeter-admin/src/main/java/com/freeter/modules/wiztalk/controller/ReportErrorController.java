package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.good.entity.ChannelEntity;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.freeter.modules.wiztalk.entity.ReportErrorEntity;
import com.freeter.modules.wiztalk.entity.view.ReportErrorView;

import com.freeter.modules.wiztalk.service.ReportErrorService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.utils.MPUtil;



/**
 * 错误日志表
 * 后端接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 15:35:50
 */
@RestController
@RequestMapping("wiztalk/reporterror")
public class ReportErrorController {
    @Autowired
    private ReportErrorService reportErrorService;


    /**
     * 列表
     */
    @RequestMapping("/getAppVersion")
    @RequiresPermissions("wiztalk:reporterror:list")
    public R getChannelList(){
        ReportErrorEntity reportErrorEntity = new ReportErrorEntity();
        EntityWrapper< ReportErrorEntity> ew = new EntityWrapper< ReportErrorEntity>();
        ew.groupBy("app_version");
       List page = reportErrorService.getAppVersion(ew);

        return R.ok().put("data", page);
    }



    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:reporterror:list")
    public R page(@RequestParam Map<String, Object> params,ReportErrorView reportErrorView){
        String text = reportErrorView.getText();
        String appVersion =reportErrorView.getAppVersion();
        String model =reportErrorView.getModel();
        EntityWrapper< ReportErrorEntity> ew = new EntityWrapper< ReportErrorEntity>();
        if (StringUtils.isNotEmpty(text)){
            ew.like("sub.text", text);
        }
        if (StringUtils.isNotEmpty(appVersion)){
            ew.eq("reportError.app_version", appVersion);
        }
        if (StringUtils.isNotEmpty(model)){
            ew.like("reportError.model", model);
        }
    	PageUtils page = reportErrorService.queryPage(params, ew);

        return R.ok().put("page", page);
        
    }

	/**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:reporterror:list")
    public R list( ReportErrorEntity reportError){
       	EntityWrapper<  ReportErrorEntity> ew = new EntityWrapper<  ReportErrorEntity>();
      	ew.allEq(MPUtil.allEQMapPre( reportError, "reportError")); 
        return R.ok().put("data",  reportErrorService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:reporterror:info")
    public R query(ReportErrorEntity reportError){
        EntityWrapper< ReportErrorEntity> ew = new EntityWrapper< ReportErrorEntity>();
 		ew.allEq(MPUtil.allEQMapPre( reportError, "reportError")); 
		ReportErrorView  reportErrorView =  reportErrorService.selectView(ew);
		return R.ok("查询错误日志表成功").put("data",  reportErrorView);
    }
	
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:reporterror:info")
    public R info(@PathVariable("id") String id){
        ReportErrorEntity reportError = reportErrorService.selectById(id);

        return R.ok().put("reportError", reportError);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:reporterror:save")
    public R save(@RequestBody ReportErrorEntity reportError){
    	ValidatorUtils.validateEntity(reportError);
        reportErrorService.insert(reportError);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:reporterror:update")
    public R update(@RequestBody ReportErrorEntity reportError){
        ValidatorUtils.validateEntity(reportError);
        reportErrorService.updateAllColumnById(reportError);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:reporterror:delete")
    public R delete(@RequestBody String[] ids){
        reportErrorService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
