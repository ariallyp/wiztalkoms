package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.Map;

import com.freeter.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;
import com.freeter.modules.wiztalk.entity.view.ReportErrorSubView;

import com.freeter.modules.wiztalk.service.ReportErrorSubService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.utils.MPUtil;



/**
 * 错误日志详情表
 * 后端接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 15:35:21
 */
@RestController
@RequestMapping("wiztalk/reporterrorsub")
public class ReportErrorSubController {
    @Autowired
    private ReportErrorSubService reportErrorSubService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:reporterrorsub:list")
    public R page(@RequestParam Map<String, Object> params,ReportErrorSubEntity reportErrorSub){
 
        EntityWrapper< ReportErrorSubEntity> ew = new EntityWrapper< ReportErrorSubEntity>();
      	ew.allEq(MPUtil.allEQMapPre( reportErrorSub, "reportErrorSub")); 
    	PageUtils page = reportErrorSubService.queryPage(params, ew);
    
        return R.ok().put("page", page);
        
    }

	/**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:reporterrorsub:list")
    public R list( ReportErrorSubEntity reportErrorSub){
       	EntityWrapper<  ReportErrorSubEntity> ew = new EntityWrapper<  ReportErrorSubEntity>();
      	ew.allEq(MPUtil.allEQMapPre( reportErrorSub, "reportErrorSub")); 
        return R.ok().put("data",  reportErrorSubService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:reporterrorsub:info")
    public R query(ReportErrorSubEntity reportErrorSub){
        EntityWrapper< ReportErrorSubEntity> ew = new EntityWrapper< ReportErrorSubEntity>();
 		ew.allEq(MPUtil.allEQMapPre( reportErrorSub, "reportErrorSub")); 
		ReportErrorSubView  reportErrorSubView =  reportErrorSubService.selectView(ew);
		return R.ok("查询错误日志详情表成功").put("data",  reportErrorSubView);
    }
	
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:reporterrorsub:info")
    public R info(@PathVariable("id") String id){
        ReportErrorSubEntity reportErrorSub = reportErrorSubService.selectById(id);

        return R.ok().put("reportErrorSub", reportErrorSub);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:reporterrorsub:save")
    public R save(@RequestBody ReportErrorSubEntity reportErrorSub){
    	ValidatorUtils.validateEntity(reportErrorSub);
        reportErrorSubService.insert(reportErrorSub);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:reporterrorsub:update")
    public R update(@RequestBody ReportErrorSubEntity reportErrorSub){
        ValidatorUtils.validateEntity(reportErrorSub);
        reportErrorSubService.updateAllColumnById(reportErrorSub);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:reporterrorsub:delete")
    public R delete(@RequestBody String[] ids){
        reportErrorSubService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
