package com.freeter.modules.wiztalk.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.MPUtil;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.ReportTrackEntity;
import com.freeter.modules.wiztalk.entity.model.ReportTrackModel;
import com.freeter.modules.wiztalk.entity.vo.ReportTrackVO;
import com.freeter.modules.wiztalk.service.ReportTrackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;




/**
 * 埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-12 18:57:20
 */
@RestController
@RequestMapping("reporttrack")
@Api(tags="埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。接口")
public class ReportTrackController {
    @Autowired
    private ReportTrackService reportTrackService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    @ApiOperation("分页查询埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。")
    public R page(@RequestParam Map<String, Object> params,ReportTrackModel reportTrackModel){
 
        EntityWrapper< ReportTrackEntity> ew = new EntityWrapper< ReportTrackEntity>();
        ReportTrackEntity reportTrack = new  ReportTrackEntity( reportTrackModel);
     	ew.allEq(MPUtil.allEQMapPre( reportTrack, "reportTrack")); 
    	PageUtils page = reportTrackService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    @ApiOperation("查询埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。")
    public R list(ReportTrackModel reportTrackModel){
		ValidatorUtils.validateEntity(reportTrackModel);
        EntityWrapper< ReportTrackEntity> ew = new EntityWrapper< ReportTrackEntity>();
		ReportTrackEntity reportTrack = new  ReportTrackEntity( reportTrackModel);
     	ew.allEq(MPUtil.allEQMapPre( reportTrack, "reportTrack")); 
		List<ReportTrackVO>  reportTrackVOList =  reportTrackService.selectListVO(ew);
		return R.ok("查询埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。成功").put("data", reportTrackVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    @ApiOperation("查询埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。")
    public R query(ReportTrackModel reportTrackModel){
		ValidatorUtils.validateEntity(reportTrackModel);
        EntityWrapper< ReportTrackEntity> ew = new EntityWrapper< ReportTrackEntity>();
		ReportTrackEntity reportTrack = new  ReportTrackEntity( reportTrackModel);
		ew.allEq(MPUtil.allEQMapPre( reportTrack, "reportTrack"));
		ReportTrackVO  reportTrackVO =  reportTrackService.selectVO(ew);
		return R.ok("查询埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。成功").put("data",  reportTrackVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    @ApiOperation("获取相应的埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。")
    public R info(@PathVariable("id") String id){
        ReportTrackEntity reportTrack = reportTrackService.selectById(id);
        return R.ok().put("reportTrack", reportTrack);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    @ApiOperation("添加埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。数据")
    public R saveJson(@RequestBody ReportTrackEntity reportTrack){
    	ValidatorUtils.validateEntity(reportTrack);
        reportTrackService.insert(reportTrack);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    @ApiOperation("添加埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。数据 （参数：表单格式）")
    public R saveForm(ReportTrackEntity reportTrack){
    	ValidatorUtils.validateEntity(reportTrack);
        reportTrackService.insert(reportTrack);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    @ApiOperation("修改埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。数据（参数：json格式）")
    public R updateJson(@RequestBody ReportTrackEntity reportTrack){
        ValidatorUtils.validateEntity(reportTrack);
        reportTrackService.updateAllColumnById(reportTrack);//全部更新

        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    @ApiOperation("修改埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。数据（参数：表单格式）")
    public R updateForm(ReportTrackEntity reportTrack){
        ValidatorUtils.validateEntity(reportTrack);
        reportTrackService.updateAllColumnById(reportTrack);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation("删除埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。数据")
    public R delete(@RequestBody String[] ids){
        reportTrackService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }


    /**
     * 删除
     */
    @PostMapping("/import")
    @ApiOperation("删除埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。数据")
    public R importModules(){
        reportTrackService.importModules();
        return R.ok();
    }


    /**
     * 获取当前的页面的统计情况
     * @param appId   产品Id
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getModuleStatNow",method = RequestMethod.POST)
    @ApiOperation("访问路径当前的统计情况，只包含前十 参数(appId)")
    public R getModuleStatNow(@RequestParam(name = "appId") String  appId) throws Exception {
        return R.ok().put("data",reportTrackService.getModuleStatNow(appId));
    }


    /**
     * 获取昨天和今天的自定义事件的统计数量
     * @param params 前台传递的参数
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getEventStatTwoDay")
    @ApiOperation("自定义事件昨天和今天的统计情况 参数(appId,appVersion,alias,page,limit,order,sidx)")
    public R getEventStatTowDay(@RequestParam Map<String,Object> params) throws Exception {
        return R.ok().put("data",reportTrackService.getEventStatTwoDay( params));
    }


    /**
     *查询自定义事件统计的详细情况
     * @param params 前台传递的参数
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getEventStatDetail",method = RequestMethod.POST)
    @ApiOperation("自定义事件统计详细 参数(appId,appVersion,alias)")
    public R getEventStatDetail(@RequestParam Map<String,Object> params) throws Exception {
        return R.ok().put("data",reportTrackService.getEventStatDetail(params));
    }


}
