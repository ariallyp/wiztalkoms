package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;

import com.freeter.common.validator.ValidatorUtils;
import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.style.ItemStyle;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.JQPageInfo;
import com.freeter.common.mpextend.parser.ParseWrapper;
import com.freeter.common.utils.PageInfo;


import com.freeter.modules.wiztalk.entity.StatisDeviceEntity;
import com.freeter.modules.wiztalk.entity.view.StatisDeviceView;
import com.freeter.modules.wiztalk.entity.model.StatisDeviceModel;

import com.freeter.modules.wiztalk.service.StatisDeviceService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.utils.MPUtil;



/**
 * 设备终端
 * 后端接口
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-24 17:18:28
 */
@RestController
@RequestMapping("wiztalk/statisdevice")
@SuppressWarnings({"unchecked","rawtypes"})
public class StatisDeviceController {
    @Autowired
    private StatisDeviceService statisDeviceService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:statisdevice:list")
    public PageUtils page(JQPageInfo jqPageInfo,StatisDeviceModel statisDevice){
        EntityWrapper< StatisDeviceEntity> ew = ParseWrapper.parseWrapper(statisDevice);
     	PageInfo pageInfo = new PageInfo(jqPageInfo);
     	PageUtils page = statisDeviceService.queryPage(pageInfo, ew);
    
        return page;
        
    }

	/**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:statisdevice:list")
    public R list( StatisDeviceModel statisDevice){
       	EntityWrapper< StatisDeviceEntity> ew = ParseWrapper.parseWrapper(statisDevice);
        return R.ok().put("data",  statisDeviceService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:statisdevice:info")
    public R query(StatisDeviceEntity statisDevice){
        EntityWrapper< StatisDeviceEntity> ew = new EntityWrapper< StatisDeviceEntity>();
 		ew.allEq(MPUtil.allEQMapPre( statisDevice, "statisDevice")); 
		StatisDeviceView  statisDeviceView =  statisDeviceService.selectView(ew);
		return R.ok("查询设备终端成功").put("data",  statisDeviceView);
    }
	
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:statisdevice:info")
    public R info(@PathVariable("id") Integer id){
        StatisDeviceEntity statisDevice = statisDeviceService.selectById(id);

        return R.ok().put("statisDevice", statisDevice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:statisdevice:save")
    public R save(@RequestBody StatisDeviceEntity statisDevice){
    	ValidatorUtils.validateEntity(statisDevice);
        statisDeviceService.insert(statisDevice);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:statisdevice:update")
    public R update(@RequestBody StatisDeviceEntity statisDevice){
        ValidatorUtils.validateEntity(statisDevice);
        statisDeviceService.updateAllColumnById(statisDevice);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:statisdevice:delete")
    public R delete(@RequestBody Integer[] ids){
        statisDeviceService.deleteBatchIds(Arrays.asList(ids));
        ItemStyle dataStyle = new ItemStyle();
        Option option = new GsonOption();
        return R.ok();
    }

}
