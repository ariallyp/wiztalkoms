package com.freeter.modules.wiztalk.controller;

import java.util.*;
import java.util.stream.Collectors;

import com.freeter.common.utils.MapConvertList;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.InfAppEntity;
import com.freeter.modules.wiztalk.service.InfAppService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.freeter.modules.wiztalk.entity.StatisErrorEntity;
import com.freeter.modules.wiztalk.entity.view.StatisErrorView;

import com.freeter.modules.wiztalk.service.StatisErrorService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.utils.MPUtil;


/**
 * 错误统计指标存放的表
 * 后端接口
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-12 10:50:56
 */
@RestController
@RequestMapping("wiztalk/statiserror")
public class StatisErrorController {
    @Autowired
    private StatisErrorService statisErrorService;
    @Autowired
    private InfAppService infAppService;

    /**
     * 列表
     */
    @RequestMapping("/getStaticData")
    @RequiresPermissions("wiztalk:statiserror:list")
    public R getStaticData(@RequestParam Map<String, Object> params, StatisErrorEntity statisError) {

        Object beginTime = params.get("startDate");
        Object endTime = params.get("endDate").toString();
        EntityWrapper<StatisErrorEntity> ew = new EntityWrapper<StatisErrorEntity>();
        EntityWrapper<InfAppEntity> ewInfoApp = new EntityWrapper<InfAppEntity>();
        Object appVersion =params.get("appVersion");;
        ew.groupBy("error.app_id");
        if (StringUtils.isNotEmpty(beginTime.toString()) && StringUtils.isNotEmpty(endTime.toString())) {
            ew.between("error.time", beginTime, endTime);
        } else if (StringUtils.isNotEmpty(beginTime.toString())) {
            ew.gt("error.time", beginTime);
        } else if (StringUtils.isNotEmpty(endTime.toString())) {
            ew.lt("error.time", beginTime);
        }
        if (StringUtils.isNotEmpty(appVersion.toString())){
            ew.eq("error.app_version",appVersion);
        }
        Map<String, Object> map2 = new HashMap<String, Object>();
        Map mapList = new HashMap();
        List errorList = statisErrorService.getErrorNum(ew);
        List errorNums = new ArrayList();
        List appName = new ArrayList();

        for (Object ob : errorList) {
            Map<String, String> mapLists = (Map<String, String>) ob;
            for (String s : mapLists.keySet()) {
                //   System.out.print("key:"+s+"\t");
                Object obb = mapLists.get(s);
                if (s.equalsIgnoreCase("value")) {
                    Integer.parseInt(obb.toString());
                    // System.out.println("value:" + obb.toString());
                    errorNums.add(Integer.parseInt(obb.toString()));
                } else {
                    appName.add(obb);
                }
            }

        }
        /*List<InfAppEntity> applist = infAppService.selectList(ewInfoApp);
        List<String> appName=applist.stream().map(InfAppEntity::getAppName).collect(Collectors.toList());*/
        map2.put("appNumbers", errorNums);
        map2.put("appNames", appName);
        map2.put("appList",errorList);
        return R.ok().put("data", map2);
    }

    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:statiserror:list")
    public R page(@RequestParam Map<String, Object> params, StatisErrorEntity statisError) {

        EntityWrapper<StatisErrorEntity> ew = new EntityWrapper<StatisErrorEntity>();
        ew.allEq(MPUtil.allEQMapPre(statisError, "statisError"));
        PageUtils page = statisErrorService.queryPage(params, ew);

        return R.ok().put("page", page);

    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:statiserror:list")
    public R list(StatisErrorEntity statisError) {
        EntityWrapper<StatisErrorEntity> ew = new EntityWrapper<StatisErrorEntity>();
        ew.allEq(MPUtil.allEQMapPre(statisError, "statisError"));
        return R.ok().put("data", statisErrorService.selectListView(ew));
    }

    /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:statiserror:info")
    public R query(StatisErrorEntity statisError) {
        EntityWrapper<StatisErrorEntity> ew = new EntityWrapper<StatisErrorEntity>();
        ew.allEq(MPUtil.allEQMapPre(statisError, "statisError"));
        StatisErrorView statisErrorView = statisErrorService.selectView(ew);
        return R.ok("查询错误统计指标存放的表成功").put("data", statisErrorView);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:statiserror:info")
    public R info(@PathVariable("id") Integer id) {
        StatisErrorEntity statisError = statisErrorService.selectById(id);

        return R.ok().put("statisError", statisError);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:statiserror:save")
    public R save(@RequestBody StatisErrorEntity statisError) {
        ValidatorUtils.validateEntity(statisError);
        statisErrorService.insert(statisError);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:statiserror:update")
    public R update(@RequestBody StatisErrorEntity statisError) {
        ValidatorUtils.validateEntity(statisError);
        statisErrorService.updateAllColumnById(statisError);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:statiserror:delete")
    public R delete(@RequestBody Integer[] ids) {
        statisErrorService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
