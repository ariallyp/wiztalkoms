package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.Map;

import com.freeter.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.freeter.modules.wiztalk.entity.StatisEventEntity;
import com.freeter.modules.wiztalk.entity.view.StatisEventView;

import com.freeter.modules.wiztalk.service.StatisEventService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.utils.MPUtil;



/**
 * 自定义事件的统计结果表
 * 后端接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 11:48:50
 */
@RestController
@RequestMapping("wiztalk/statisevent")
public class StatisEventController {
    @Autowired
    private StatisEventService statisEventService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:statisevent:list")
    public R page(@RequestParam Map<String, Object> params,StatisEventEntity statisEvent){
 
        EntityWrapper< StatisEventEntity> ew = new EntityWrapper< StatisEventEntity>();
      	ew.allEq(MPUtil.allEQMapPre( statisEvent, "statisEvent")); 
    	PageUtils page = statisEventService.queryPage(params, ew);
    
        return R.ok().put("page", page);
        
    }

	/**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:statisevent:list")
    public R list( StatisEventEntity statisEvent){
       	EntityWrapper<  StatisEventEntity> ew = new EntityWrapper<  StatisEventEntity>();
      	ew.allEq(MPUtil.allEQMapPre( statisEvent, "statisEvent")); 
        return R.ok().put("data",  statisEventService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:statisevent:info")
    public R query(StatisEventEntity statisEvent){
        EntityWrapper< StatisEventEntity> ew = new EntityWrapper< StatisEventEntity>();
 		ew.allEq(MPUtil.allEQMapPre( statisEvent, "statisEvent")); 
		StatisEventView  statisEventView =  statisEventService.selectView(ew);
		return R.ok("查询自定义事件的统计结果表成功").put("data",  statisEventView);
    }
	
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:statisevent:info")
    public R info(@PathVariable("id") Integer id){
        StatisEventEntity statisEvent = statisEventService.selectById(id);

        return R.ok().put("statisEvent", statisEvent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:statisevent:save")
    public R save(@RequestBody StatisEventEntity statisEvent){
    	ValidatorUtils.validateEntity(statisEvent);
        statisEventService.insert(statisEvent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:statisevent:update")
    public R update(@RequestBody StatisEventEntity statisEvent){
        ValidatorUtils.validateEntity(statisEvent);
        statisEventService.updateAllColumnById(statisEvent);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:statisevent:delete")
    public R delete(@RequestBody Integer[] ids){
        statisEventService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
