package com.freeter.modules.wiztalk.controller;

import java.util.*;
import java.util.stream.Collectors;

import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.InfAppEntity;
import com.freeter.modules.wiztalk.entity.StatisErrorEntity;
import com.freeter.modules.wiztalk.entity.StatisUserEntity;
import com.freeter.modules.wiztalk.entity.model.StatisUserModel;
import com.freeter.modules.wiztalk.service.InfAppService;
import com.freeter.modules.wiztalk.service.StatisUserService;
import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.code.Orient;
import com.github.abel533.echarts.code.Tool;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Pie;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.JQPageInfo;
import com.freeter.common.mpextend.parser.ParseWrapper;
import com.freeter.common.utils.PageInfo;


import com.freeter.modules.wiztalk.entity.StatisStartEntity;
import com.freeter.modules.wiztalk.entity.view.StatisStartView;
import com.freeter.modules.wiztalk.entity.model.StatisStartModel;

import com.freeter.modules.wiztalk.service.StatisStartService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.utils.MPUtil;


/**
 * 项目概况
 * 后端接口
 *
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-25 10:25:38
 */
@RestController
@RequestMapping("wiztalk/statisstart")
@SuppressWarnings({"unchecked", "rawtypes"})
public class StatisStartController {
    @Autowired
    private StatisStartService statisStartService;

    @Autowired
    private StatisUserService statisUserService;
    @Autowired
    private InfAppService infAppService;






    /**
     * 查询
     */
    @RequestMapping("/getAppVersion")
    @RequiresPermissions("wiztalk:statisstart:info")
    public R getAppVersion( ) {
        EntityWrapper<StatisStartEntity> ew = new EntityWrapper<StatisStartEntity>();
        ew.groupBy("app_version");
        List list = statisStartService.selectAppVersion(ew);
        return R.ok("查询项目概况成功").put("data", list);
    }



    /**
     * 列表
     */
    @RequestMapping("/getStaticTrendData")
    @RequiresPermissions("wiztalk:statisstart:list")
    public R getStaticTrendData(@RequestParam Map<String, Object> params, StatisStartEntity statisStartEntity) {
        Object beginTime = params.get("startDate");
        Object endTime = params.get("endDate").toString();
        EntityWrapper<StatisStartEntity> ew = new EntityWrapper<StatisStartEntity>();

        Object appVersion = params.get("appVersion");

        ew.groupBy("start.app_version");
        if (StringUtils.isNotEmpty(beginTime.toString()) && StringUtils.isNotEmpty(endTime.toString())) {
            ew.between("start.time", beginTime, endTime);
        } else if (StringUtils.isNotEmpty(beginTime.toString())) {
            ew.gt("start.time", beginTime);
        } else if (StringUtils.isNotEmpty(endTime.toString())) {
            ew.lt("start.time", beginTime);
        }
        if (StringUtils.isNotEmpty(appVersion.toString())) {
            ew.eq("start.app_version", appVersion);
        }
        Map<String, Object> map2 = new HashMap<String, Object>();
        List startList = statisStartService.selectTrendData(ew);
        List errorNums = new ArrayList();
        List appName = new ArrayList();

        for (Object ob : startList) {
            Map<String, String> mapLists = (Map<String, String>) ob;
            for (String s : mapLists.keySet()) {
                //   System.out.print("key:"+s+"\t");
                Object obb = mapLists.get(s);
                if (s.equalsIgnoreCase("value")) {
                    Integer.parseInt(obb.toString());
                    // System.out.println("value:" + obb.toString());
                    errorNums.add(Integer.parseInt(obb.toString()));
                } else {
                    appName.add(obb);
                }
            }

        }
        /*List<InfAppEntity> applist = infAppService.selectList(ewInfoApp);
        List<String> appName=applist.stream().map(InfAppEntity::getAppName).collect(Collectors.toList());*/
        map2.put("appNumbers", errorNums);
        map2.put("appNames", appName);
        map2.put("appList", startList);
        return R.ok().put("data", map2);


    }


    /**
     * 列表
     */
    @RequestMapping("/getAppListData")
    @RequiresPermissions("wiztalk:statisstart:list")
    public R getAppListData() {
        EntityWrapper<InfAppEntity> ewInfoApp = new EntityWrapper<InfAppEntity>();
        List<InfAppEntity> applist = infAppService.selectList(ewInfoApp);
        List<String> appName=applist.stream().map(InfAppEntity::getAppName).collect(Collectors.toList());
        return R.ok("查询项目概况成功").put("data", applist);




    }

    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:statisstart:list")
    public PageUtils page(JQPageInfo jqPageInfo, StatisStartModel statisStart) {
        EntityWrapper<StatisStartEntity> ew = ParseWrapper.parseWrapper(statisStart);
        PageInfo pageInfo = new PageInfo(jqPageInfo);
        PageUtils page = statisStartService.queryPage(pageInfo, ew);

        return page;

    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:statisstart:list")
    public R list(StatisStartModel statisStart) {
        EntityWrapper<StatisStartEntity> ew = ParseWrapper.parseWrapper(statisStart);
        //需要的数据
        String title = "搜索引擎统计";
        String[] searchs = {"百度", "必应", "豆瓣", "搜狗"};
        int[] datas = {123, 456, 789, 555};
        //创建option对象
        Option option = new GsonOption();
        //设置标题  二级标题  标题位置
        option.title().text(title).subtext("二级标题").x("center");
        //设置工具栏 展示  能标记
        option.toolbox().show(true).feature(Tool.mark);
        //设置显示工具格式
        option.tooltip().show(true).formatter("{a}</br>{b}：{c}/个");
        //设置图例  图例位置  图例对齐方式 竖列对齐
        option.legend().data(searchs).x("left").orient(Orient.vertical);
        //填充数据
        Pie pie = new Pie();//创建饼图对象

        //设置饼图的标题 半径、位置
        pie.name(title).radius("55%").center("50%", "50%");
        //填充数据
        for (int i = 0; i < searchs.length; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("value", datas[i]);//填充饼图数据
            map.put("name", searchs[i]);//填充饼图数据对应的搜索引擎
            pie.data(map);
        }
        option.series(pie); //设置数据

        return R.ok().put("data", option);
    }

    /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:statisstart:info")
    public R query(StatisStartEntity statisStart) {
        EntityWrapper<StatisStartEntity> ew = new EntityWrapper<StatisStartEntity>();
        ew.allEq(MPUtil.allEQMapPre(statisStart, "statisStart"));
        StatisStartView statisStartView = statisStartService.selectView(ew);
        return R.ok("查询项目概况成功").put("data", statisStartView);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:statisstart:info")
    public R info(@PathVariable("id") Integer id) {
        StatisStartEntity statisStart = statisStartService.selectById(id);

        return R.ok().put("statisStart", statisStart);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:statisstart:save")
    public R save(@RequestBody StatisStartEntity statisStart) {
        ValidatorUtils.validateEntity(statisStart);
        statisStartService.insert(statisStart);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:statisstart:update")
    public R update(@RequestBody StatisStartEntity statisStart) {
        ValidatorUtils.validateEntity(statisStart);
        statisStartService.updateAllColumnById(statisStart);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:statisstart:delete")
    public R delete(@RequestBody Integer[] ids) {
        statisStartService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
