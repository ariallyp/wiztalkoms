package com.freeter.modules.wiztalk.controller;

import java.util.*;
import java.util.stream.Collectors;

import com.freeter.common.utils.*;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.InfAppEntity;
import com.freeter.modules.wiztalk.entity.StatisErrorEntity;
import com.freeter.modules.wiztalk.entity.StatisStartEntity;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.mpextend.parser.ParseWrapper;


import com.freeter.modules.wiztalk.entity.StatisUserEntity;
import com.freeter.modules.wiztalk.entity.view.StatisUserView;
import com.freeter.modules.wiztalk.entity.model.StatisUserModel;

import com.freeter.modules.wiztalk.service.StatisUserService;


/**
 * 关于用户的统计结果的表
 * 后端接口
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-11-01 16:25:26
 */
@RestController
@RequestMapping("wiztalk/statisuser")
@SuppressWarnings({"unchecked","rawtypes"})
public class StatisUserController {
    @Autowired
    private StatisUserService statisUserService;


    /**
     * 列表
     */
    @RequestMapping("/getTrendDayData")
    public R getTrendDayData(@RequestParam Map<String, Object> params, StatisUserModel statisUser) {
        EntityWrapper< StatisUserEntity> ew = new EntityWrapper<StatisUserEntity>();
        Map<String, Object> map = new HashMap<String, Object>();
        Object beginTime = params.get("startDate");
        Object endTime = params.get("endDate").toString();
        Object appVersion = params.get("appVersion");
        if (StringUtils.isNotEmpty(beginTime.toString()) && StringUtils.isNotEmpty(endTime.toString())) {
            ew.between("time", beginTime, endTime);
        } else if (StringUtils.isNotEmpty(beginTime.toString())) {
            ew.gt("time", beginTime);
        } else if (StringUtils.isNotEmpty(endTime.toString())) {
            ew.lt("time", beginTime);
        }else {
            ew.where("TO_DAYS(NOW())-TO_DAYS(time) <= 30 ");
        }
        if (StringUtils.isNotEmpty(appVersion.toString())) {
            ew.eq("app_id", appVersion);
        }
        ew.groupBy("DATE_FORMAT(TIME, '%Y%m%d')");
        System.out.println(ew.getSqlSegment());
        List<StatisUserView> list = statisUserService.selectTrendDayData(ew);
        List<String> dateList =  new ArrayList<>();
        List<Integer> addList= new ArrayList<>();
        List<Integer> userList= new ArrayList<>();
        List<Integer> startList= new ArrayList<>();
        for (StatisUserView statisUserView : list) {
            String currentHour =statisUserView.getCurrentMonth();
            dateList.add(currentHour);
            Integer userAdd=statisUserView.getUserAdd();
            addList.add(userAdd);
            Integer userUser=statisUserView.getUserUsed();
            userList.add(userUser);
            Integer userStart=statisUserView.getUserStart();
            startList.add(userStart);
        }
        map.put("dateList",dateList);
        map.put("userAdd",addList);
        map.put("userUser",userList);
        map.put("userStart",startList);
        return R.ok().put("data", map);
    }


    /**
     * 列表
     */
    @RequestMapping("/getAllStaticHourData")
    @RequiresPermissions("wiztalk:statisuser:list")
    public R getAllStaticHourData(@RequestParam Map<String, Object> params, StatisUserModel statisUser) {
        EntityWrapper< StatisUserEntity> ew = ParseWrapper.parseWrapper(statisUser);
        Map<String, Object> map = new HashMap<String, Object>();
        List<StatisUserView> listToday = statisUserService.selectCurrentDayHourData(ew);
        List<StatisUserView> listYesterday = statisUserService.selectYesterDayHourData(ew);
        List<StatisUserView> listWeek = statisUserService.selectWeekHourData(ew);
        List<StatisUserView> listMonth = statisUserService.selectMonthHourData(ew);
        List<String> dateList = Arrays.asList("0;00", "1:00", "2:00", "3:00", "4:00"
                , "5:00", "6:00", "7:00", "8:00", "9:00", "10:00",
                "11:00", "12:00", "13:00", "14:00", "15:00", "16:00",
                "17:00", "18:00", "19:00", "20:00", "21:00", "22:00","23:00");

        List<Integer> addListToday= new ArrayList<>(24);
        List<Integer> addListYesterday= new ArrayList<>();
        List<Integer> addListWeek= new ArrayList<>();
        List<Integer> addListMonth= new ArrayList<>();

        List<Integer> userList= new ArrayList<>(24);
        List<Integer> userListYesterday= new ArrayList<>();
        List<Integer> userListWeek= new ArrayList<>();
        List<Integer> userListMonth= new ArrayList<>();

        List<Integer> startList= new ArrayList<>(24);
        List<Integer> startListYesterday= new ArrayList<>();
        List<Integer> startListWeek= new ArrayList<>();
        List<Integer> startListMonth= new ArrayList<>();

        for (StatisUserView statisUserView : listToday) {

            Integer userAdd=statisUserView.getUserAdd();
            addListToday.add(userAdd);
            Integer userUser=statisUserView.getUserUsed();
            userList.add(userUser);
            Integer userStart=statisUserView.getUserStart();
            startList.add(userStart);
        }
        for (StatisUserView statisUserView : listYesterday) {

            Integer userAdd=statisUserView.getUserAdd();
            addListYesterday.add(userAdd);
            Integer userUser=statisUserView.getUserUsed();
            userListYesterday.add(userUser);
            Integer userStart=statisUserView.getUserStart();
            startListYesterday.add(userStart);
        }
        for (StatisUserView statisUserView : listWeek) {

            Integer userAdd=statisUserView.getUserAdd();
            addListWeek.add(userAdd);
            Integer userUser=statisUserView.getUserUsed();
            userListWeek.add(userUser);
            Integer userStart=statisUserView.getUserStart();
            startListWeek.add(userStart);
        }
        for (StatisUserView statisUserView : listMonth) {

            Integer userAdd=statisUserView.getUserAdd();
            addListMonth.add(userAdd);
            Integer userUser=statisUserView.getUserUsed();
            userListMonth.add(userUser);
            Integer userStart=statisUserView.getUserStart();
            startListMonth.add(userStart);
        }
        map.put("dateList",dateList);
        map.put("userAdd",addListToday);
        map.put("userAddYesterday",addListYesterday);
        map.put("userAddWeek",addListWeek);
        map.put("userAddMonth",addListMonth);

        map.put("userUser",userList);
        map.put("userListYesterday",userListYesterday);
        map.put("userListWeek",userListWeek);
        map.put("userListMonth",userListMonth);

        map.put("userStart",startList);
        map.put("startListYesterday",startListYesterday);
        map.put("startListWeek",startListWeek);
        map.put("startListMonth",startListMonth);
        return R.ok().put("data", map);
    }

    /**
     * 列表
     */
    @RequestMapping("/getStaticHourData")
    @RequiresPermissions("wiztalk:statisuser:list")
    public R getStaticHourData(@RequestParam Map<String, Object> params, StatisUserModel statisUser) {
        EntityWrapper< StatisUserEntity> ew = ParseWrapper.parseWrapper(statisUser);
        Map<String, Object> map = new HashMap<String, Object>();
        List<StatisUserView> list = statisUserService.selectHourData(ew);
        List<String> dateList =  new ArrayList<>();
        List<Integer> addList= new ArrayList<>();
        List<Integer> userList= new ArrayList<>();
        List<Integer> startList= new ArrayList<>();
        for (StatisUserView statisUserView : list) {
            String currentHour =statisUserView.getCurrentHour();
            dateList.add(currentHour);
            Integer userAdd=statisUserView.getUserAdd();
            addList.add(userAdd);
            Integer userUser=statisUserView.getUserUsed();
            userList.add(userUser);
            Integer userStart=statisUserView.getUserStart();
            startList.add(userStart);
        }
        map.put("dateList",dateList);
        map.put("userAdd",addList);
        map.put("userUser",userList);
        map.put("userStart",startList);
        return R.ok().put("data", map);
    }

    /**
     * 列表
     */
    @RequestMapping("/getStaticData")
    @RequiresPermissions("wiztalk:statisuser:list")
    public R getStaticData(@RequestParam Map<String, Object> params, StatisUserModel statisUser) {
        EntityWrapper< StatisUserEntity> ew = ParseWrapper.parseWrapper(statisUser);
        Map<String, Object> map = new HashMap<String, Object>();
        List<StatisUserView> list = statisUserService.selectLatYearData(ew);
        List<String> dateList =  new ArrayList<>();
        List<Integer> addList= new ArrayList<>();
        List<Integer> userList= new ArrayList<>();
        List<Integer> startList= new ArrayList<>();
        for (StatisUserView statisUserView : list) {
                String currentMonth =statisUserView.getCurrentMonth();
                dateList.add(currentMonth);
                Integer userAdd=statisUserView.getUserAdd();
                addList.add(userAdd);
                Integer userUser=statisUserView.getUserUsed();
                userList.add(userUser);
                Integer userStart=statisUserView.getUserStart();
                startList.add(userStart);
        }
        map.put("dateList",dateList);
        map.put("userAdd",addList);
        map.put("userUser",userList);
        map.put("userStart",startList);
        return R.ok().put("data", map);
    }

    /**
     * 列表
     */
    @RequestMapping("/getTotalData")
    @RequiresPermissions("wiztalk:statisuser:list")
    public R getTotalData( StatisUserModel statisUser){
        EntityWrapper< StatisUserEntity> ew = ParseWrapper.parseWrapper(statisUser);
        return R.ok().put("data",  statisUserService.selectAllTotalData(ew));
    }
    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:statisuser:list")
    public PageUtils page(JQPageInfo jqPageInfo,StatisUserModel statisUser){
        EntityWrapper< StatisUserEntity> ew = ParseWrapper.parseWrapper(statisUser);
     	PageInfo pageInfo = new PageInfo(jqPageInfo);
     	/*String startMonth=DateUtilsNew.getOneYearDate();
     	String endMonth= DateUtilsNew.getCurrentYearMonth();
     	ew.groupBy("time");
     	ew.between("time",startMonth,endMonth);*/
        List<StatisUserView> list=statisUserService.selectAllTotalData(ew);
     	PageUtils page = new PageUtils(list,1,1,1);

    
        return page;
        
    }

	/**
     * 列表
     */
    @RequestMapping("/list")
    public R list( StatisUserModel statisUser){
       	EntityWrapper< StatisUserEntity> ew = ParseWrapper.parseWrapper(statisUser);
        return R.ok().put("data",  statisUserService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:statisuser:info")
    public R query(StatisUserEntity statisUser){
        EntityWrapper< StatisUserEntity> ew = new EntityWrapper< StatisUserEntity>();
 		ew.allEq(MPUtil.allEQMapPre( statisUser, "statisUser")); 
		StatisUserView  statisUserView =  statisUserService.selectView(ew);
		return R.ok("查询关于用户的统计结果的表成功").put("data",  statisUserView);
    }
	
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("wiztalk:statisuser:info")
    public R info(@PathVariable("id") Integer id){
        StatisUserEntity statisUser = statisUserService.selectById(id);

        return R.ok().put("statisUser", statisUser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:statisuser:save")
    public R save(@RequestBody StatisUserEntity statisUser){
    	ValidatorUtils.validateEntity(statisUser);
        statisUserService.insert(statisUser);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:statisuser:update")
    public R update(@RequestBody StatisUserEntity statisUser){
        ValidatorUtils.validateEntity(statisUser);
        statisUserService.updateAllColumnById(statisUser);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:statisuser:delete")
    public R delete(@RequestBody Integer[] ids){
        statisUserService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
