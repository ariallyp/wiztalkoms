package com.freeter.modules.wiztalk.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.MPUtil;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.TenantEntity;
import com.freeter.modules.wiztalk.entity.view.TenantView;
import com.freeter.modules.wiztalk.service.TenantService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * d
 * 后端接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-03 17:47:58
 */
@RestController
@RequestMapping("wiztalk/tenant")
public class TenantController {
    @Autowired
    private TenantService tenantService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    @RequiresPermissions("wiztalk:tenant:list")
    public R page(@RequestParam Map<String, Object> params,TenantEntity tenant){
 
        EntityWrapper< TenantEntity> ew = new EntityWrapper< TenantEntity>();
      	ew.allEq(MPUtil.allEQMapPre( tenant, "tenant"));
    	PageUtils page = tenantService.queryPage(params, ew);
        return R.ok().put("page", page);
        
    }

	/**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("wiztalk:tenant:list")
    public R list( TenantEntity tenant){
       	EntityWrapper<  TenantEntity> ew = new EntityWrapper<  TenantEntity>();
      	ew.allEq(MPUtil.allEQMapPre( tenant, "tenant")); 
        return R.ok().put("data",  tenantService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("wiztalk:tenant:info")
    public R query(TenantEntity tenant){
        EntityWrapper< TenantEntity> ew = new EntityWrapper< TenantEntity>();
 		ew.allEq(MPUtil.allEQMapPre( tenant, "tenant")); 
		TenantView  tenantView =  tenantService.selectView(ew);
		return R.ok("查询成功").put("data",  tenantView);
    }
	
    /**
     * 信息
     */
    @RequestMapping("/info/{tenantId}")
    @RequiresPermissions("wiztalk:tenant:info")
    public R info(@PathVariable("tenantId") String tenantId){
        TenantEntity tenant = tenantService.selectById(tenantId);

        return R.ok().put("tenant", tenant);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("wiztalk:tenant:save")
    public R save(@RequestBody TenantEntity tenant){
    	ValidatorUtils.validateEntity(tenant);
        tenantService.insert(tenant);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("wiztalk:tenant:update")
    public R update(@RequestBody TenantEntity tenant){
        ValidatorUtils.validateEntity(tenant);
        tenantService.updateAllColumnById(tenant);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("wiztalk:tenant:delete")
    public R delete(@RequestBody String[] tenantIds){
        tenantService.deleteBatchIds(Arrays.asList(tenantIds));

        return R.ok();
    }

}
