package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.ClientLogEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.ClientLogVO;
import com.freeter.modules.wiztalk.entity.view.ClientLogView;


/**
 * 
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-04 11:20:41
 */
public interface ClientLogDao extends BaseMapper<ClientLogEntity> {
	
	List<ClientLogVO> selectListVO(@Param("ew") Wrapper<ClientLogEntity> wrapper);
	
	ClientLogVO selectVO(@Param("ew") Wrapper<ClientLogEntity> wrapper);
	
	
	List<ClientLogView> selectListView(@Param("ew") Wrapper<ClientLogEntity> wrapper);

	List<ClientLogView> selectListView(Pagination page,@Param("ew") Wrapper<ClientLogEntity> wrapper);
	
	ClientLogView selectView(@Param("ew") Wrapper<ClientLogEntity> wrapper);
}
