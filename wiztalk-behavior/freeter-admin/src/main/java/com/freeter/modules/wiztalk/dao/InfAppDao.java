package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.InfAppEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.InfAppVO;
import com.freeter.modules.wiztalk.entity.view.InfAppView;


/**
 * 产品表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-17 17:57:05
 */
public interface InfAppDao extends BaseMapper<InfAppEntity> {
	
	List<InfAppVO> selectListVO(@Param("ew") Wrapper<InfAppEntity> wrapper);
	
	InfAppVO selectVO(@Param("ew") Wrapper<InfAppEntity> wrapper);
	
	
	List<InfAppView> selectListView(@Param("ew") Wrapper<InfAppEntity> wrapper);

	List<InfAppView> selectListView(Pagination page,@Param("ew") Wrapper<InfAppEntity> wrapper);
	
	InfAppView selectView(@Param("ew") Wrapper<InfAppEntity> wrapper);
}
