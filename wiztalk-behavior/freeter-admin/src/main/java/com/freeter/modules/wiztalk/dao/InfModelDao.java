package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.InfModelEntity;
import com.freeter.modules.wiztalk.entity.view.InfModelView;
import com.freeter.modules.wiztalk.entity.vo.InfModelVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 手机表，存储手机的品牌、型号、对应的appId。
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-04 21:02:57
 */
public interface InfModelDao extends BaseMapper<InfModelEntity> {
	
	List<InfModelVO> selectListVO(@Param("ew") Wrapper<InfModelEntity> wrapper);
	
	InfModelVO selectVO(@Param("ew") Wrapper<InfModelEntity> wrapper);
	
	
	List<InfModelView> selectListView(@Param("ew") Wrapper<InfModelEntity> wrapper);

	List<InfModelView> selectListView(Pagination page, @Param("ew") Wrapper<InfModelEntity> wrapper);
	
	InfModelView selectView(@Param("ew") Wrapper<InfModelEntity> wrapper);


    List<Map<String,Object>> getModelStat(Pagination page, @Param("params")Map<String,Object> params);
}
