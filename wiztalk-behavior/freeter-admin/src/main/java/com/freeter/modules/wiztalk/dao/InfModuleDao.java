package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import com.freeter.modules.wiztalk.entity.view.InfModuleView;
import com.freeter.modules.wiztalk.entity.vo.InfModuleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-12 18:06:41
 */
public interface InfModuleDao extends BaseMapper<InfModuleEntity> {
	
	List<InfModuleVO> selectListVO(@Param("ew") Wrapper<InfModuleEntity> wrapper);
	
	InfModuleVO selectVO(@Param("ew") Wrapper<InfModuleEntity> wrapper);
	
	
	List<InfModuleView> selectListView(@Param("ew") Wrapper<InfModuleEntity> wrapper);

	List<InfModuleView> selectListView(Pagination page,@Param("ew") Wrapper<InfModuleEntity> wrapper);
	
	InfModuleView selectView(@Param("ew") Wrapper<InfModuleEntity> wrapper);

	int insertModule(InfModuleEntity infModuleEntity);

	List<InfModuleEntity> selectEntityByConditon(@Param("params")Map<String,Object> params);
}
