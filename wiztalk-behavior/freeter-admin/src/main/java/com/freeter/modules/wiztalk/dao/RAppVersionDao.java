package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.RAppVersionEntity;
import com.freeter.modules.wiztalk.entity.view.RAppVersionView;
import com.freeter.modules.wiztalk.entity.vo.RAppVersionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * app与小版本之间的关系表。
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-18 19:45:24
 */
public interface RAppVersionDao extends BaseMapper<RAppVersionEntity> {
	
	List<RAppVersionVO> selectListVO(@Param("ew") Wrapper<RAppVersionEntity> wrapper);
	
	RAppVersionVO selectVO(@Param("ew") Wrapper<RAppVersionEntity> wrapper);
	
	
	List<RAppVersionView> selectListView(@Param("ew") Wrapper<RAppVersionEntity> wrapper);

	List<RAppVersionView> selectListView(Pagination page, @Param("ew") Wrapper<RAppVersionEntity> wrapper);
	
	RAppVersionView selectView(@Param("ew") Wrapper<RAppVersionEntity> wrapper);
}
