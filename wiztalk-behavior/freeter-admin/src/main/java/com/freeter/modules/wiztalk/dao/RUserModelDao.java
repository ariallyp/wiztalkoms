package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.RUserModelEntity;
import com.freeter.modules.wiztalk.entity.view.RUserModelView;
import com.freeter.modules.wiztalk.entity.vo.RUserModelVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 用户与手机机型的关系，用户统计各个机型下的用户分布情况。
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 10:43:38
 */
public interface RUserModelDao extends BaseMapper<RUserModelEntity> {
	
	List<RUserModelVO> selectListVO(@Param("ew") Wrapper<RUserModelEntity> wrapper);
	
	RUserModelVO selectVO(@Param("ew") Wrapper<RUserModelEntity> wrapper);
	
	
	List<RUserModelView> selectListView(@Param("ew") Wrapper<RUserModelEntity> wrapper);

	List<RUserModelView> selectListView(Pagination page, @Param("ew") Wrapper<RUserModelEntity> wrapper);
	
	RUserModelView selectView(@Param("ew") Wrapper<RUserModelEntity> wrapper);
}
