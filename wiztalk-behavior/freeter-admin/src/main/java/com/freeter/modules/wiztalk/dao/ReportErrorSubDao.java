package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorSubVO;
import com.freeter.modules.wiztalk.entity.view.ReportErrorSubView;


/**
 * 错误日志详情表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 15:35:21
 */
public interface ReportErrorSubDao extends BaseMapper<ReportErrorSubEntity> {
	
	List<ReportErrorSubVO> selectListVO(@Param("ew") Wrapper<ReportErrorSubEntity> wrapper);
	
	ReportErrorSubVO selectVO(@Param("ew") Wrapper<ReportErrorSubEntity> wrapper);
	
	
	List<ReportErrorSubView> selectListView(@Param("ew") Wrapper<ReportErrorSubEntity> wrapper);

	List<ReportErrorSubView> selectListView(Pagination page,@Param("ew") Wrapper<ReportErrorSubEntity> wrapper);
	
	ReportErrorSubView selectView(@Param("ew") Wrapper<ReportErrorSubEntity> wrapper);
}
