package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.ReportStartEntity;
import com.freeter.modules.wiztalk.entity.view.ReportStartView;
import com.freeter.modules.wiztalk.entity.vo.ReportStartVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 启动start表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 17:44:22
 */
public interface ReportStartDao extends BaseMapper<ReportStartEntity> {
	
	List<ReportStartVO> selectListVO(@Param("ew") Wrapper<ReportStartEntity> wrapper);
	
	ReportStartVO selectVO(@Param("ew") Wrapper<ReportStartEntity> wrapper);
	
	
	List<ReportStartView> selectListView(@Param("ew") Wrapper<ReportStartEntity> wrapper);

	List<ReportStartView> selectListView(Pagination page, @Param("ew") Wrapper<ReportStartEntity> wrapper);
	
	ReportStartView selectView(@Param("ew") Wrapper<ReportStartEntity> wrapper);
}
