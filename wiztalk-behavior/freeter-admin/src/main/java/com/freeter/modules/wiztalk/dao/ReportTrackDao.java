package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import com.freeter.modules.wiztalk.entity.ReportTrackEntity;
import com.freeter.modules.wiztalk.entity.view.ReportTrackView;
import com.freeter.modules.wiztalk.entity.vo.ReportTrackVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-07 14:14:07
 */
public interface ReportTrackDao extends BaseMapper<ReportTrackEntity> {
	
	List<ReportTrackVO> selectListVO(@Param("ew") Wrapper<ReportTrackEntity> wrapper);
	
	ReportTrackVO selectVO(@Param("ew") Wrapper<ReportTrackEntity> wrapper);
	
	
	List<ReportTrackView> selectListView(@Param("ew") Wrapper<ReportTrackEntity> wrapper);

	List<ReportTrackView> selectListView(Pagination page, @Param("ew") Wrapper<ReportTrackEntity> wrapper);
	
	ReportTrackView selectView(@Param("ew") Wrapper<ReportTrackEntity> wrapper);

	void batchInsert(@Param("entityList")List<ReportTrackEntity> entityList,@Param("tableName")String tableName);

    void batchDelete(@Param("entityList")List<ReportTrackEntity> entityList,@Param("tableName")String tableName);

	List<ReportTrackEntity> selectListEntity(@Param("params")Map<String,Object> params, @Param("tableName")
            String tableName);

	List<Map<String,Object>> statModuleByDay(@Param("day") String day);

    List<Map<String,Object>> statEventByDay(@Param("day") String day);

    List<Map<String,Object>> getModuleStatNow(@Param("appId") String appId);

    List<Map<String,Object>> selectNewModules();

    void coverModuleId(@Param("module")InfModuleEntity module);

    List<Map<String,Object>> getEventStatTwoDay(Pagination page,@Param("params")Map<String,Object> params);

    List<Map<String,Object>> getEventStatDetail(@Param("params")Map<String,Object> params);
}
