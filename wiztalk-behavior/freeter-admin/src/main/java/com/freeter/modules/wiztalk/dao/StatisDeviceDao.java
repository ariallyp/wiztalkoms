package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.StatisDeviceEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.StatisDeviceVO;
import com.freeter.modules.wiztalk.entity.view.StatisDeviceView;


/**
 * 设备终端
 * 
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-24 17:18:28
 */
 @SuppressWarnings({"unchecked","rawtypes"})
public interface StatisDeviceDao extends BaseMapper<StatisDeviceEntity> {
	
	List<StatisDeviceVO> selectListVO(@Param("ew") Wrapper<StatisDeviceEntity> wrapper);
	
	StatisDeviceVO selectVO(@Param("ew") Wrapper<StatisDeviceEntity> wrapper);
	
	
	List<StatisDeviceView> selectListView(@Param("ew") Wrapper<StatisDeviceEntity> wrapper);

	List<StatisDeviceView> selectListView(Pagination page,@Param("ew") Wrapper<StatisDeviceEntity> wrapper);
	
	StatisDeviceView selectView(@Param("ew") Wrapper<StatisDeviceEntity> wrapper);
}
