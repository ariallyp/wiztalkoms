package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.StatisErrorEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.StatisErrorVO;
import com.freeter.modules.wiztalk.entity.view.StatisErrorView;


/**
 * 错误统计指标存放的表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-12 10:50:56
 */
public interface StatisErrorDao extends BaseMapper<StatisErrorEntity> {
	
	List<StatisErrorVO> selectListVO(@Param("ew") Wrapper<StatisErrorEntity> wrapper);
	
	StatisErrorVO selectVO(@Param("ew") Wrapper<StatisErrorEntity> wrapper);
	
	List<StatisErrorView> selectListView(@Param("ew") Wrapper<StatisErrorEntity> wrapper);

	List<StatisErrorView> selectListView(Pagination page,@Param("ew") Wrapper<StatisErrorEntity> wrapper);
	
	StatisErrorView selectView(@Param("ew") Wrapper<StatisErrorEntity> wrapper);

	List getErrorNum (@Param("ew") Wrapper<StatisErrorEntity> wrapper);
}
