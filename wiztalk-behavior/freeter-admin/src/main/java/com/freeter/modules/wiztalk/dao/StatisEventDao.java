package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.StatisEventEntity;
import com.freeter.modules.wiztalk.entity.view.StatisEventView;
import com.freeter.modules.wiztalk.entity.vo.StatisEventVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 自定义事件的统计结果表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 11:48:50
 */
public interface StatisEventDao extends BaseMapper<StatisEventEntity> {
	
	List<StatisEventVO> selectListVO(@Param("ew") Wrapper<StatisEventEntity> wrapper);
	
	StatisEventVO selectVO(@Param("ew") Wrapper<StatisEventEntity> wrapper);
	
	
	List<StatisEventView> selectListView(@Param("ew") Wrapper<StatisEventEntity> wrapper);

	List<StatisEventView> selectListView(Pagination page,@Param("ew") Wrapper<StatisEventEntity> wrapper);
	
	StatisEventView selectView(@Param("ew") Wrapper<StatisEventEntity> wrapper);

    List<Map<String,Object>> getStatByCondition(@Param("params")Map<String,Object> params);

    void batchInsert(@Param("entityList")List<Map<String,Object>> entityList);
}
