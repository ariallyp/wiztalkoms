package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.StatisModuleEntity;
import com.freeter.modules.wiztalk.entity.view.StatisModuleView;
import com.freeter.modules.wiztalk.entity.vo.StatisModuleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 页面访问量统计结果表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 19:11:13
 */
public interface StatisModuleDao extends BaseMapper<StatisModuleEntity> {
	
	List<StatisModuleVO> selectListVO(@Param("ew") Wrapper<StatisModuleEntity> wrapper);
	
	StatisModuleVO selectVO(@Param("ew") Wrapper<StatisModuleEntity> wrapper);
	
	
	List<StatisModuleView> selectListView(@Param("ew") Wrapper<StatisModuleEntity> wrapper);

	List<StatisModuleView> selectListView(Pagination page, @Param("ew") Wrapper<StatisModuleEntity> wrapper);
	
	StatisModuleView selectView(@Param("ew") Wrapper<StatisModuleEntity> wrapper);

	void batchInsert(@Param("entityList")List<Map<String,Object>> entityList);


}
