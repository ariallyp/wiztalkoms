package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.StatisStartEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.StatisStartVO;
import com.freeter.modules.wiztalk.entity.view.StatisStartView;


/**
 * 项目概况
 * 
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-25 10:25:38
 */
 @SuppressWarnings({"unchecked","rawtypes"})
public interface StatisStartDao extends BaseMapper<StatisStartEntity> {
	
	List<StatisStartVO> selectListVO(@Param("ew") Wrapper<StatisStartEntity> wrapper);
	
	StatisStartVO selectVO(@Param("ew") Wrapper<StatisStartEntity> wrapper);
	
	
	List<StatisStartView> selectListView(@Param("ew") Wrapper<StatisStartEntity> wrapper);

	List<StatisStartView> selectListView(Pagination page,@Param("ew") Wrapper<StatisStartEntity> wrapper);
	
	StatisStartView selectView(@Param("ew") Wrapper<StatisStartEntity> wrapper);

	List selectTrendData(@Param("ew") Wrapper<StatisStartEntity> wrapper);

	List<StatisStartView> selectAppVersion(@Param("ew") Wrapper<StatisStartEntity> wrapper);

}
