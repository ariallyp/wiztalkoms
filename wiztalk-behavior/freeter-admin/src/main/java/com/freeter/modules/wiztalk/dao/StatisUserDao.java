package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.StatisUserEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.StatisUserVO;
import com.freeter.modules.wiztalk.entity.view.StatisUserView;


/**
 * 关于用户的统计结果的表
 * 
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-11-01 16:25:26
 */
 @SuppressWarnings({"unchecked","rawtypes"})
public interface StatisUserDao extends BaseMapper<StatisUserEntity> {
	
	List<StatisUserVO> selectListVO(@Param("ew") Wrapper<StatisUserEntity> wrapper);
	
	StatisUserVO selectVO(@Param("ew") Wrapper<StatisUserEntity> wrapper);
	
	
	List<StatisUserView> selectListView(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectListView(Pagination page,@Param("ew") Wrapper<StatisUserEntity> wrapper);
	
	StatisUserView selectView(@Param("ew") Wrapper<StatisUserEntity> wrapper);


	List<StatisUserView> selectLatYearData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectAllTotalData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);


	List<StatisUserView> selectCurrentDayHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectYesterDayHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectWeekHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectMonthHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectTrendDayData(@Param("ew") Wrapper<StatisUserEntity> wrapper);


}
