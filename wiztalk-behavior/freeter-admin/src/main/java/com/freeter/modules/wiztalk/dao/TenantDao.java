package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.TenantEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.TenantVO;
import com.freeter.modules.wiztalk.entity.view.TenantView;


/**
 * 
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-03 17:47:58
 */
public interface TenantDao extends BaseMapper<TenantEntity> {
	
	List<TenantVO> selectListVO(@Param("ew") Wrapper<TenantEntity> wrapper);
	
	TenantVO selectVO(@Param("ew") Wrapper<TenantEntity> wrapper);
	
	
	List<TenantView> selectListView(@Param("ew") Wrapper<TenantEntity> wrapper);

	List<TenantView> selectListView(Pagination page,@Param("ew") Wrapper<TenantEntity> wrapper);
	
	TenantView selectView(@Param("ew") Wrapper<TenantEntity> wrapper);
}
