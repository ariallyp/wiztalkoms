package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;



/**
 * 自定义事件表
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-26 09:55:23
 */
@TableName("wztk_inf_event")
@ApiModel(value = "InfEvent")
public class InfEventEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public InfEventEntity() {
		
	}
	
	public InfEventEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 事件id
	 */
	
	@TableId 					
	@ApiModelProperty(value = "事件id",hidden = true)
	private String id;
	
	/**
	 * 事件名称
	 */
						
	@ApiModelProperty(value = "事件名称")
	private String eventName;
	
	/**
	 * 事件别名
	 */
						
	@ApiModelProperty(value = "事件别名")
	private String alias;
	
	/**
	 * 产品编号
	 */
						
	@ApiModelProperty(value = "产品编号")
	private String appId;
	
	/**
	 * 创建时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 			
	@TableField(fill = FieldFill.INSERT) 
	@ApiModelProperty(value = "创建时间",hidden = true)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	@TableField(fill = FieldFill.UPDATE) 	
	@ApiModelProperty(value = "修改时间",hidden = true)
	private Date updateTime;
	
	/**
	 * 状态   0停用  1启用
	 */
						
	@ApiModelProperty(value = "状态   0停用  1启用")
	private String status;
	
	/**
	 * 创建者
	 */
						
	@ApiModelProperty(value = "创建者")
	private Long createUser;
	
	/**
	 * 修改者
	 */
						
	@ApiModelProperty(value = "修改者")
	private Long updateUser;
	
	/**
	 * 设置：事件id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：事件id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：事件名称
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * 获取：事件名称
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * 设置：事件别名
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * 获取：事件别名
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * 设置：产品编号
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：状态   0停用  1启用
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态   0停用  1启用
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建者
	 */
	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}
	/**
	 * 获取：创建者
	 */
	public Long getCreateUser() {
		return createUser;
	}
	/**
	 * 设置：修改者
	 */
	public void setUpdateUser(Long updateUser) {
		this.updateUser = updateUser;
	}
	/**
	 * 获取：修改者
	 */
	public Long getUpdateUser() {
		return updateUser;
	}
}
