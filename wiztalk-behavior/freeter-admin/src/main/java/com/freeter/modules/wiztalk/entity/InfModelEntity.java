package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;



/**
 * 手机表，存储手机的品牌、型号、对应的appId。
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 21:20:38
 */
@TableName("wztk_inf_model")
@ApiModel(value = "InfModel")
public class InfModelEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public InfModelEntity() {
		
	}
	
	public InfModelEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 机型ID
	 */
	
	@TableId 					
	@ApiModelProperty(value = "机型ID",hidden = true)
	private String id;
	
	/**
	 * 机型名称
	 */
						
	@ApiModelProperty(value = "机型名称")
	private String model;
	
	/**
	 * 手机品牌
	 */
						
	@ApiModelProperty(value = "手机品牌")
	private String vendor;
	
	/**
	 * 系统
	 */
						
	@ApiModelProperty(value = "系统")
	private String os;
	
	/**
	 * 启动次数
	 */
						
	@ApiModelProperty(value = "启动次数")
	private Long startTimes;
	
	/**
	 * 产品编号
	 */
						
	@ApiModelProperty(value = "产品编号")
	private String appId;
	
	/**
	 * 创建时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 			
	@TableField(fill = FieldFill.INSERT) 
	@ApiModelProperty(value = "创建时间",hidden = true)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	@TableField(fill = FieldFill.UPDATE) 	
	@ApiModelProperty(value = "修改时间",hidden = true)
	private Date updateTime;
	
	/**
	 * 修改人
	 */
						
	@ApiModelProperty(value = "修改人")
	private Long updateUser;
	
	/**
	 * 机型状态 0表示停用  1表示启用
	 */
						
	@ApiModelProperty(value = "机型状态 0表示停用  1表示启用")
	private String status;
	
	/**
	 * 设置：机型ID
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：机型ID
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：机型名称
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * 获取：机型名称
	 */
	public String getModel() {
		return model;
	}
	/**
	 * 设置：手机品牌
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	/**
	 * 获取：手机品牌
	 */
	public String getVendor() {
		return vendor;
	}
	/**
	 * 设置：系统
	 */
	public void setOs(String os) {
		this.os = os;
	}
	/**
	 * 获取：系统
	 */
	public String getOs() {
		return os;
	}
	/**
	 * 设置：启动次数
	 */
	public void setStartTimes(Long startTimes) {
		this.startTimes = startTimes;
	}
	/**
	 * 获取：启动次数
	 */
	public Long getStartTimes() {
		return startTimes;
	}
	/**
	 * 设置：产品编号
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：修改人
	 */
	public void setUpdateUser(Long updateUser) {
		this.updateUser = updateUser;
	}
	/**
	 * 获取：修改人
	 */
	public Long getUpdateUser() {
		return updateUser;
	}
	/**
	 * 设置：机型状态 0表示停用  1表示启用
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：机型状态 0表示停用  1表示启用
	 */
	public String getStatus() {
		return status;
	}
}
