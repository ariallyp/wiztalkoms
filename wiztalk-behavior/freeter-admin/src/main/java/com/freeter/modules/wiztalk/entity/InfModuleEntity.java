package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;



/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-29 17:21:49
 */
@TableName("wztk_inf_module")
@ApiModel(value = "InfModule")
public class InfModuleEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public InfModuleEntity() {
		
	}
	
	public InfModuleEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	
	@TableId 					
	@ApiModelProperty(value = "",hidden = true)
	private String id;
	
	/**
	 * 页面对应的类名
	 */
						
	@ApiModelProperty(value = "页面对应的类名")
	private String moduleName;
	
	/**
	 * 页面的中文名称
	 */
						
	@ApiModelProperty(value = "页面的中文名称")
	private String alias;
	
	/**
	 * 产品id
	 */
						
	@ApiModelProperty(value = "产品id")
	private String appId;
	
	/**
	 * 创建时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 			
	@TableField(fill = FieldFill.INSERT) 
	@ApiModelProperty(value = "创建时间",hidden = true)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	@TableField(fill = FieldFill.UPDATE) 	
	@ApiModelProperty(value = "修改时间",hidden = true)
	private Date updateTime;
	
	/**
	 * 创建人
	 */
						
	@ApiModelProperty(value = "创建人")
	private Long createUser;
	
	/**
	 * 修改人
	 */
						
	@ApiModelProperty(value = "修改人")
	private Long updateUser;
	
	/**
	 * 路径状态  0表示停用 1表示启用
	 */
						
	@ApiModelProperty(value = "路径状态  0表示停用 1表示启用")
	private String status;
	
	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：页面对应的类名
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	/**
	 * 获取：页面对应的类名
	 */
	public String getModuleName() {
		return moduleName;
	}
	/**
	 * 设置：页面的中文名称
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * 获取：页面的中文名称
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * 设置：产品id
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}
	/**
	 * 获取：创建人
	 */
	public Long getCreateUser() {
		return createUser;
	}
	/**
	 * 设置：修改人
	 */
	public void setUpdateUser(Long updateUser) {
		this.updateUser = updateUser;
	}
	/**
	 * 获取：修改人
	 */
	public Long getUpdateUser() {
		return updateUser;
	}
	/**
	 * 设置：路径状态  0表示停用 1表示启用
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：路径状态  0表示停用 1表示启用
	 */
	public String getStatus() {
		return status;
	}
}
