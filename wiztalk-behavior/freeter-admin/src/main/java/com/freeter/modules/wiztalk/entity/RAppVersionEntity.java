package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;



/**
 * app与小版本之间的关系表。
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-18 19:45:24
 */
@TableName("wztk_r_app_version")
@ApiModel(value = "RAppVersion")
public class RAppVersionEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public RAppVersionEntity() {
		
	}
	
	public RAppVersionEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 产品版本号
	 */
	
	@TableId 					
	@ApiModelProperty(value = "产品版本号",hidden = true)
	private String appVersion;
	
	/**
	 * 产品id
	 */
				
	@NotBlank (message = "产品id不能为空") 			
	@ApiModelProperty(value = "产品id")
	private String appId;
	
	/**
	 * 入库时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 			
	@TableField(fill = FieldFill.INSERT) 
	@ApiModelProperty(value = "入库时间",hidden = true)
	private Date createTime;
	
	/**
	 * 设置：产品版本号
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
	/**
	 * 设置：产品id
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：入库时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：入库时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
