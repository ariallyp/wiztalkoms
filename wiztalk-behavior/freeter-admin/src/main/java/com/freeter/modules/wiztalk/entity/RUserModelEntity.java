package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;



/**
 * 用户与手机机型的关系，用户统计各个机型下的用户分布情况。
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 10:43:38
 */
@TableName("wztk_r_user_model")
@ApiModel(value = "RUserModel")
public class RUserModelEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public RUserModelEntity() {
		
	}
	
	public RUserModelEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 用户ID
	 */
	
	@TableId 					
	@ApiModelProperty(value = "用户ID",hidden = true)
	private String uid;
	
	/**
	 * 机型id
	 */
				
	@NotBlank (message = "机型id不能为空") 			
	@ApiModelProperty(value = "机型id")
	private String modelId;
	
	/**
	 * 设置：用户ID
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}
	/**
	 * 获取：用户ID
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * 设置：机型id
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	/**
	 * 获取：机型id
	 */
	public String getModelId() {
		return modelId;
	}
}
