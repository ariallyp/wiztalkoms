package com.freeter.modules.wiztalk.entity;

public class ReportDataModel {
    public String clientData;

    public String getClientData() {
        return clientData;
    }

    public void setClientData(String clientData) {
        this.clientData = clientData;
    }
}
