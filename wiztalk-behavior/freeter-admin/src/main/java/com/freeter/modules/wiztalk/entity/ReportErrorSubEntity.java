package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;



/**
 * 错误日志详情表
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 15:35:21
 */
@TableName("wztk_report_error_sub")
@ApiModel(value = "ReportErrorSub")
public class ReportErrorSubEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public ReportErrorSubEntity() {
		
	}
	
	public ReportErrorSubEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * id
	 */
	
	@TableId 					
	@ApiModelProperty(value = "id",hidden = true)
	private String id;
	
	/**
	 * 错误内容
	 */
						
	@ApiModelProperty(value = "错误内容")
	private String text;
	
	/**
	 * 设置：id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：错误内容
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * 获取：错误内容
	 */
	public String getText() {
		return text;
	}
}
