package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;


/**
 * 登录上报信息实时表，只存放昨天和今天两天的数据，零点的时候清除前天的数据，并移存到对应的历史表中。
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-04 20:38:14
 */
@TableName("wztk_report_login")
@ApiModel(value = "ReportLogin")
public class ReportLoginEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public ReportLoginEntity() {
		
	}
	
	public ReportLoginEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * id
	 */
	
	@TableId 					
	@ApiModelProperty(value = "id",hidden = true)
	private String id;
	
	/**
	 * 手机型号
	 */
						
	@ApiModelProperty(value = "手机型号")
	private String model;
	
	/**
	 * 与手机相关的参数
	 */
						
	@ApiModelProperty(value = "与手机相关的参数")
	private String mac;
	
	/**
	 * 与手机相关的参数
	 */
						
	@ApiModelProperty(value = "与手机相关的参数")
	private String imsi;
	
	/**
	 * 与手机相关的参数
	 */
						
	@ApiModelProperty(value = "与手机相关的参数")
	private String imei;
	
	/**
	 * 设备名称
	 */
						
	@ApiModelProperty(value = "设备名称")
	private String deviceName;
	
	/**
	 * 设备ID
	 */
						
	@ApiModelProperty(value = "设备ID")
	private String modelId;
	
	/**
	 * 手机系统
	 */
						
	@ApiModelProperty(value = "手机系统")
	private String os;
	
	/**
	 * 手机品牌
	 */
						
	@ApiModelProperty(value = "手机品牌")
	private String vendor;
	
	/**
	 * 用户id
	 */
						
	@ApiModelProperty(value = "用户id")
	private String uid;
	
	/**
	 * 网络类型，与字典表关联
	 */
						
	@ApiModelProperty(value = "网络类型，与字典表关联")
	private String netType;
	
	/**
	 * 产品id
	 */
						
	@ApiModelProperty(value = "产品id")
	private String appId;
	
	/**
	 * 产品版本号
	 */
						
	@ApiModelProperty(value = "产品版本号")
	private String appVersion;
	
	/**
	 * 该条上报信息上报的时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	@ApiModelProperty(value = "该条上报信息上报的时间")
	private Date time;
	
	/**
	 * 该条上报消息到达跟踪分析平台的时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	@ApiModelProperty(value = "该条上报消息到达跟踪分析平台的时间")
	private Date reachTime;
	
	/**
	 * 手机ip地址
	 */
						
	@ApiModelProperty(value = "手机ip地址")
	private String ip;
	
	/**
	 * 备用字段
	 */
						
	@ApiModelProperty(value = "备用字段")
	private String spare;
	
	/**
	 * 设置：id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：手机型号
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * 获取：手机型号
	 */
	public String getModel() {
		return model;
	}
	/**
	 * 设置：与手机相关的参数
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}
	/**
	 * 获取：与手机相关的参数
	 */
	public String getMac() {
		return mac;
	}
	/**
	 * 设置：与手机相关的参数
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	/**
	 * 获取：与手机相关的参数
	 */
	public String getImsi() {
		return imsi;
	}
	/**
	 * 设置：与手机相关的参数
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}
	/**
	 * 获取：与手机相关的参数
	 */
	public String getImei() {
		return imei;
	}
	/**
	 * 设置：设备名称
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	/**
	 * 获取：设备名称
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * 设置：设备ID
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	/**
	 * 获取：设备ID
	 */
	public String getModelId() {
		return modelId;
	}
	/**
	 * 设置：手机系统
	 */
	public void setOs(String os) {
		this.os = os;
	}
	/**
	 * 获取：手机系统
	 */
	public String getOs() {
		return os;
	}
	/**
	 * 设置：手机品牌
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	/**
	 * 获取：手机品牌
	 */
	public String getVendor() {
		return vendor;
	}
	/**
	 * 设置：用户id
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}
	/**
	 * 获取：用户id
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * 设置：网络类型，与字典表关联
	 */
	public void setNetType(String netType) {
		this.netType = netType;
	}
	/**
	 * 获取：网络类型，与字典表关联
	 */
	public String getNetType() {
		return netType;
	}
	/**
	 * 设置：产品id
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：产品版本号
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
	/**
	 * 设置：该条上报信息上报的时间
	 */
	public void setTime(Date time) {
		this.time = time;
	}
	/**
	 * 获取：该条上报信息上报的时间
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * 设置：该条上报消息到达跟踪分析平台的时间
	 */
	public void setReachTime(Date reachTime) {
		this.reachTime = reachTime;
	}
	/**
	 * 获取：该条上报消息到达跟踪分析平台的时间
	 */
	public Date getReachTime() {
		return reachTime;
	}
	/**
	 * 设置：手机ip地址
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * 获取：手机ip地址
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * 设置：备用字段
	 */
	public void setSpare(String spare) {
		this.spare = spare;
	}
	/**
	 * 获取：备用字段
	 */
	public String getSpare() {
		return spare;
	}
}
