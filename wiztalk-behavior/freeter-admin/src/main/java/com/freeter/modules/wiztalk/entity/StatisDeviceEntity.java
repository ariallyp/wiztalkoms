package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;



/**
 * 设备终端
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-24 17:18:28
 */
@TableName("wztk_statis_device")
@ApiModel(value = "StatisDevice")
public class StatisDeviceEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public StatisDeviceEntity() {
		
	}
	
	public StatisDeviceEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * id
	 */
	
	@TableId 					
	@ApiModelProperty(value = "id",hidden = true)
	private Integer id;
	
	/**
	 * 机型
	 */
						
	@ApiModelProperty(value = "机型")
	private String deviceModel;
	
	/**
	 * 用户数
	 */
						
	@ApiModelProperty(value = "用户数")
	private Integer userNum;
	
	/**
	 * 启动次数
	 */
						
	@ApiModelProperty(value = "启动次数")
	private Integer startTimes;
	
	/**
	 * 启动次数占比
	 */
						
	@ApiModelProperty(value = "启动次数占比")
	private Double startTimesPer;
	
	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：机型
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	/**
	 * 获取：机型
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	/**
	 * 设置：用户数
	 */
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}
	/**
	 * 获取：用户数
	 */
	public Integer getUserNum() {
		return userNum;
	}
	/**
	 * 设置：启动次数
	 */
	public void setStartTimes(Integer startTimes) {
		this.startTimes = startTimes;
	}
	/**
	 * 获取：启动次数
	 */
	public Integer getStartTimes() {
		return startTimes;
	}
	/**
	 * 设置：启动次数占比
	 */
	public void setStartTimesPer(Double startTimesPer) {
		this.startTimesPer = startTimesPer;
	}
	/**
	 * 获取：启动次数占比
	 */
	public Double getStartTimesPer() {
		return startTimesPer;
	}
}
