package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;



/**
 * 错误统计指标存放的表
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-12 10:50:56
 */
@TableName("wztk_statis_error")
@ApiModel(value = "StatisError")
public class StatisErrorEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public StatisErrorEntity() {
		
	}
	
	public StatisErrorEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	
	@TableId 					
	@ApiModelProperty(value = "",hidden = true)
	private Integer id;
	
	/**
	 * 错误数量
	 */
						
	@ApiModelProperty(value = "错误数量")
	private Integer num;
	
	/**
	 * 结果日期
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	@ApiModelProperty(value = "结果日期")
	private Date time;
	
	/**
	 * 产品编号
	 */
						
	@ApiModelProperty(value = "产品编号")
	private String appId;
	
	/**
	 * 产品小版本号
	 */
						
	@ApiModelProperty(value = "产品小版本号")
	private String appVersion;
	
	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：错误数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}
	/**
	 * 获取：错误数量
	 */
	public Integer getNum() {
		return num;
	}
	/**
	 * 设置：结果日期
	 */
	public void setTime(Date time) {
		this.time = time;
	}
	/**
	 * 获取：结果日期
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * 设置：产品编号
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：产品小版本号
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	/**
	 * 获取：产品小版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
}
