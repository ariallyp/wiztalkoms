package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;



/**
 * 关于用户的统计结果的表
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-11-23 11:54:36
 */
@TableName("wztk_statis_user")
@ApiModel(value = "StatisUser")
public class StatisUserEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public StatisUserEntity() {
		
	}
	
	public StatisUserEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * id
	 */
	
	@TableId 					
	@ApiModelProperty(value = "id",hidden = true)
	private Integer id;
	
	/**
	 * 新增用户
	 */
						
	@ApiModelProperty(value = "新增用户")
	private Integer userAdd;
	
	/**
	 * 用户使用
	 */
						
	@ApiModelProperty(value = "用户使用")
	private Integer userUsed;
	
	/**
	 * 用户启动
	 */
						
	@ApiModelProperty(value = "用户启动")
	private Integer userStart;
	
	/**
	 * 产品类型
	 */
						
	@ApiModelProperty(value = "产品类型")
	private String appId;
	
	/**
	 * 手机类型
	 */
						
	@ApiModelProperty(value = "手机类型")
	private String appType;
	
	/**
	 * 产品版本号
	 */
						
	@ApiModelProperty(value = "产品版本号")
	private String appVersion;
	
	/**
	 * 统计结果日期
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	@ApiModelProperty(value = "统计结果日期")
	private Date time;
	
	/**
	 * 设置：id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：新增用户
	 */
	public void setUserAdd(Integer userAdd) {
		this.userAdd = userAdd;
	}
	/**
	 * 获取：新增用户
	 */
	public Integer getUserAdd() {
		return userAdd;
	}
	/**
	 * 设置：用户使用
	 */
	public void setUserUsed(Integer userUsed) {
		this.userUsed = userUsed;
	}
	/**
	 * 获取：用户使用
	 */
	public Integer getUserUsed() {
		return userUsed;
	}
	/**
	 * 设置：用户启动
	 */
	public void setUserStart(Integer userStart) {
		this.userStart = userStart;
	}
	/**
	 * 获取：用户启动
	 */
	public Integer getUserStart() {
		return userStart;
	}
	/**
	 * 设置：产品类型
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品类型
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：手机类型
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	/**
	 * 获取：手机类型
	 */
	public String getAppType() {
		return appType;
	}
	/**
	 * 设置：产品版本号
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
	/**
	 * 设置：统计结果日期
	 */
	public void setTime(Date time) {
		this.time = time;
	}
	/**
	 * 获取：统计结果日期
	 */
	public Date getTime() {
		return time;
	}
}
