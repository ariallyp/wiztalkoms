package com.freeter.modules.wiztalk.entity.model;

import com.freeter.modules.wiztalk.entity.ClientLogEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-04 11:20:41
 */
@ApiModel(value = "ClientLogModel")
public class ClientLogModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 用户id
	 */
	
	@ApiModelProperty(value = "用户id") 
	private String uid;
		
	/**
	 * 用户名
	 */
	
	@ApiModelProperty(value = "用户名") 
	private String uname;
		
	/**
	 * 设备名称
	 */
	
	@ApiModelProperty(value = "设备名称") 
	private String deviceName;
		
	/**
	 * 系统版本
	 */
	
	@ApiModelProperty(value = "系统版本") 
	private String osVersion;
		
	/**
	 * app版本
	 */
	
	@ApiModelProperty(value = "app版本") 
	private String appVersion;
		
	/**
	 * 文件id
	 */
	
	@ApiModelProperty(value = "文件id") 
	private String fid;
		
	/**
	 * 类型
	 */
	
	@ApiModelProperty(value = "类型") 
	private String type;
		
	/**
	 * 设备
	 */
	
	@ApiModelProperty(value = "设备") 
	private String vendor;
		
	/**
	 * 设备型号
	 */
	
	@ApiModelProperty(value = "设备型号") 
	private String model;
		
	/**
	 * 设备ID
	 */
	
	@ApiModelProperty(value = "设备ID") 
	private String deviceid;
		
	/**
	 * 创建时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "创建时间") 
	private Date created;
		
	/**
	 * 生成时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "生成时间") 
	private Date time;
		
	/**
	 * 访问路径
	 */
	
	@ApiModelProperty(value = "访问路径") 
	private String eventPath;
		
	/**
	 * 用户IP
	 */
	
	@ApiModelProperty(value = "用户IP") 
	private String ip;
				
	
	/**
	 * 设置：用户id
	 */
	 
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	/**
	 * 获取：用户id
	 */
	public String getUid() {
		return uid;
	}
				
	
	/**
	 * 设置：用户名
	 */
	 
	public void setUname(String uname) {
		this.uname = uname;
	}
	
	/**
	 * 获取：用户名
	 */
	public String getUname() {
		return uname;
	}
				
	
	/**
	 * 设置：设备名称
	 */
	 
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	/**
	 * 获取：设备名称
	 */
	public String getDeviceName() {
		return deviceName;
	}
				
	
	/**
	 * 设置：系统版本
	 */
	 
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	/**
	 * 获取：系统版本
	 */
	public String getOsVersion() {
		return osVersion;
	}
				
	
	/**
	 * 设置：app版本
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：app版本
	 */
	public String getAppVersion() {
		return appVersion;
	}
				
	
	/**
	 * 设置：文件id
	 */
	 
	public void setFid(String fid) {
		this.fid = fid;
	}
	
	/**
	 * 获取：文件id
	 */
	public String getFid() {
		return fid;
	}
				
	
	/**
	 * 设置：类型
	 */
	 
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * 获取：类型
	 */
	public String getType() {
		return type;
	}
				
	
	/**
	 * 设置：设备
	 */
	 
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	/**
	 * 获取：设备
	 */
	public String getVendor() {
		return vendor;
	}
				
	
	/**
	 * 设置：设备型号
	 */
	 
	public void setModel(String model) {
		this.model = model;
	}
	
	/**
	 * 获取：设备型号
	 */
	public String getModel() {
		return model;
	}
				
	
	/**
	 * 设置：设备ID
	 */
	 
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	
	/**
	 * 获取：设备ID
	 */
	public String getDeviceid() {
		return deviceid;
	}
				
	
	/**
	 * 设置：创建时间
	 */
	 
	public void setCreated(Date created) {
		this.created = created;
	}
	
	/**
	 * 获取：创建时间
	 */
	public Date getCreated() {
		return created;
	}
				
	
	/**
	 * 设置：生成时间
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：生成时间
	 */
	public Date getTime() {
		return time;
	}
				
	
	/**
	 * 设置：访问路径
	 */
	 
	public void setEventPath(String eventPath) {
		this.eventPath = eventPath;
	}
	
	/**
	 * 获取：访问路径
	 */
	public String getEventPath() {
		return eventPath;
	}
				
	
	/**
	 * 设置：用户IP
	 */
	 
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	/**
	 * 获取：用户IP
	 */
	public String getIp() {
		return ip;
	}
			
}
