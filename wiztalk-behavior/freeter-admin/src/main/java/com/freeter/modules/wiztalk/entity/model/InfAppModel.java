package com.freeter.modules.wiztalk.entity.model;

import com.freeter.modules.wiztalk.entity.InfAppEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 产品表
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-17 17:57:05
 */
@ApiModel(value = "InfAppModel")
public class InfAppModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 产品名称
	 */
	
	@ApiModelProperty(value = "产品名称") 
	private String appName;
		
	/**
	 * 产品编码
	 */
	
	@ApiModelProperty(value = "产品编码") 
	private String appCode;
						
	
	/**
	 * 设置：产品名称
	 */
	 
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	/**
	 * 获取：产品名称
	 */
	public String getAppName() {
		return appName;
	}
				
	
	/**
	 * 设置：产品编码
	 */
	 
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	
	/**
	 * 获取：产品编码
	 */
	public String getAppCode() {
		return appCode;
	}
							
}
