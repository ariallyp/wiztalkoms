package com.freeter.modules.wiztalk.entity.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
 

/**
 * 用户与手机机型的关系，用户统计各个机型下的用户分布情况。
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 10:43:38
 */
@ApiModel(value = "RUserModelModel")
public class RUserModelModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 机型id
	 */
	
	@ApiModelProperty(value = "机型id") 
	private String modelId;
				
	
	/**
	 * 设置：机型id
	 */
	 
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	
	/**
	 * 获取：机型id
	 */
	public String getModelId() {
		return modelId;
	}
			
}
