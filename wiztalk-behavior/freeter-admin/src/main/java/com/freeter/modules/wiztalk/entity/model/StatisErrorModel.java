package com.freeter.modules.wiztalk.entity.model;

import com.freeter.modules.wiztalk.entity.StatisErrorEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 错误统计指标存放的表
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-12 10:50:56
 */
@ApiModel(value = "StatisErrorModel")
public class StatisErrorModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 错误数量
	 */
	
	@ApiModelProperty(value = "错误数量") 
	private Integer num;
		
	/**
	 * 结果日期
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "结果日期") 
	private Date time;
		
	/**
	 * 产品编号
	 */
	
	@ApiModelProperty(value = "产品编号") 
	private String appId;
		
	/**
	 * 产品小版本号
	 */
	
	@ApiModelProperty(value = "产品小版本号") 
	private String appVersion;
				
	
	/**
	 * 设置：错误数量
	 */
	 
	public void setNum(Integer num) {
		this.num = num;
	}
	
	/**
	 * 获取：错误数量
	 */
	public Integer getNum() {
		return num;
	}
				
	
	/**
	 * 设置：结果日期
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：结果日期
	 */
	public Date getTime() {
		return time;
	}
				
	
	/**
	 * 设置：产品编号
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
				
	
	/**
	 * 设置：产品小版本号
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：产品小版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
			
}
