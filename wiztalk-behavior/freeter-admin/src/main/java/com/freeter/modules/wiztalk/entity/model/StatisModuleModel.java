package com.freeter.modules.wiztalk.entity.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
 

/**
 * 页面访问量统计结果表
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 19:11:13
 */
@ApiModel(value = "StatisModuleModel")
public class StatisModuleModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 产品id
	 */
	
	@ApiModelProperty(value = "产品id") 
	private String appId;
		
	/**
	 * 页面id
	 */
	
	@ApiModelProperty(value = "页面id") 
	private String moduleId;
		
	/**
	 * 统计结果日期
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "统计结果日期") 
	private Date time;
		
	/**
	 * 产品版本号
	 */
	
	@ApiModelProperty(value = "产品版本号") 
	private String appVersion;
		
	/**
	 * 页面被访问量
	 */
	
	@ApiModelProperty(value = "页面被访问量") 
	private Integer num;
				
	
	/**
	 * 设置：产品id
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
				
	
	/**
	 * 设置：页面id
	 */
	 
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	/**
	 * 获取：页面id
	 */
	public String getModuleId() {
		return moduleId;
	}
				
	
	/**
	 * 设置：统计结果日期
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：统计结果日期
	 */
	public Date getTime() {
		return time;
	}
				
	
	/**
	 * 设置：产品版本号
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
				
	
	/**
	 * 设置：页面被访问量
	 */
	 
	public void setNum(Integer num) {
		this.num = num;
	}
	
	/**
	 * 获取：页面被访问量
	 */
	public Integer getNum() {
		return num;
	}
			
}
