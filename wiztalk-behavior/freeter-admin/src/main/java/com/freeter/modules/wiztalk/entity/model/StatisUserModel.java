package com.freeter.modules.wiztalk.entity.model;

import com.freeter.modules.wiztalk.entity.StatisUserEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import com.freeter.common.annotation.OwnerTable;
 

/**
 * 关于用户的统计结果的表
 * 接收传参的实体类  
 *（后台接收参数） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-11-01 16:25:26
 */
@OwnerTable(StatisUserEntity.class)
@ApiModel(value = "StatisUserModel")
public class StatisUserModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 	
	/**
	 * id
	 */
	 		
	@ApiModelProperty(value = "id") 
	private Integer id;

	
	/**
	 * 新增用户
	 */
	 		
	@ApiModelProperty(value = "新增用户") 
	private Integer userAdd;

	
	/**
	 * 用户使用
	 */
	 		
	@ApiModelProperty(value = "用户使用") 
	private Integer userUsed;

	
	/**
	 * 用户启动
	 */
	 		
	@ApiModelProperty(value = "用户启动") 
	private Integer userStart;

	
	/**
	 * 手机类型
	 */
	 		
	@ApiModelProperty(value = "手机类型") 
	private String appType;

	
	/**
	 * 产品版本号
	 */
	 		
	@ApiModelProperty(value = "产品版本号") 
	private String appVersion;

	
	/**
	 * 统计结果日期
	 */
	 			
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "统计结果日期") 
	private Date time;

 	
	
	/**
	 * 设置：id
	 */
	 
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * 获取：id
	 */
	public Integer getId() {
		return id;
	}
 	 	
	
	/**
	 * 设置：新增用户
	 */
	 
	public void setUserAdd(Integer userAdd) {
		this.userAdd = userAdd;
	}
	
	/**
	 * 获取：新增用户
	 */
	public Integer getUserAdd() {
		return userAdd;
	}
 	 	
	
	/**
	 * 设置：用户使用
	 */
	 
	public void setUserUsed(Integer userUsed) {
		this.userUsed = userUsed;
	}
	
	/**
	 * 获取：用户使用
	 */
	public Integer getUserUsed() {
		return userUsed;
	}
 	 	
	
	/**
	 * 设置：用户启动
	 */
	 
	public void setUserStart(Integer userStart) {
		this.userStart = userStart;
	}
	
	/**
	 * 获取：用户启动
	 */
	public Integer getUserStart() {
		return userStart;
	}
 	 	
	
	/**
	 * 设置：手机类型
	 */
	 
	public void setAppType(String appType) {
		this.appType = appType;
	}
	
	/**
	 * 获取：手机类型
	 */
	public String getAppType() {
		return appType;
	}
 	 	
	
	/**
	 * 设置：产品版本号
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
 	 	
	
	/**
	 * 设置：统计结果日期
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：统计结果日期
	 */
	public Date getTime() {
		return time;
	}
 		
}
