package com.freeter.modules.wiztalk.entity.query;

import com.freeter.modules.wiztalk.entity.StatisStartEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import com.freeter.common.annotation.OwnerTable;


/**
 * 项目概况
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-25 10:25:38
 */
@OwnerTable(StatisStartEntity.class)
@ApiModel(value = "StatisStartQuery")
public class StatisStartQuery  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 启动次数
	 */
	 		
	@ApiModelProperty(value = "启动次数") 
	private Integer startTimes;
		
	/**
	 * 统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等
	 */
	 		
	@ApiModelProperty(value = "统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等") 
	private String timeType;
		
	/**
	 * 统计结果日期
	 */
	 			
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "统计结果日期") 
	private Date time;
		
	/**
	 * 产品id
	 */
	 		
	@ApiModelProperty(value = "产品id") 
	private String appId;
		
	/**
	 * 产品版本号
	 */
	 		
	@ApiModelProperty(value = "产品版本号") 
	private String appVersion;
				
	
	/**
	 * 设置：启动次数
	 */
	 
	public void setStartTimes(Integer startTimes) {
		this.startTimes = startTimes;
	}
	
	/**
	 * 获取：启动次数
	 */
	public Integer getStartTimes() {
		return startTimes;
	}
				
	
	/**
	 * 设置：统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等
	 */
	 
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}
	
	/**
	 * 获取：统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等
	 */
	public String getTimeType() {
		return timeType;
	}
				
	
	/**
	 * 设置：统计结果日期
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：统计结果日期
	 */
	public Date getTime() {
		return time;
	}
				
	
	/**
	 * 设置：产品id
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
				
	
	/**
	 * 设置：产品版本号
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
			
}
