package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.ClientLogEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-04 11:20:41
 */
@TableName("wztk_client_log")
@ApiModel(value = "ClientLog")
public class ClientLogView  extends ClientLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ClientLogView(){
	}
 
 	public ClientLogView(ClientLogEntity clientLogEntity){
 	try {
			BeanUtils.copyProperties(this, clientLogEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
