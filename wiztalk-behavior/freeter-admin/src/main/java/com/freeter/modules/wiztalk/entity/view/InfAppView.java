package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.InfAppEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 产品表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-17 17:57:05
 */
@TableName("wztk_inf_app")
@ApiModel(value = "InfApp")
public class InfAppView  extends InfAppEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public InfAppView(){
	}
 
 	public InfAppView(InfAppEntity infAppEntity){
 	try {
			BeanUtils.copyProperties(this, infAppEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
