package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.InfEventEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
 

/**
 * 自定义事件表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-26 09:55:23
 */
@TableName("wztk_inf_event")
@ApiModel(value = "InfEvent")
public class InfEventView  extends InfEventEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public InfEventView(){
	}
 
 	public InfEventView(InfEventEntity infEventEntity){
 	try {
			BeanUtils.copyProperties(this, infEventEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
