package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.InfModelEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
 

/**
 * 手机表，存储手机的品牌、型号、对应的appId。
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 21:20:38
 */
@TableName("wztk_inf_model")
@ApiModel(value = "InfModel")
public class InfModelView  extends InfModelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public InfModelView(){
	}
 
 	public InfModelView(InfModelEntity infModelEntity){
 	try {
			BeanUtils.copyProperties(this, infModelEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
