package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
 

/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-29 17:21:49
 */
@TableName("wztk_inf_module")
@ApiModel(value = "InfModule")
public class InfModuleView  extends InfModuleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public InfModuleView(){
	}
 
 	public InfModuleView(InfModuleEntity infModuleEntity){
 	try {
			BeanUtils.copyProperties(this, infModuleEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
