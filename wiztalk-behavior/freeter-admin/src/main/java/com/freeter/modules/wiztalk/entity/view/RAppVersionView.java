package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.RAppVersionEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * app与小版本之间的关系表。
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-18 19:45:24
 */
@TableName("wztk_r_app_version")
@ApiModel(value = "RAppVersion")
public class RAppVersionView  extends RAppVersionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public RAppVersionView(){
	}
 
 	public RAppVersionView(RAppVersionEntity rAppVersionEntity){
 	try {
			BeanUtils.copyProperties(this, rAppVersionEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
