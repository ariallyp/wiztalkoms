package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.RUserModelEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
 

/**
 * 用户与手机机型的关系，用户统计各个机型下的用户分布情况。
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 10:43:38
 */
@TableName("wztk_r_user_model")
@ApiModel(value = "RUserModel")
public class RUserModelView  extends RUserModelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public RUserModelView(){
	}
 
 	public RUserModelView(RUserModelEntity rUserModelEntity){
 	try {
			BeanUtils.copyProperties(this, rUserModelEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
