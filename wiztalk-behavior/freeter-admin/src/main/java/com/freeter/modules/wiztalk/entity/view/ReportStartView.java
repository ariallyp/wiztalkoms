package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.ReportStartEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;


/**
 * 启动上报信息实时表，存放昨天和今天两天的启动上报信息。零点清除前天的数据并转移到历史表中
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-04 20:37:57
 */
@TableName("wztk_report_start")
@ApiModel(value = "ReportStart")
public class ReportStartView  extends ReportStartEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ReportStartView(){
	}
 
 	public ReportStartView(ReportStartEntity reportStartEntity){
 	try {
			BeanUtils.copyProperties(this, reportStartEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
