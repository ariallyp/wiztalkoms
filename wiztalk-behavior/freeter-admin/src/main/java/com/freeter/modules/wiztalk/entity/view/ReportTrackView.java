package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.ReportTrackEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;


/**
 * 埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-04 20:37:57
 */
@TableName("wztk_report_track")
@ApiModel(value = "ReportTrack")
public class ReportTrackView  extends ReportTrackEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ReportTrackView(){
	}
 
 	public ReportTrackView(ReportTrackEntity reportTrackEntity){
 	try {
			BeanUtils.copyProperties(this, reportTrackEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
