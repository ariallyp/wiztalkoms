package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.StatisDeviceEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 设备终端
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-24 17:18:28
 */
@ApiModel(value = "StatisDevice")
public class StatisDeviceView  extends StatisDeviceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public StatisDeviceView(){
	}
 
 	public StatisDeviceView(StatisDeviceEntity statisDeviceEntity){
 	try {
			BeanUtils.copyProperties(this, statisDeviceEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
