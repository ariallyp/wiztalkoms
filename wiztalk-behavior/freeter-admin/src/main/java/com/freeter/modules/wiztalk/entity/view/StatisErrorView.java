package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.StatisErrorEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 错误统计指标存放的表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-12 10:50:56
 */
@TableName("wztk_statis_error")
@ApiModel(value = "StatisError")
public class StatisErrorView  extends StatisErrorEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public StatisErrorView(){
	}

	public List<Integer> getErrorNum() {
		return errorNum;
	}

	public void setErrorNum(List<Integer> errorNum) {
		this.errorNum = errorNum;
	}

	List<Integer> errorNum = new ArrayList<>();


 	public StatisErrorView(StatisErrorEntity statisErrorEntity){
 	try {
			BeanUtils.copyProperties(this, statisErrorEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
