package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.StatisModuleEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
 

/**
 * 页面访问量统计结果表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 19:11:13
 */
@TableName("wztk_statis_module")
@ApiModel(value = "StatisModule")
public class StatisModuleView  extends StatisModuleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public StatisModuleView(){

	}
 
 	public StatisModuleView(StatisModuleEntity statisModuleEntity){
 	try {
			BeanUtils.copyProperties(this, statisModuleEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
