package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.StatisStartEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 项目概况
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-25 10:25:38
 */
@ApiModel(value = "StatisStart")
public class StatisStartView  extends StatisStartEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public StatisStartView(){
	}
 
 	public StatisStartView(StatisStartEntity statisStartEntity){
 	try {
			BeanUtils.copyProperties(this, statisStartEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
