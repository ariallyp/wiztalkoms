package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.StatisUserEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 关于用户的统计结果的表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-11-01 16:25:26
 */
@ApiModel(value = "StatisUser")
public class StatisUserView  extends StatisUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public StatisUserView(){
	}
 
 	public StatisUserView(StatisUserEntity statisUserEntity){
 	try {
			BeanUtils.copyProperties(this, statisUserEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}

	private String currentMonth;
	public String currentHour;

	public String getCurrentHour() {
		return currentHour;
	}

	public void setCurrentHour(String currentHour) {
		this.currentHour = currentHour;
	}

	public String getCurrentMonth() {
		return currentMonth;
	}
	public void setCurrentMonth(String currentMonth) {
		this.currentMonth = currentMonth;
	}
}
