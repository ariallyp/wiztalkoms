package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.TenantEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-03 17:47:58
 */
@TableName("wztk_tenant")
@ApiModel(value = "Tenant")
public class TenantView  extends TenantEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public TenantView(){
	}
 
 	public TenantView(TenantEntity tenantEntity){
 	try {
			BeanUtils.copyProperties(this, tenantEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
