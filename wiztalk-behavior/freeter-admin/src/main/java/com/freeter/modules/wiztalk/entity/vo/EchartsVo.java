package com.freeter.modules.wiztalk.entity.vo;

import java.util.List;

public class EchartsVo {

    private List<String> xAxis;
    private List<String> lengend;
    private List<Series> seriesList;

    public List<String> getxAxis() {
        return xAxis;
    }

    public void setxAxis(List<String> xAxis) {
        this.xAxis = xAxis;
    }

    public List<String> getLengend() {
        return lengend;
    }

    public void setLengend(List<String> lengend) {
        this.lengend = lengend;
    }

    public List<Series> getSeriesList() {
        return seriesList;
    }

    public void setSeriesList(List<Series> seriesList) {
        this.seriesList = seriesList;
    }

}
