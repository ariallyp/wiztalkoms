package com.freeter.modules.wiztalk.entity.vo;

import com.freeter.modules.wiztalk.entity.InfAppEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 产品表
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-17 17:57:05
 */
@ApiModel(value = "InfAppVO")
public class InfAppVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 产品名称
	 */
	
	@ApiModelProperty(value = "产品名称") 
	private String appName;
		
	/**
	 * 产品编码
	 */
	
	@ApiModelProperty(value = "产品编码") 
	private String appCode;
						
	
	/**
	 * 设置：产品名称
	 */
	 
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	/**
	 * 获取：产品名称
	 */
	public String getAppName() {
		return appName;
	}
				
	
	/**
	 * 设置：产品编码
	 */
	 
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	
	/**
	 * 获取：产品编码
	 */
	public String getAppCode() {
		return appCode;
	}
							
}
