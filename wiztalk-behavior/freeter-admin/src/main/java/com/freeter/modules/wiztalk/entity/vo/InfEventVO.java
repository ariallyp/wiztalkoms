package com.freeter.modules.wiztalk.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
 

/**
 * 自定义事件表
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-26 09:55:23
 */
@ApiModel(value = "InfEventVO")
public class InfEventVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 事件名称
	 */
	
	@ApiModelProperty(value = "事件名称") 
	private String eventName;
		
	/**
	 * 事件别名
	 */
	
	@ApiModelProperty(value = "事件别名") 
	private String alias;
		
	/**
	 * 产品编号
	 */
	
	@ApiModelProperty(value = "产品编号") 
	private String appId;
				
	/**
	 * 状态   0停用  1启用
	 */
	
	@ApiModelProperty(value = "状态   0停用  1启用") 
	private String status;
		
	/**
	 * 创建者
	 */
	
	@ApiModelProperty(value = "创建者") 
	private Long createUser;
		
	/**
	 * 修改者
	 */
	
	@ApiModelProperty(value = "修改者") 
	private Long updateUser;
				
	
	/**
	 * 设置：事件名称
	 */
	 
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	/**
	 * 获取：事件名称
	 */
	public String getEventName() {
		return eventName;
	}
				
	
	/**
	 * 设置：事件别名
	 */
	 
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	/**
	 * 获取：事件别名
	 */
	public String getAlias() {
		return alias;
	}
				
	
	/**
	 * 设置：产品编号
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
								
	
	/**
	 * 设置：状态   0停用  1启用
	 */
	 
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * 获取：状态   0停用  1启用
	 */
	public String getStatus() {
		return status;
	}
				
	
	/**
	 * 设置：创建者
	 */
	 
	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}
	
	/**
	 * 获取：创建者
	 */
	public Long getCreateUser() {
		return createUser;
	}
				
	
	/**
	 * 设置：修改者
	 */
	 
	public void setUpdateUser(Long updateUser) {
		this.updateUser = updateUser;
	}
	
	/**
	 * 获取：修改者
	 */
	public Long getUpdateUser() {
		return updateUser;
	}
			
}
