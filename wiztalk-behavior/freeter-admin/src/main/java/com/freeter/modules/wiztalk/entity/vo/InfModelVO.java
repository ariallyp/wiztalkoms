package com.freeter.modules.wiztalk.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
 

/**
 * 手机表，存储手机的品牌、型号、对应的appId。
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 21:20:38
 */
@ApiModel(value = "InfModelVO")
public class InfModelVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 机型名称
	 */
	
	@ApiModelProperty(value = "机型名称") 
	private String model;
		
	/**
	 * 手机品牌
	 */
	
	@ApiModelProperty(value = "手机品牌") 
	private String vendor;
		
	/**
	 * 系统
	 */
	
	@ApiModelProperty(value = "系统") 
	private String os;
		
	/**
	 * 启动次数
	 */
	
	@ApiModelProperty(value = "启动次数") 
	private Long startTimes;
		
	/**
	 * 产品编号
	 */
	
	@ApiModelProperty(value = "产品编号") 
	private String appId;
				
	/**
	 * 修改人
	 */
	
	@ApiModelProperty(value = "修改人") 
	private Long updateUser;
		
	/**
	 * 机型状态 0表示停用  1表示启用
	 */
	
	@ApiModelProperty(value = "机型状态 0表示停用  1表示启用") 
	private String status;
				
	
	/**
	 * 设置：机型名称
	 */
	 
	public void setModel(String model) {
		this.model = model;
	}
	
	/**
	 * 获取：机型名称
	 */
	public String getModel() {
		return model;
	}
				
	
	/**
	 * 设置：手机品牌
	 */
	 
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	/**
	 * 获取：手机品牌
	 */
	public String getVendor() {
		return vendor;
	}
				
	
	/**
	 * 设置：系统
	 */
	 
	public void setOs(String os) {
		this.os = os;
	}
	
	/**
	 * 获取：系统
	 */
	public String getOs() {
		return os;
	}
				
	
	/**
	 * 设置：启动次数
	 */
	 
	public void setStartTimes(Long startTimes) {
		this.startTimes = startTimes;
	}
	
	/**
	 * 获取：启动次数
	 */
	public Long getStartTimes() {
		return startTimes;
	}
				
	
	/**
	 * 设置：产品编号
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
								
	
	/**
	 * 设置：修改人
	 */
	 
	public void setUpdateUser(Long updateUser) {
		this.updateUser = updateUser;
	}
	
	/**
	 * 获取：修改人
	 */
	public Long getUpdateUser() {
		return updateUser;
	}
				
	
	/**
	 * 设置：机型状态 0表示停用  1表示启用
	 */
	 
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * 获取：机型状态 0表示停用  1表示启用
	 */
	public String getStatus() {
		return status;
	}
			
}
