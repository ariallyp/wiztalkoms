package com.freeter.modules.wiztalk.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
 

/**
 * app与小版本之间的关系表。
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-18 19:45:24
 */
@ApiModel(value = "RAppVersionVO")
public class RAppVersionVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 产品id
	 */
	
	@ApiModelProperty(value = "产品id") 
	private String appId;
					
	
	/**
	 * 设置：产品id
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
					
}
