package com.freeter.modules.wiztalk.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
 

/**
 * 用户与手机机型的关系，用户统计各个机型下的用户分布情况。
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 10:43:38
 */
@ApiModel(value = "RUserModelVO")
public class RUserModelVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 机型id
	 */
	
	@ApiModelProperty(value = "机型id") 
	private String modelId;
				
	
	/**
	 * 设置：机型id
	 */
	 
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	
	/**
	 * 获取：机型id
	 */
	public String getModelId() {
		return modelId;
	}
			
}
