package com.freeter.modules.wiztalk.entity.vo;

import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 错误日志详情表
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 15:35:21
 */
@ApiModel(value = "ReportErrorSubVO")
public class ReportErrorSubVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 错误内容
	 */
	
	@ApiModelProperty(value = "错误内容") 
	private String text;
				
	
	/**
	 * 设置：错误内容
	 */
	 
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * 获取：错误内容
	 */
	public String getText() {
		return text;
	}
			
}
