package com.freeter.modules.wiztalk.entity.vo;

import com.freeter.modules.wiztalk.entity.StatisDeviceEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 设备终端
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-24 17:18:28
 */
@ApiModel(value = "StatisDeviceVO")
public class StatisDeviceVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 机型
	 */
	
	@ApiModelProperty(value = "机型") 
	private String deviceModel;
		
	/**
	 * 用户数
	 */
	
	@ApiModelProperty(value = "用户数") 
	private Integer userNum;
		
	/**
	 * 启动次数
	 */
	
	@ApiModelProperty(value = "启动次数") 
	private Integer startTimes;
		
	/**
	 * 启动次数占比
	 */
	
	@ApiModelProperty(value = "启动次数占比") 
	private Double startTimesPer;
				
	
	/**
	 * 设置：机型
	 */
	 
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	
	/**
	 * 获取：机型
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
				
	
	/**
	 * 设置：用户数
	 */
	 
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}
	
	/**
	 * 获取：用户数
	 */
	public Integer getUserNum() {
		return userNum;
	}
				
	
	/**
	 * 设置：启动次数
	 */
	 
	public void setStartTimes(Integer startTimes) {
		this.startTimes = startTimes;
	}
	
	/**
	 * 获取：启动次数
	 */
	public Integer getStartTimes() {
		return startTimes;
	}
				
	
	/**
	 * 设置：启动次数占比
	 */
	 
	public void setStartTimesPer(Double startTimesPer) {
		this.startTimesPer = startTimesPer;
	}
	
	/**
	 * 获取：启动次数占比
	 */
	public Double getStartTimesPer() {
		return startTimesPer;
	}
			
}
