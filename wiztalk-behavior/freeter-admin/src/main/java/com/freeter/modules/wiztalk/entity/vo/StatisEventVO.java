package com.freeter.modules.wiztalk.entity.vo;

import com.freeter.modules.wiztalk.entity.StatisEventEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 自定义事件的统计结果表
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 11:48:50
 */
@ApiModel(value = "StatisEventVO")
public class StatisEventVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 产品id
	 */
	
	@ApiModelProperty(value = "产品id") 
	private String appId;
		
	/**
	 * 自定义事件id
	 */
	
	@ApiModelProperty(value = "自定义事件id") 
	private String eventId;
		
	/**
	 * 统计结果日期
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "统计结果日期") 
	private Date time;
		
	/**
	 * 产品版本号
	 */
	
	@ApiModelProperty(value = "产品版本号") 
	private String appVersion;
		
	/**
	 * 消息的数量
	 */
	
	@ApiModelProperty(value = "消息的数量") 
	private Integer num;
				
	
	/**
	 * 设置：产品id
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
				
	
	/**
	 * 设置：自定义事件id
	 */
	 
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	/**
	 * 获取：自定义事件id
	 */
	public String getEventId() {
		return eventId;
	}
				
	
	/**
	 * 设置：统计结果日期
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：统计结果日期
	 */
	public Date getTime() {
		return time;
	}
				
	
	/**
	 * 设置：产品版本号
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
				
	
	/**
	 * 设置：消息的数量
	 */
	 
	public void setNum(Integer num) {
		this.num = num;
	}
	
	/**
	 * 获取：消息的数量
	 */
	public Integer getNum() {
		return num;
	}
			
}
