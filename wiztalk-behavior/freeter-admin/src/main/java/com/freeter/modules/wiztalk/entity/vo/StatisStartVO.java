package com.freeter.modules.wiztalk.entity.vo;

import com.freeter.modules.wiztalk.entity.StatisStartEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 项目概况
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-25 10:25:38
 */
@ApiModel(value = "StatisStartVO")
public class StatisStartVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 启动次数
	 */
	
	@ApiModelProperty(value = "启动次数") 
	private Integer startTimes;
		
	/**
	 * 统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等
	 */
	
	@ApiModelProperty(value = "统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等") 
	private String timeType;
		
	/**
	 * 统计结果日期
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "统计结果日期") 
	private Date time;
		
	/**
	 * 产品id
	 */
	
	@ApiModelProperty(value = "产品id") 
	private String appId;
		
	/**
	 * 产品版本号
	 */
	
	@ApiModelProperty(value = "产品版本号") 
	private String appVersion;
				
	
	/**
	 * 设置：启动次数
	 */
	 
	public void setStartTimes(Integer startTimes) {
		this.startTimes = startTimes;
	}
	
	/**
	 * 获取：启动次数
	 */
	public Integer getStartTimes() {
		return startTimes;
	}
				
	
	/**
	 * 设置：统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等
	 */
	 
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}
	
	/**
	 * 获取：统计结果类型，该字段表明该条结果是时统计，天统计，周统计，月统计，年统计等
	 */
	public String getTimeType() {
		return timeType;
	}
				
	
	/**
	 * 设置：统计结果日期
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：统计结果日期
	 */
	public Date getTime() {
		return time;
	}
				
	
	/**
	 * 设置：产品id
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
				
	
	/**
	 * 设置：产品版本号
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
			
}
