package com.freeter.modules.wiztalk.entity.vo;

import com.freeter.modules.wiztalk.entity.TenantEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-03 17:47:58
 */
@ApiModel(value = "TenantVO")
public class TenantVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 
	 */
	
	@ApiModelProperty(value = "") 
	private String code;
		
	/**
	 * 
	 */
	
	@ApiModelProperty(value = "") 
	private String tenantName;
		
	/**
	 * 
	 */
	
	@ApiModelProperty(value = "") 
	private Integer status;
		
	/**
	 * 
	 */
	
	@ApiModelProperty(value = "") 
	private String customerId;
		
	/**
	 * 
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "") 
	private Date created;
		
	/**
	 * 
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "") 
	private Date updated;
				
	
	/**
	 * 设置：
	 */
	 
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * 获取：
	 */
	public String getCode() {
		return code;
	}
				
	
	/**
	 * 设置：
	 */
	 
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
	/**
	 * 获取：
	 */
	public String getTenantName() {
		return tenantName;
	}
				
	
	/**
	 * 设置：
	 */
	 
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
				
	
	/**
	 * 设置：
	 */
	 
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	/**
	 * 获取：
	 */
	public String getCustomerId() {
		return customerId;
	}
				
	
	/**
	 * 设置：
	 */
	 
	public void setCreated(Date created) {
		this.created = created;
	}
	
	/**
	 * 获取：
	 */
	public Date getCreated() {
		return created;
	}
				
	
	/**
	 * 设置：
	 */
	 
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	/**
	 * 获取：
	 */
	public Date getUpdated() {
		return updated;
	}
			
}
