package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.ClientLogEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.ClientLogVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.ClientLogView;


/**
 * 
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-04 11:20:41
 */
public interface ClientLogService extends IService<ClientLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ClientLogVO> selectListVO(Wrapper<ClientLogEntity> wrapper);
   	
   	ClientLogVO selectVO(@Param("ew") Wrapper<ClientLogEntity> wrapper);
   	
   	List<ClientLogView> selectListView(Wrapper<ClientLogEntity> wrapper);
   	
   	ClientLogView selectView(@Param("ew") Wrapper<ClientLogEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<ClientLogEntity> wrapper);
}

