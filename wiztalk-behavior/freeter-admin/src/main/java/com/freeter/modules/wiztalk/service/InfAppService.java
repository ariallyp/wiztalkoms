package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.InfAppEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.InfAppVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.InfAppView;


/**
 * 产品表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-17 17:57:05
 */
public interface InfAppService extends IService<InfAppEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<InfAppVO> selectListVO(Wrapper<InfAppEntity> wrapper);
   	
   	InfAppVO selectVO(@Param("ew") Wrapper<InfAppEntity> wrapper);
   	
   	List<InfAppView> selectListView(Wrapper<InfAppEntity> wrapper);
   	
   	InfAppView selectView(@Param("ew") Wrapper<InfAppEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<InfAppEntity> wrapper);
}

