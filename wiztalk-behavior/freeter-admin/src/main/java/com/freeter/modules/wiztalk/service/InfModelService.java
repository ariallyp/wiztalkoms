package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.InfModelEntity;
import com.freeter.modules.wiztalk.entity.view.InfModelView;
import com.freeter.modules.wiztalk.entity.vo.InfModelVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 手机表，存储手机的品牌、型号、对应的appId。
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-04 21:02:57
 */
public interface InfModelService extends IService<InfModelEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<InfModelVO> selectListVO(Wrapper<InfModelEntity> wrapper);
   	
   	InfModelVO selectVO(@Param("ew") Wrapper<InfModelEntity> wrapper);
   	
   	List<InfModelView> selectListView(Wrapper<InfModelEntity> wrapper);
   	
   	InfModelView selectView(@Param("ew") Wrapper<InfModelEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params, Wrapper<InfModelEntity> wrapper);

    PageUtils getModelStat(@Param("params") Map<String,Object> params);
}

