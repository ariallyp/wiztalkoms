package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.RAppVersionEntity;
import com.freeter.modules.wiztalk.entity.view.RAppVersionView;
import com.freeter.modules.wiztalk.entity.vo.RAppVersionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * app与小版本之间的关系表。
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-18 19:45:24
 */
public interface RAppVersionService extends IService<RAppVersionEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<RAppVersionVO> selectListVO(Wrapper<RAppVersionEntity> wrapper);
   	
   	RAppVersionVO selectVO(@Param("ew") Wrapper<RAppVersionEntity> wrapper);
   	
   	List<RAppVersionView> selectListView(Wrapper<RAppVersionEntity> wrapper);
   	
   	RAppVersionView selectView(@Param("ew") Wrapper<RAppVersionEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params, Wrapper<RAppVersionEntity> wrapper);
}

