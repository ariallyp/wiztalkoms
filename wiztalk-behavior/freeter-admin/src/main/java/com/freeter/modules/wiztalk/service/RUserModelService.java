package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.RUserModelEntity;
import com.freeter.modules.wiztalk.entity.view.RUserModelView;
import com.freeter.modules.wiztalk.entity.vo.RUserModelVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 用户与手机机型的关系，用户统计各个机型下的用户分布情况。
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-05 10:43:38
 */
public interface RUserModelService extends IService<RUserModelEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<RUserModelVO> selectListVO(Wrapper<RUserModelEntity> wrapper);
   	
   	RUserModelVO selectVO(@Param("ew") Wrapper<RUserModelEntity> wrapper);
   	
   	List<RUserModelView> selectListView(Wrapper<RUserModelEntity> wrapper);
   	
   	RUserModelView selectView(@Param("ew") Wrapper<RUserModelEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params, Wrapper<RUserModelEntity> wrapper);
}

