package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.ReportLoginEntity;
import com.freeter.modules.wiztalk.entity.view.ReportLoginView;
import com.freeter.modules.wiztalk.entity.vo.ReportLoginVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 登录上报信息实时表，只存放昨天和今天两天的数据，零点的时候清除前天的数据，并移存到对应的历史表中。
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 17:06:02
 */
public interface ReportLoginService extends IService<ReportLoginEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ReportLoginVO> selectListVO(Wrapper<ReportLoginEntity> wrapper);
   	
   	ReportLoginVO selectVO(@Param("ew") Wrapper<ReportLoginEntity> wrapper);
   	
   	List<ReportLoginView> selectListView(Wrapper<ReportLoginEntity> wrapper);
   	
   	ReportLoginView selectView(@Param("ew") Wrapper<ReportLoginEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params, Wrapper<ReportLoginEntity> wrapper);
}

