package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.ReportStartEntity;
import com.freeter.modules.wiztalk.entity.view.ReportStartView;
import com.freeter.modules.wiztalk.entity.vo.ReportStartVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 启动start表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 17:44:22
 */
public interface ReportStartService extends IService<ReportStartEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ReportStartVO> selectListVO(Wrapper<ReportStartEntity> wrapper);
   	
   	ReportStartVO selectVO(@Param("ew") Wrapper<ReportStartEntity> wrapper);
   	
   	List<ReportStartView> selectListView(Wrapper<ReportStartEntity> wrapper);
   	
   	ReportStartView selectView(@Param("ew") Wrapper<ReportStartEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params, Wrapper<ReportStartEntity> wrapper);
}

