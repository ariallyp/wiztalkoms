package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.StatisDeviceEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.StatisDeviceVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.StatisDeviceView;
import com.freeter.common.utils.PageInfo;


/**
 * 设备终端
 *
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-24 17:18:28
 */
 @SuppressWarnings({"unchecked","rawtypes"})
public interface StatisDeviceService extends IService<StatisDeviceEntity> {

    
   	List<StatisDeviceVO> selectListVO(Wrapper<StatisDeviceEntity> wrapper);
   	
   	StatisDeviceVO selectVO(@Param("ew") Wrapper<StatisDeviceEntity> wrapper);
   	
   	List<StatisDeviceView> selectListView(Wrapper<StatisDeviceEntity> wrapper);
   	
   	StatisDeviceView selectView(@Param("ew") Wrapper<StatisDeviceEntity> wrapper);
   	
   	PageUtils queryPage(PageInfo pageInfo,Wrapper<StatisDeviceEntity> wrapper);
}

