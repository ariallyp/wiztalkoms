package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.ReportErrorEntity;
import com.freeter.modules.wiztalk.entity.StatisErrorEntity;
import java.util.List;
import java.util.Map;

import com.freeter.modules.wiztalk.entity.view.ReportErrorView;
import com.freeter.modules.wiztalk.entity.vo.StatisErrorVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.StatisErrorView;


/**
 * 错误统计指标存放的表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-10-12 10:50:56
 */
public interface StatisErrorService extends IService<StatisErrorEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<StatisErrorVO> selectListVO(Wrapper<StatisErrorEntity> wrapper);
   	
   	StatisErrorVO selectVO(@Param("ew") Wrapper<StatisErrorEntity> wrapper);
   	
   	List<StatisErrorView> selectListView(Wrapper<StatisErrorEntity> wrapper);
   	
   	StatisErrorView selectView(@Param("ew") Wrapper<StatisErrorEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<StatisErrorEntity> wrapper);


	List getErrorNum (@Param("ew") Wrapper<StatisErrorEntity> wrapper);

}

