package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.StatisEventEntity;
import com.freeter.modules.wiztalk.entity.view.StatisEventView;
import com.freeter.modules.wiztalk.entity.vo.StatisEventVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 自定义事件的统计结果表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 11:48:50
 */
public interface StatisEventService extends IService<StatisEventEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<StatisEventVO> selectListVO(Wrapper<StatisEventEntity> wrapper);
   	
   	StatisEventVO selectVO(@Param("ew") Wrapper<StatisEventEntity> wrapper);
   	
   	List<StatisEventView> selectListView(Wrapper<StatisEventEntity> wrapper);
   	
   	StatisEventView selectView(@Param("ew") Wrapper<StatisEventEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<StatisEventEntity> wrapper);

    List<Map<String,Object>> getStatByCondition(@Param("params")Map<String,Object> params);


    void batchInsert(List<Map<String,Object>> entityList);
}

