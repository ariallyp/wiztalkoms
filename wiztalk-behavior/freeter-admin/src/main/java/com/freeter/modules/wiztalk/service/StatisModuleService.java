package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.StatisModuleEntity;
import com.freeter.modules.wiztalk.entity.view.StatisModuleView;
import com.freeter.modules.wiztalk.entity.vo.StatisModuleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 页面访问量统计结果表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 19:11:13
 */
public interface StatisModuleService extends IService<StatisModuleEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<StatisModuleVO> selectListVO(Wrapper<StatisModuleEntity> wrapper);
   	
   	StatisModuleVO selectVO(@Param("ew") Wrapper<StatisModuleEntity> wrapper);
   	
   	List<StatisModuleView> selectListView(Wrapper<StatisModuleEntity> wrapper);
   	
   	StatisModuleView selectView(@Param("ew") Wrapper<StatisModuleEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params, Wrapper<StatisModuleEntity> wrapper);

    void batchInsert(List<Map<String,Object>> entityList);

}

