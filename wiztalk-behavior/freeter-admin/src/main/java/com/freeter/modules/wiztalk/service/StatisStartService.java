package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.StatisStartEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.StatisStartVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.StatisStartView;
import com.freeter.common.utils.PageInfo;


/**
 * 项目概况
 *
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-10-25 10:25:38
 */
 @SuppressWarnings({"unchecked","rawtypes"})
public interface StatisStartService extends IService<StatisStartEntity> {

    
   	List<StatisStartVO> selectListVO(Wrapper<StatisStartEntity> wrapper);
   	
   	StatisStartVO selectVO(@Param("ew") Wrapper<StatisStartEntity> wrapper);
   	
   	List<StatisStartView> selectListView(Wrapper<StatisStartEntity> wrapper);
   	
   	StatisStartView selectView(@Param("ew") Wrapper<StatisStartEntity> wrapper);
   	
   	PageUtils queryPage(PageInfo pageInfo,Wrapper<StatisStartEntity> wrapper);

	List selectTrendData(@Param("ew") Wrapper<StatisStartEntity> wrapper);

	List<StatisStartView> selectAppVersion(@Param("ew") Wrapper<StatisStartEntity> wrapper);

}

