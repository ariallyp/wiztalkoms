package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.StatisUserEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.StatisUserVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.StatisUserView;
import com.freeter.common.utils.PageInfo;


/**
 * 关于用户的统计结果的表
 *
 * @author Arial
 * @email arial@foxmail.com
 * @date 2018-11-01 16:25:26
 */
 @SuppressWarnings({"unchecked","rawtypes"})
public interface StatisUserService extends IService<StatisUserEntity> {

    
   	List<StatisUserVO> selectListVO(Wrapper<StatisUserEntity> wrapper);
   	
   	StatisUserVO selectVO(@Param("ew") Wrapper<StatisUserEntity> wrapper);
   	
   	List<StatisUserView> selectListView(Wrapper<StatisUserEntity> wrapper);
   	
   	StatisUserView selectView(@Param("ew") Wrapper<StatisUserEntity> wrapper);
   	
   	PageUtils queryPage(PageInfo pageInfo,Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectLatYearData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectAllTotalData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectCurrentDayHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectYesterDayHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectWeekHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectMonthHourData(@Param("ew") Wrapper<StatisUserEntity> wrapper);

	List<StatisUserView> selectTrendDayData(@Param("ew") Wrapper<StatisUserEntity> wrapper);
}

