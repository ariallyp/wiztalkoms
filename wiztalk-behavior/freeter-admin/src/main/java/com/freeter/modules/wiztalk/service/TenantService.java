package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.TenantEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.TenantVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.TenantView;


/**
 * 
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-03 17:47:58
 */
public interface TenantService extends IService<TenantEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<TenantVO> selectListVO(Wrapper<TenantEntity> wrapper);
   	
   	TenantVO selectVO(@Param("ew") Wrapper<TenantEntity> wrapper);
   	
   	List<TenantView> selectListView(Wrapper<TenantEntity> wrapper);
   	
   	TenantView selectView(@Param("ew") Wrapper<TenantEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<TenantEntity> wrapper);
}

