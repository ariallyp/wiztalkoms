package com.freeter.modules.wiztalk.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;

import com.freeter.modules.wiztalk.dao.ClientLogDao;
import com.freeter.modules.wiztalk.entity.ClientLogEntity;
import com.freeter.modules.wiztalk.service.ClientLogService;
import com.freeter.modules.wiztalk.entity.vo.ClientLogVO;
import com.freeter.modules.wiztalk.entity.view.ClientLogView;


@Service("clientLogService")
public class ClientLogServiceImpl extends ServiceImpl<ClientLogDao, ClientLogEntity> implements ClientLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ClientLogEntity> page = this.selectPage(
                new Query<ClientLogEntity>(params).getPage(),
                new EntityWrapper<ClientLogEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ClientLogEntity> wrapper) {
		  Page<ClientLogView> page =new Query<ClientLogView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<ClientLogVO> selectListVO( Wrapper<ClientLogEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ClientLogVO selectVO( Wrapper<ClientLogEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ClientLogView> selectListView(Wrapper<ClientLogEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ClientLogView selectView(Wrapper<ClientLogEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
