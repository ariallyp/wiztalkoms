package com.freeter.modules.wiztalk.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;

import com.freeter.modules.wiztalk.dao.InfAppDao;
import com.freeter.modules.wiztalk.entity.InfAppEntity;
import com.freeter.modules.wiztalk.service.InfAppService;
import com.freeter.modules.wiztalk.entity.vo.InfAppVO;
import com.freeter.modules.wiztalk.entity.view.InfAppView;


@Service("infAppService")
public class InfAppServiceImpl extends ServiceImpl<InfAppDao, InfAppEntity> implements InfAppService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<InfAppEntity> page = this.selectPage(
                new Query<InfAppEntity>(params).getPage(),
                new EntityWrapper<InfAppEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<InfAppEntity> wrapper) {
		  Page<InfAppView> page =new Query<InfAppView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<InfAppVO> selectListVO( Wrapper<InfAppEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public InfAppVO selectVO( Wrapper<InfAppEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<InfAppView> selectListView(Wrapper<InfAppEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public InfAppView selectView(Wrapper<InfAppEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
