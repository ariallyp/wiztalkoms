package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.InfModelDao;
import com.freeter.modules.wiztalk.entity.InfModelEntity;
import com.freeter.modules.wiztalk.entity.view.InfModelView;
import com.freeter.modules.wiztalk.entity.vo.InfModelVO;
import com.freeter.modules.wiztalk.service.InfModelService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;


@Service("infModelService")
public class InfModelServiceImpl extends ServiceImpl<InfModelDao, InfModelEntity> implements InfModelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<InfModelEntity> page = this.selectPage(
                new Query<InfModelEntity>(params).getPage(),
                new EntityWrapper<InfModelEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<InfModelEntity> wrapper) {
		  Page<InfModelView> page =new Query<InfModelView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<InfModelVO> selectListVO( Wrapper<InfModelEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public InfModelVO selectVO( Wrapper<InfModelEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<InfModelView> selectListView(Wrapper<InfModelEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public InfModelView selectView(Wrapper<InfModelEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}



    /**
     * 根据前台传来的条件获取设备终端的统计情况
     *
     * @param params 前台传递的参数
     * @return
     */
    @Override
    public PageUtils getModelStat(Map<String, Object> params) {
        Page page = getEventStatPage(params);
        List<Map<String,Object>> maps =  baseMapper.getModelStat(page,params);
        Double sum=0D;
        NumberFormat num = NumberFormat.getPercentInstance();
        num.setMaximumIntegerDigits(3);
        num.setMaximumFractionDigits(2);
        for(Map<String,Object> map:maps){
             sum=sum+Double.parseDouble(map.get("startTimes").toString());
        }
        for(Map<String,Object> map:maps){
            if(sum >0 ){
                map.put("statPercent",num.format(Double.parseDouble(map.get("startTimes").toString())/sum));
            }else{
                map.put("statPercent",num.format(0D));
            }

        }
        page.setRecords(maps);
        PageUtils pageUtil = new PageUtils(page);
        return pageUtil;
    }



    /**
     * 根据前台传来的条件设置一个page对象
     *
     * @param params 前台传来的条件
     * @return
     */
    public Page<Map<String, Object>> getEventStatPage(Map<String, Object> params) {
        int pageNum = 1;
        int pageSize = 10;
        if (params.get("page") != null) {
            pageNum = Integer.parseInt(params.get("page").toString());
            if (pageNum <= 0) {
                pageNum = 1;
            }
        }
        if (params.get("limit") != null) {
            pageSize = Integer.parseInt(params.get("limit").toString());
            if (pageSize <= 0) {
                pageSize = 10;
            }
        }
        Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageNum, pageSize);
        if (params.get("sidx") != null) {
            if("stat_percent".equalsIgnoreCase(params.get("sidx").toString())){
                params.put("sidx","start_times");
            }
            page.setOrderByField(params.get("sidx").toString());
            if (params.get("order") != null) {
                if ("asc".equalsIgnoreCase(params.get("order").toString())) {
                    page.setAsc(true);
                } else if ("desc".equalsIgnoreCase(params.get("order").toString())) {
                    page.setAsc(false);
                }
            }
        }
        return page;
    }

    @Scheduled(cron = "0 39 15 * * * ")
    public static void  test(){

            System.out.println(" ===========================首选===================================================");
                double f = 0.5585;
              BigDecimal b = new BigDecimal(f);
                 double f1 = b.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
                System.out.println(f1);
                System.out.println(Integer.parseInt(new DecimalFormat("0").format(f*100))+"%");//百分比没有小数点
            System.out.println(" ===========================首选===================================================");
               double result1=0.51111122111111;
                 DecimalFormat df = new DecimalFormat("0.00%");
                  String r = df.format(result1);
                System.out.println(r);//great
            System.out.println(" ===========================首选===================================================");
                  NumberFormat num = NumberFormat.getPercentInstance();
                   num.setMaximumIntegerDigits(3);
                    num.setMaximumFractionDigits(2);
                    double csdn = 0.55555555555555555;
                   System.out.println(num.format(csdn));//good
            System.out.println(" ===========================首选===================================================");
                 double result=1;
                   int temp = (int)(result * 1000);
                   result = (double)temp / 10;
                    System.out.println(result + "%");//100%  变成了  100.0%
           }

}
