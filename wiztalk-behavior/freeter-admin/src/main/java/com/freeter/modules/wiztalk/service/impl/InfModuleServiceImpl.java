package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.InfModuleDao;
import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import com.freeter.modules.wiztalk.entity.view.InfModuleView;
import com.freeter.modules.wiztalk.entity.vo.InfModuleVO;
import com.freeter.modules.wiztalk.service.InfModuleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("infModuleService")
public class InfModuleServiceImpl extends ServiceImpl<InfModuleDao, InfModuleEntity> implements InfModuleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<InfModuleEntity> page = this.selectPage(
                new Query<InfModuleEntity>(params).getPage(),
                new EntityWrapper<InfModuleEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<InfModuleEntity> wrapper) {
		  Page<InfModuleView> page =new Query<InfModuleView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<InfModuleVO> selectListVO( Wrapper<InfModuleEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public InfModuleVO selectVO( Wrapper<InfModuleEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<InfModuleView> selectListView(Wrapper<InfModuleEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public InfModuleView selectView(Wrapper<InfModuleEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

    /**
     * 导入新页面
     * @param newModules
     * @return
     */
    @Override
    @Transactional
	public List<InfModuleEntity> importModules(List<Map<String,Object>> newModules){
        Date createTime = new Date();
        List<InfModuleEntity> moduleList = new ArrayList<InfModuleEntity>();
        for(Map<String,Object> map:newModules){
            if(baseMapper.selectEntityByConditon(map).isEmpty()){
                InfModuleEntity infModuleEntity = new InfModuleEntity();
                infModuleEntity.setId(UUID.randomUUID().toString());
                infModuleEntity.setAlias(map.get("moduleName").toString());
                infModuleEntity.setAppId(map.get("appId").toString());
                infModuleEntity.setCreateTime(createTime);
                //此处应该加上创建人
                infModuleEntity.setUpdateTime(createTime);
                infModuleEntity.setModuleName(map.get("moduleName").toString());
                baseMapper.insertModule(infModuleEntity);
                moduleList.add(infModuleEntity);
            }
        }
        return moduleList;
    }

}
