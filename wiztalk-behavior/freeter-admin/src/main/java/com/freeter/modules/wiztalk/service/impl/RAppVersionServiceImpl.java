package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.RAppVersionDao;
import com.freeter.modules.wiztalk.entity.RAppVersionEntity;
import com.freeter.modules.wiztalk.entity.view.RAppVersionView;
import com.freeter.modules.wiztalk.entity.vo.RAppVersionVO;
import com.freeter.modules.wiztalk.service.RAppVersionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("rAppVersionService")
public class RAppVersionServiceImpl extends ServiceImpl<RAppVersionDao, RAppVersionEntity> implements RAppVersionService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RAppVersionEntity> page = this.selectPage(
                new Query<RAppVersionEntity>(params).getPage(),
                new EntityWrapper<RAppVersionEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<RAppVersionEntity> wrapper) {
		  Page<RAppVersionView> page =new Query<RAppVersionView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<RAppVersionVO> selectListVO( Wrapper<RAppVersionEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public RAppVersionVO selectVO( Wrapper<RAppVersionEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<RAppVersionView> selectListView(Wrapper<RAppVersionEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public RAppVersionView selectView(Wrapper<RAppVersionEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
