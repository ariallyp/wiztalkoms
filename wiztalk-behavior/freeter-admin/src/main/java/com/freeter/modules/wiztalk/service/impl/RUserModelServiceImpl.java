package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.RUserModelDao;
import com.freeter.modules.wiztalk.entity.RUserModelEntity;
import com.freeter.modules.wiztalk.entity.view.RUserModelView;
import com.freeter.modules.wiztalk.entity.vo.RUserModelVO;
import com.freeter.modules.wiztalk.service.RUserModelService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("rUserModelService")
public class RUserModelServiceImpl extends ServiceImpl<RUserModelDao, RUserModelEntity> implements RUserModelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RUserModelEntity> page = this.selectPage(
                new Query<RUserModelEntity>(params).getPage(),
                new EntityWrapper<RUserModelEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<RUserModelEntity> wrapper) {
		  Page<RUserModelView> page =new Query<RUserModelView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<RUserModelVO> selectListVO( Wrapper<RUserModelEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public RUserModelVO selectVO( Wrapper<RUserModelEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<RUserModelView> selectListView(Wrapper<RUserModelEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public RUserModelView selectView(Wrapper<RUserModelEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
