package com.freeter.modules.wiztalk.service.impl;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;

import com.freeter.modules.wiztalk.dao.ReportErrorDao;
import com.freeter.modules.wiztalk.entity.ReportErrorEntity;
import com.freeter.modules.wiztalk.service.ReportErrorService;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorVO;
import com.freeter.modules.wiztalk.entity.view.ReportErrorView;


@Service("reportErrorService")
public class ReportErrorServiceImpl extends ServiceImpl<ReportErrorDao, ReportErrorEntity> implements ReportErrorService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ReportErrorEntity> page = this.selectPage(
                new Query<ReportErrorEntity>(params).getPage(),
                new EntityWrapper<ReportErrorEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ReportErrorEntity> wrapper) {
		  Page<ReportErrorView> page =new Query<ReportErrorView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<ReportErrorVO> selectListVO( Wrapper<ReportErrorEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ReportErrorVO selectVO( Wrapper<ReportErrorEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ReportErrorView> selectListView(Wrapper<ReportErrorEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ReportErrorView selectView(Wrapper<ReportErrorEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

	@Override
	public List<ReportErrorView> getAppVersion( Wrapper<ReportErrorEntity> wrapper){
    	return baseMapper.getAppVersion(wrapper);
	};

}
