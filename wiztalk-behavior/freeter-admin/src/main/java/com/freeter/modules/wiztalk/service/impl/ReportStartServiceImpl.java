package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.ReportStartDao;
import com.freeter.modules.wiztalk.entity.ReportStartEntity;
import com.freeter.modules.wiztalk.entity.view.ReportStartView;
import com.freeter.modules.wiztalk.entity.vo.ReportStartVO;
import com.freeter.modules.wiztalk.service.ReportStartService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("reportStartService")
public class ReportStartServiceImpl extends ServiceImpl<ReportStartDao, ReportStartEntity> implements ReportStartService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ReportStartEntity> page = this.selectPage(
                new Query<ReportStartEntity>(params).getPage(),
                new EntityWrapper<ReportStartEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ReportStartEntity> wrapper) {
		  Page<ReportStartView> page =new Query<ReportStartView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<ReportStartVO> selectListVO(Wrapper<ReportStartEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ReportStartVO selectVO(Wrapper<ReportStartEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ReportStartView> selectListView(Wrapper<ReportStartEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ReportStartView selectView(Wrapper<ReportStartEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
