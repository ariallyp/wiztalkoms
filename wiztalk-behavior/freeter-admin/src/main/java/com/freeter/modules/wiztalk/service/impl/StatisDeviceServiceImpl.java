package com.freeter.modules.wiztalk.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;

import com.freeter.modules.wiztalk.dao.StatisDeviceDao;
import com.freeter.modules.wiztalk.entity.StatisDeviceEntity;
import com.freeter.modules.wiztalk.service.StatisDeviceService;
import com.freeter.modules.wiztalk.entity.vo.StatisDeviceVO;
import com.freeter.modules.wiztalk.entity.view.StatisDeviceView;
import com.freeter.common.utils.PageInfo;

@SuppressWarnings({"unchecked","rawtypes"})
@Service("statisDeviceService")
public class StatisDeviceServiceImpl extends ServiceImpl<StatisDeviceDao, StatisDeviceEntity> implements StatisDeviceService {


    @Override
	public PageUtils queryPage(PageInfo pageInfo, Wrapper<StatisDeviceEntity> wrapper) {
		  	Page<StatisDeviceView> page =pageInfo.getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<StatisDeviceVO> selectListVO( Wrapper<StatisDeviceEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public StatisDeviceVO selectVO( Wrapper<StatisDeviceEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<StatisDeviceView> selectListView(Wrapper<StatisDeviceEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public StatisDeviceView selectView(Wrapper<StatisDeviceEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
