package com.freeter.modules.wiztalk.service.impl;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;

import com.freeter.modules.wiztalk.dao.StatisErrorDao;
import com.freeter.modules.wiztalk.entity.StatisErrorEntity;
import com.freeter.modules.wiztalk.service.StatisErrorService;
import com.freeter.modules.wiztalk.entity.vo.StatisErrorVO;
import com.freeter.modules.wiztalk.entity.view.StatisErrorView;


@Service("statisErrorService")
public class StatisErrorServiceImpl extends ServiceImpl<StatisErrorDao, StatisErrorEntity> implements StatisErrorService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<StatisErrorEntity> page = this.selectPage(
                new Query<StatisErrorEntity>(params).getPage(),
                new EntityWrapper<StatisErrorEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<StatisErrorEntity> wrapper) {
		  Page<StatisErrorView> page =new Query<StatisErrorView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<StatisErrorVO> selectListVO( Wrapper<StatisErrorEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public StatisErrorVO selectVO( Wrapper<StatisErrorEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<StatisErrorView> selectListView(Wrapper<StatisErrorEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public StatisErrorView selectView(Wrapper<StatisErrorEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

	@Override
	public List getErrorNum (@Param("ew") Wrapper<StatisErrorEntity> wrapper) {
		return baseMapper.getErrorNum(wrapper);
	}

}
