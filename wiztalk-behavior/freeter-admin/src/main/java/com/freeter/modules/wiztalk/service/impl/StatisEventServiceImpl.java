package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.StatisEventDao;
import com.freeter.modules.wiztalk.entity.StatisEventEntity;
import com.freeter.modules.wiztalk.entity.view.StatisEventView;
import com.freeter.modules.wiztalk.entity.vo.StatisEventVO;
import com.freeter.modules.wiztalk.service.StatisEventService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("statisEventService")
public class StatisEventServiceImpl extends ServiceImpl<StatisEventDao, StatisEventEntity> implements StatisEventService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<StatisEventEntity> page = this.selectPage(
                new Query<StatisEventEntity>(params).getPage(),
                new EntityWrapper<StatisEventEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<StatisEventEntity> wrapper) {
		  Page<StatisEventView> page =new Query<StatisEventView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<StatisEventVO> selectListVO( Wrapper<StatisEventEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public StatisEventVO selectVO( Wrapper<StatisEventEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<StatisEventView> selectListView(Wrapper<StatisEventEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public StatisEventView selectView(Wrapper<StatisEventEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


	public List<Map<String,Object>> getStatByCondition(@Param("params")Map<String,Object> params){
        return baseMapper.getStatByCondition(params);
    }


    /**
     *自定义事件的统计结果批量插入统计结果表
     * @param entityList 要插入的数据集合
     */
    @Override
    @Transactional
    public void batchInsert(List<Map<String,Object>> entityList){
        baseMapper.batchInsert(entityList);
    }
}
