package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.StatisModuleDao;
import com.freeter.modules.wiztalk.entity.StatisModuleEntity;
import com.freeter.modules.wiztalk.entity.view.StatisModuleView;
import com.freeter.modules.wiztalk.entity.vo.StatisModuleVO;
import com.freeter.modules.wiztalk.service.StatisModuleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("statisModuleService")
public class StatisModuleServiceImpl extends ServiceImpl<StatisModuleDao, StatisModuleEntity> implements StatisModuleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<StatisModuleEntity> page = this.selectPage(
                new Query<StatisModuleEntity>(params).getPage(),
                new EntityWrapper<StatisModuleEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<StatisModuleEntity> wrapper) {
		  Page<StatisModuleView> page =new Query<StatisModuleView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<StatisModuleVO> selectListVO( Wrapper<StatisModuleEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public StatisModuleVO selectVO( Wrapper<StatisModuleEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<StatisModuleView> selectListView(Wrapper<StatisModuleEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public StatisModuleView selectView(Wrapper<StatisModuleEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

    /**
     *埋点信息统计结果批量插入统计结果表
     * @param entityList 要插入的数据集合
     */
    @Override
    @Transactional
    public void batchInsert(List<Map<String,Object>> entityList){
        baseMapper.batchInsert(entityList);
    }


}
