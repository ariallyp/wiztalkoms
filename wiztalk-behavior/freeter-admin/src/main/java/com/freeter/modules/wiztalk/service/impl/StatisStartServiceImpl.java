package com.freeter.modules.wiztalk.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;

import com.freeter.modules.wiztalk.dao.StatisStartDao;
import com.freeter.modules.wiztalk.entity.StatisStartEntity;
import com.freeter.modules.wiztalk.service.StatisStartService;
import com.freeter.modules.wiztalk.entity.vo.StatisStartVO;
import com.freeter.modules.wiztalk.entity.view.StatisStartView;
import com.freeter.common.utils.PageInfo;

@SuppressWarnings({"unchecked","rawtypes"})
@Service("statisStartService")
public class StatisStartServiceImpl extends ServiceImpl<StatisStartDao, StatisStartEntity> implements StatisStartService {


    @Override
	public PageUtils queryPage(PageInfo pageInfo, Wrapper<StatisStartEntity> wrapper) {
		  	Page<StatisStartView> page =pageInfo.getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<StatisStartVO> selectListVO( Wrapper<StatisStartEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public StatisStartVO selectVO( Wrapper<StatisStartEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<StatisStartView> selectListView(Wrapper<StatisStartEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public List selectTrendData(Wrapper<StatisStartEntity> wrapper) {
		return baseMapper.selectTrendData(wrapper);
	}

	@Override
	public List<StatisStartView> selectAppVersion(Wrapper<StatisStartEntity> wrapper) {
		return  baseMapper.selectAppVersion(wrapper);
	}

	@Override
	public StatisStartView selectView(Wrapper<StatisStartEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}



}
