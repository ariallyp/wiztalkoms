package com.freeter.modules.wiztalk.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;

import com.freeter.modules.wiztalk.dao.StatisUserDao;
import com.freeter.modules.wiztalk.entity.StatisUserEntity;
import com.freeter.modules.wiztalk.service.StatisUserService;
import com.freeter.modules.wiztalk.entity.vo.StatisUserVO;
import com.freeter.modules.wiztalk.entity.view.StatisUserView;
import com.freeter.common.utils.PageInfo;

@SuppressWarnings({"unchecked","rawtypes"})
@Service("statisUserService")
public class StatisUserServiceImpl extends ServiceImpl<StatisUserDao, StatisUserEntity> implements StatisUserService {



	@Override
	public PageUtils queryPage(PageInfo pageInfo, Wrapper<StatisUserEntity> wrapper) {
		  	Page<StatisUserView> page =pageInfo.getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}

	@Override
	public List<StatisUserView> selectLatYearData(Wrapper<StatisUserEntity> wrapper) {
		return baseMapper.selectLatYearData(wrapper);
	}

	@Override
	public List<StatisUserView> selectAllTotalData(Wrapper<StatisUserEntity> wrapper) {
		return baseMapper.selectAllTotalData(wrapper);
	}

	@Override
	public List<StatisUserView> selectHourData(Wrapper<StatisUserEntity> wrapper) {
		return baseMapper.selectHourData(wrapper);
	}

	@Override
	public List<StatisUserVO> selectListVO( Wrapper<StatisUserEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public StatisUserVO selectVO( Wrapper<StatisUserEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<StatisUserView> selectListView(Wrapper<StatisUserEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public StatisUserView selectView(Wrapper<StatisUserEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

	@Override
	public List<StatisUserView> selectCurrentDayHourData(Wrapper<StatisUserEntity> wrapper) {
		return  baseMapper.selectCurrentDayHourData(wrapper);
	}

	@Override
	public List<StatisUserView> selectYesterDayHourData(Wrapper<StatisUserEntity> wrapper) {
		return  baseMapper.selectYesterDayHourData(wrapper);
	}

	@Override
	public List<StatisUserView> selectWeekHourData(Wrapper<StatisUserEntity> wrapper) {
		return  baseMapper.selectWeekHourData(wrapper);
	}

	@Override
	public List<StatisUserView> selectMonthHourData(Wrapper<StatisUserEntity> wrapper) {
		return  baseMapper.selectMonthHourData(wrapper);
	}

	@Override
	public List<StatisUserView> selectTrendDayData(Wrapper<StatisUserEntity> wrapper) {
		return baseMapper.selectTrendDayData(wrapper);
	}
}
