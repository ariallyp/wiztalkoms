$(function () {
    var container = echarts.init(document.getElementById('container'));//demo 1
    var initChartLine = echarts.init(document.getElementById('initChartLine'));  //demo2
    var app4 = echarts.init(document.getElementById('app4'));  //demo3

    Highcharts.chart('container', {
        title: {
            text: 'Highcharts 桑基图'
        },
        series: [{
            keys: ['from', 'to', 'weight'],
            point: {
                // 鼠标划过节点时高亮相连的连接节点
                events: {
                    mouseOver: function() {
                        if(this.isNode) {
                            Highcharts.each(this.linksFrom, function(p) {
                                var newColor = new Highcharts.color(p.color).setOpacity(1);
                                p.graphic.element.setAttribute('fill', 'rgba(' + newColor.rgba.join() + ')');
                            });
                            Highcharts.each(this.linksTo, function(p) {
                                var newColor = new Highcharts.color(p.color).setOpacity(1);
                                p.graphic.element.setAttribute('fill', 'rgba(' + newColor.rgba.join() + ')');
                            });
                        }
                    },
                    mouseOut: function() {
                        if(this.isNode) {
                            Highcharts.each(this.linksFrom, function(p) {
                                var newColor = new Highcharts.color(p.color).setOpacity(0.5);
                                p.graphic.element.setAttribute('fill', 'rgba(' + newColor.rgba.join() + ')');
                            });
                            Highcharts.each(this.linksTo, function(p) {
                                var newColor = new Highcharts.color(p.color).setOpacity(0.5);
                                p.graphic.element.setAttribute('fill', 'rgba(' + newColor.rgba.join() + ')');
                            });
                        }
                    }
                }
            },
            data: [
                ['巴西', '葡萄牙', 5 ],
                ['巴西', '法国', 1 ],
                ['巴西', '西班牙', 1 ],
                ['巴西', '英国', 1 ],
                ['加拿大', '葡萄牙', 1 ],
                ['加拿大', '法国', 5 ],
                ['加拿大', '英国', 1 ],
                ['墨西哥', '葡萄牙', 1 ],
                ['墨西哥', '法国', 1 ],
                ['墨西哥', '西班牙', 5 ],
                ['墨西哥', '英国', 1 ],
                ['美国', '葡萄牙', 1 ],
                ['美国', '法国', 1 ],
                ['美国', '西班牙', 1 ],
                ['美国', '英国', 5 ],
                ['葡萄牙', '安哥拉', 2 ],
                ['葡萄牙', '塞内加尔', 1 ],
                ['葡萄牙', '摩洛哥', 1 ],
                ['葡萄牙', '南非', 3 ],
                ['法国', '安哥拉', 1 ],
                ['法国', '塞内加尔', 3 ],
                ['法国', '马里', 3 ],
                ['法国', '摩洛哥', 3 ],
                ['法国', '南非', 1 ],
                ['西班牙', '塞内加尔', 1 ],
                ['西班牙', '摩洛哥', 3 ],
                ['西班牙', '南非', 1 ],
                ['英国', '安哥拉', 1 ],
                ['英国', '塞内加尔', 1 ],
                ['英国', '摩洛哥', 2 ],
                ['英国', '南非', 7 ],
                ['南非', '中国', 5 ],
                ['南非', '印度', 1 ],
                ['南非', '日本', 3 ],
                ['安哥拉', '中国', 5 ],
                ['安哥拉', '印度', 1 ],
                ['安哥拉', '日本', 3 ],
                ['塞内加尔', '中国', 5 ],
                ['塞内加尔', '印度', 1 ],
                ['塞内加尔', '日本', 3 ],
                ['马里', '中国', 5 ],
                ['马里', '印度', 1 ],
                ['马里', '日本', 3 ],
                ['摩洛哥', '中国', 5 ],
                ['摩洛哥', '印度', 1 ],
                ['摩洛哥', '日本', 3 ]
            ],
            type: 'sankey',
            name: 'Sankey demo series'
        }]
    });


    app4.on('updateAxisPointer', function (event) {
        var xAxisInfo = event.axesInfo[0];
        if (xAxisInfo) {
            var dimension = xAxisInfo.value + 1;
            app4.setOption({
                series: {
                    id: 'pie',
                    label: {
                        formatter: '{b}: {@[' + dimension + ']} ({d}%)'
                    },
                    encode: {
                        value: dimension,
                        tooltip: dimension
                    }
                }
            });
        }
    });

    var option = {
        title: {
            text: 'Wiz数据跟踪分析 '
        },
        tooltip: {},
        legend: {
            data:['点击次数']
        },
        xAxis: {
            data: ["登录","通讯录","插件","聊天","个人中心","其它"]
        },
        yAxis: {},
        series: [{
            name: '点击次数',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
        }]
    };

    var option2 = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['通讯录', '首页', '聊天', '登录', '个人中心', '其他']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '通讯录',
                type: 'bar',
                data: [320, 332, 301, 334, 390, 330, 320]
            },
            {
                name: '首页',
                type: 'bar',
                stack: '广告',
                data: [120, 132, 101, 134, 90, 230, 210]
            },
            {
                name: '聊天',
                type: 'bar',
                stack: '广告',
                data: [220, 182, 191, 234, 290, 330, 310]
            },
            {
                name: '登录',
                type: 'bar',
                stack: '广告',
                data: [150, 232, 201, 154, 190, 330, 410]
            },
            {
                name: '个人中心',
                type: 'bar',
                data: [862, 1018, 964, 1026, 1679, 1600, 1570],
                markLine: {
                    lineStyle: {
                        normal: {
                            type: 'dashed'
                        }
                    },
                    data: [
                        [{ type: 'min' }, { type: 'max' }]
                    ]
                }
            },

            {
                name: '其他',
                type: 'bar',
                stack: '个人中心',
                data: [62, 82, 91, 84, 109, 110, 120]
            }
        ]
    };
    initChartLine.setOption(option2);
    app4.setOption(option);
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		clientLog: {}
	}

});