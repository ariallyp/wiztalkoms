/**
 * 获取下来产品列表
 */
function getAppList() {
    $.ajax({
        url : baseURL +"wiztalk/infapp/getAppList",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : 'data',

        async:false,

        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {
            console.info(data);
            if(!data.data||data.data.length==0){
                alert("产品数据还未出现")
            }
            // $('#search_appId').append(
            //     "<option value=''>所有产品 </option>");
            $.each(data.data, function(i) {
                $('#search_appId').append(
                    "<option value=" + data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });
            $('#search_appId').selectpicker('refresh');
            $('#search_appId').selectpicker('val', '');
            $('#search_appId').selectpicker('render');
        },
        error : function(data) {
            alert("查询失败" + data);
        }
    })

}


/**
 * 刷新饼图
 */
function freshPie(){
    var appId = $("#search_appId option:selected").val();
    if(appId == ''){
        alert("产品数据还未出现");
        return;
    }else{
        var array = new Array();
        $.ajax({
            url : baseURL +"reporttrack/getModuleStatNow?appId="+appId,//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
            // 数据发送方式
            type : "post",
            // 接受数据格式
            dataType : "json",
            //同步
            async:false,
            // 回调函数，接受服务器端返回给客户端的值，即result值
            success : function(data) {
                console.info(data);
                if(!data.data||data.data.length==0){
                    alert("无统计结果")
                }
                $.each(data.data, function(i) {
                    var obj = new Object();
                    obj.name=data.data[i].alias ;
                    obj.y=data.data[i].percentage;
                    obj.num=data.data[i].num;
                    array[i] = obj;
                    console.info(obj);
                });
            },
            error : function(data) {
                alert("查询失败" + data);
            }
        })

        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: '访问路径占比'
            },
            credits:{
                enabled:false
            },
            exporting:{
                enabled:false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b><br/>数量: <b>{point.num}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.2f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: '占比',
                colorByPoint: true,
                data: array
            }]
        });
    }
}

/**
 * 页面初始化
 */
function init(){
    getAppList();//初始化下拉框中的值
    $('#search_appId').change(function(){//为下拉框注册选中改变事件
        freshPie();
    });
    $('#search_appId option:first').prop("selected",true);//默认选中下拉框中的第一个
    var selectedValue=$('#search_appId option:first').text();//获取下拉框中被选中的值
    $('.filter-option.pull-left:first').text(selectedValue);//将被选中的值展示在文本框中
    freshPie();//刷新饼图
}


init();


