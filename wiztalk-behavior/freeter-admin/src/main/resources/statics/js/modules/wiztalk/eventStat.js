/*获取产品列表*/
function getAppList() {
    $.ajax({
        url: baseURL + "wiztalk/infapp/getAppList",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : 'data',
        //同步
        async: false,
        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {
            console.info(data);
            if(!data.data||data.data.length==0){
                alert("暂无产品信息")
            }
            $('#search_appId').append(
                "<option value=''>所有产品 </option>");
            $.each(data.data, function(i) {
                $('#search_appId').append(
                    "<option value=" +data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });


            $('#appId').append(
                "<option value=''>所有产品 </option>");
            $.each(data.data, function(i) {
                $('#appId').append(
                    "<option value=" +data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });
            $('#appId').selectpicker('refresh');
            $('#appId').selectpicker('val', '');
            $('#appId').selectpicker('render');


            $('#search_appId').selectpicker('refresh');
            $('#search_appId').selectpicker('val', '');
            $('#search_appId').selectpicker('render');

        },
        error : function(data) {

            alert("查询失败" + data);

        }
    })
}

function bindAppId(){
    var selectAppId=vm.infEvent.appId;
    if(typeof(selectAppId) == "undefined" || selectAppId  == ''){
        return ;
    }
    $('#appId').find("option[value = '"+selectAppId+"']").attr("selected","selected");
    var selectedValue= $('#appId').find("option[value = '"+selectAppId+"']").text();//获取下拉框中被选中的值
    $('.filter-option.pull-left:last').text(selectedValue);//将被选中的值展示在文本框中
}

/*获取下拉版本列表*/
function getAppVersionList() {//获取下拉版本列表
    var channelId =   $('#search_appId').selectpicker('val');
    $.ajax({
        url: baseURL + "rappversion/getAppVersionList",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : {appId:channelId},
        //同步
        async: false,
        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {
            if((!data.data||data.data.length==0) && channelId != ''){
                alert("暂无版本信息")
            }
            $('#search_appVersion').empty();
            $('#search_appVersion').append(
                "<option value=''>版本 </option>");
            $.each(data.data, function(i) {
                $('#search_appVersion').append(
                    "<option value=" + data.data[i].appVersion   + ">"
                    + data.data[i].appVersion + "</option>");
            });
            $('#search_appVersion').selectpicker('refresh');
            $('#search_appVersion').selectpicker('val', '');
            $('#search_appVersion').selectpicker('render');

        },
        error : function(data) {
            alert("查询失败" + data);

        }
    })

}

$(function () {
    $("#search_appId").selectpicker({
        noneSelectedText : '请选择产品'  ,
        liveSearchPlaceholder : "请输入关键字",
        noneResultsText : "内容无法匹配"
    });
    $("#search_appVersion").selectpicker({
        noneSelectedText : '请选择版本'  ,
        liveSearchPlaceholder : "请输入关键字",
        noneResultsText : "内容无法匹配"
    });



    getAppList();

    $('#search_appId').change(function(){
        getAppVersionList();
    });


    //getImage();
    $("#jqGrid").jqGrid({
        url: baseURL + 'reporttrack/getEventStatTwoDay',
        datatype: "json",
        colModel: [
            { label: 'eventId', name: 'eventId', index: 'event_id', width: 50, key: true,hidden:true },
            { label: '事件ID', name: 'eventName', index: 'event_name', width: 50},
            { label: '事件名称', name: 'alias', index: 'alias', width: 80 },
            { label: '昨日消息数', name: 'yesterdayNum', index: 'yesterday_num', width: 80 },
            { label: '今日消息数', name: 'todayNum', index: 'today_num', width: 80 },
            { label: '详情', name: '', index: 'operate', width: 80, align: 'center',
                formatter: function (cellvalue, options, rowObject) {
                    var detail="<input type='button' value='查看' onclick='detail(\""+rowObject.event_id+"\",\""+rowObject.event_name+"\")'>";
                    return detail;
                },},
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.list",
            page: "data.currPage",
            total: "data.totalPage",
            records: "data.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        showList: true,
        title: null,
        infEvent: {},
        search:{}
    },
    methods: {
        validateEventName:function(){
            if($.trim(vm.infEvent.eventName) == ''){
                $('#eventName').val("事件ID不能为空");
                $('#eventName')[0].focus();
            }
        },
        validateAlias:function(){
            if($.trim(vm.infEvent.alias) == ''){
                $('#alias').val("事件名称不能为空");
                $('#alias')[0].focus();
            }
        },
        query: function () {
            vm.search.appId =$('#search_appId').selectpicker('val');
            vm.search.appVersion =$('#search_appVersion').selectpicker('val');
            vm.search.alias=$('#search_alias').val();
            vm.reload();
        },
        add: function(){
            vm.showList = false;
            vm.title = "新增";
            vm.infEvent = {};
            $('#appId').selectpicker('refresh');
            $('#appId').selectpicker('val', '');
            $('#appId').selectpicker('render');
        },
        update: function (event) {
            var rowId = getSelectedRow();
            console.info(rowId);
            vm.showList = false;
            vm.title = "修改";
            vm.getInfo(rowId)
            bindAppId();
        },
        saveOrUpdate: function (event) {
            var url = vm.infEvent.id == null ? "wiztalk/infevent/save" : "wiztalk/infevent/update";
            var  appId=$('#appId').selectpicker('val');
            if(appId == ""){
                alert("请选中产品");
                return ;
            }
            vm.infEvent.appId=appId;
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.infEvent),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功', function(index){
                            vm.reload();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        del: function (event) {
            var rowIds = getSelectedRows();
            if(rowIds == null){
                return ;
            }
            confirm('确定要删除选中的记录？', function(){
                $.ajax({
                    type: "POST",
                    url: baseURL + "wiztalk/infevent/delete",
                    contentType: "application/json",
                    data: JSON.stringify(rowIds),
                    success: function(r){
                        if(r.code == 0){
                            alert('操作成功', function(index){
                                $("#jqGrid").trigger("reloadGrid");
                            });
                        }else{
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        getInfo: function(eventId){
            // $.get(baseURL + "wiztalk/infevent/info/"+eventId, function(r){
            //     vm.infEvent = r.infEvent;
            // });
            $.ajax({
                url: baseURL + "wiztalk/infevent/info/"+eventId,//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
                // 数据发送方式
                type : "get",
                // 接受数据格式
                dataType : "json",
                // 要传递的数据
                data : 'data',
                //同步
                async: false,
                // 回调函数，接受服务器端返回给客户端的值，即result值
                success : function(data) {
                    console.info(data);
                    vm.infEvent = data.infEvent;
                },
                error : function(data) {

                    alert("查询事件详细失败" + data);

                }
            })
        },
        reload: function (event) {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:vm.search,
                page:1
            }).trigger("reloadGrid");
        },
    }
});

function detail(event_id,event_name){
    var appId=$('#search_appId').selectpicker('val');
    var appVersion =$('#search_appVersion').selectpicker('val');
    if(appId == null){
        appId = "";
    }
    if(appVersion == null){
        appVersion = "";
    }
    location.href=baseURL + 'modules/wiztalk/eventStatDetail.html';
}