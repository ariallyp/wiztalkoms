var versionArray = [];//版本号组成的数组，每次改变产品后会更新该数组

/**
 * 获取下来产品列表
 */
function getAppList() {
    $.ajax({
        url: baseURL + "wiztalk/infapp/getAppList",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式
        type: "get",
        // 接受数据格式
        dataType: "json",
        // 要传递的数据
        data: 'data',

        async: false,

        // 回调函数，接受服务器端返回给客户端的值，即result值
        success: function (data) {
            console.info(data);
            if (!data.data || data.data.length == 0) {
                alert("产品数据还未出现")
            }
            // $('#search_appId').append(
            //     "<option value=''>所有产品 </option>");
            $.each(data.data, function (i) {
                $('#search_appId').append(
                    "<option value=" + data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });
            $('#search_appId').selectpicker('refresh');
            $('#search_appId').selectpicker('val', '');
            $('#search_appId').selectpicker('render');
        },
        error: function (data) {
            alert("查询失败" + data);
        }
    })

}

/**
 * 根据当前产品获取产品的包含的版本号
 */
function getAppVersionList() {
    $('#search_appVersion').empty();
    var appId = $("#search_appId option:selected").val();
    $.ajax({
        url: baseURL + "rappversion/getAppVersionList",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式
        type: "get",
        // 接受数据格式
        dataType: "json",
        // 要传递的数据
        data: {"appId": appId},

        async: false,

        // 回调函数，接受服务器端返回给客户端的值，即result值
        success: function (data) {
            console.info(data);
            versionArray = [];//每次改变产品，意味这版本号数组也要更新，所以这里先清空，后面再填充
            if (!data.data || data.data.length == 0) {
                alert("版本数据还未出现")
                return;
            }
            $.each(data.data, function (i) {
                $('#search_appVersion').append(
                    "<option value=" + data.data[i].appVersion + ">"
                    + data.data[i].appVersion + "</option>");
                versionArray[i] = data.data[i].appVersion;
            });
            $('#search_appVersion').selectpicker('refresh');
            $('#search_appVersion').selectpicker('val', '');
            $('#search_appVersion').selectpicker('render');
        },
        error: function (data) {
            alert("查询失败" + data);
        }
    })


    $('#search_appVersion option:last').prop("selected", true);//默认选中版本下拉框中的最后一个
    var selectedAppVersion = $('#search_appVersion option:last').text();//获取版本下拉框中被选中的值
    $('.filter-option.pull-left:last').text(selectedAppVersion);//将被选中的值展示在版本文本框中
}



/**
 * 点击查询按钮执行该方法
 */
function freshLines() {
    var appId = $("#search_appId option:selected").val();
    var appVersion = $("#search_appVersion option:selected").val();
    var eventId = '1';
    var beginTime = $("#beginTime").val();
    var endTime = $("#endTime").val();
    if (appId == '') {
        alert("产品数据还未出现");
        return;
    } else if (appVersion == '') {
        alert("产品版本还未出现");
        return;
    } else {
        var list = [];//后台获取到的统计结果的集合
        $.ajax({
            url: baseURL + "reporttrack/getEventStatDetail",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
            // 数据发送方式
            type: "post",
            // 接受数据格式
            dataType: "json",
            //传递的数据
            data: {
                "appId": appId,
                "appVersion": appVersion,
                "eventId": eventId,
                "beginTime": beginTime,
                "endTime": endTime
            },
            //同步
            async: false,
            // 回调函数，接受服务器端返回给客户端的值，即result值
            success: function (data) {
                console.info(data);
                if (!data.data || data.data.length == 0) {
                    alert("无统计结果")
                    return;
                } else {
                    list = data.data;
                }
            },
            error: function (data) {
                alert("查询失败" + data);
            }
        })
        var timeArray = getAll(beginTime, endTime);
        var serieArray = [];
        for (var i = 0; i < versionArray.length; i++) {
            var obj = {};//series中的一个对象
            var name = versionArray[i];//对象中的name熟悉的值
            var datum = [];//对象中的数量数组
            for (var k = 0; k < timeArray.length; k++) {
                datum[k] = getNUmByVersionAndTime(timeArray[k], name, list);
            }
            obj.name = name;
            obj.data = datum;
            serieArray[i] = obj;
        }

        var chart = Highcharts.chart('container', {
            title: {
                text: '自定义事件消息数详情'
            },
            yAxis: {
                title: {
                    text: '消息数量'
                }
            },
            xAxis: {
                categories: timeArray
            },
            credits:{
                enabled:false
            },
            exporting:{
                enabled:false
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    }
                }
            },
            series: serieArray,
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
    }
}

/**
 * 根据日期条件从集合中筛选出符合条件的统计数据
 * @param day  日期
 * @param version 版本号
 * @param list    后台查询得到的统计数据集合
 */
function getNUmByVersionAndTime(day, version, list) {
    var number = 0;
    for (var j = 0; j < list.length; j++) {
        if (list[j].appVersion == version && list[j].time == day) {
            number =list[j].num;
        }
    }
    return number;
}


Date.prototype.format = function () {
    var s = '';
    var mouth = (this.getMonth() + 1) >= 10 ? (this.getMonth() + 1) : ('0' + (this.getMonth() + 1));
    var day = this.getDate() >= 10 ? this.getDate() : ('0' + this.getDate());
    s += this.getFullYear() + '-'; // 获取年份。
    s += mouth + "-"; // 获取月份。
    s += day; // 获取日。
    return (s); // 返回日期。
};


/**
 * 获取开始日期与结束日期之间所有的日期构成的数组，包括开始日期与结束日期
 * @param begin
 * @param end
 * @returns {Array}
 */
function getAll(begin, end) {
    var arr = [];
    var ab = begin.split("-");
    var ae = end.split("-");
    var db = new Date();
    db.setUTCFullYear(ab[0], ab[1] - 1, ab[2]);
    var de = new Date();
    de.setUTCFullYear(ae[0], ae[1] - 1, ae[2]);
    var unixDb = db.getTime() - 24 * 60 * 60 * 1000;
    var unixDe = de.getTime() - 24 * 60 * 60 * 1000;
    for (var k = unixDb; k <= unixDe;) {
        //console.log((new Date(parseInt(k))).format());
        k = k + 24 * 60 * 60 * 1000;
        arr.push((new Date(parseInt(k))).format());
    }
    return arr;
}


/**
 * js获取今天的日期 yyyy-mm-dd
 * @returns {*}
 */
function curDateTime() {
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    var date = d.getDate();
    var curDateTime = year;
    if (month > 9) {
        curDateTime = curDateTime + '-' + month;
    } else {
        curDateTime = curDateTime + '-' + "0" + month;
    }
    if (date > 9) {
        curDateTime = curDateTime + '-' + date;
    } else {
        curDateTime = curDateTime + '-' + "0" + date;
    }

    return curDateTime;

}


/**
 * js获取六天之前的日期
 * @returns {*}
 */
function sixDayBefore() {
    var d = new Date();
    d.setDate(d.getDate() - 6);
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    var date = d.getDate();
    var curDateTime = year;
    if (month > 9) {
        curDateTime = curDateTime + '-' + month;
    } else {
        curDateTime = curDateTime + '-' + "0" + month;
    }
    if (date > 9) {
        curDateTime = curDateTime + '-' + date;
    } else {
        curDateTime = curDateTime + '-' + "0" + date;
    }

    return curDateTime;

}


/**
 * 页面初始化
 */
function init() {
    getAppList();//初始化下拉框中的值
    $('#search_appId').change(function () {//为下拉框注册选中改变事件
        getAppVersionList();
    });
    $('#search_appId option:first').prop("selected", true);//默认选中产品下拉框中的第一个
    var selectedAppId = $('#search_appId option:first').text();//获取产品下拉框中被选中的值
    $('.filter-option.pull-left:first').text(selectedAppId);//将被选中的值展示在产品文本框中
    getAppVersionList();//刷新饼图


    $('#search_appVersion option:last').prop("selected", true);//默认选中版本下拉框中的第一个
    var selectedAppVersion = $('#search_appVersion option:last').text();//获取版本下拉框中被选中的值
    $('.filter-option.pull-left:last').text(selectedAppVersion);//将被选中的值展示在版本文本框中

    $('#beginTime').val(sixDayBefore());
    $('#endTime').val(curDateTime());


    freshLines();
}

init();