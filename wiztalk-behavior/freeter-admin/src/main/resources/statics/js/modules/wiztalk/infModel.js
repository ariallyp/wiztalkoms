$(function () {
    getAppList();//初始化下拉框中的值
    var appId=$('#search_appId').val();
    if(appId == ""){
        alert("产品类型不得为空");
        return;
    }
    console.info(vm.search.appId);
    $("#jqGrid").jqGrid({
        url: baseURL + 'infmodel/getModelStat',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true ,hidden:true},
			{ label: '机型', name: 'model', index: 'model', width: 80 },
			{ label: '用户数量', name: 'users', index: 'users', width: 80 },
			{ label: '启动次数', name: 'startTimes', index: 'start_times', width: 80 },
			{ label: '启动次数占比', name: 'statPercent', index: 'statPercent', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.list",
            page: "data.currPage",
            total: "data.totalPage",
            records: "data.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        postData:{appId:"10000000001"},
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
        search:{},
		infModel: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.infModel = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.infEvent.id == null ? "wiztalk/infmodel/save" : "wiztalk/infmodel/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.infEvent),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "wiztalk/infmodel/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "wiztalk/infmodel/info/"+id, function(r){
                vm.infEvent = r.infEvent;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
			    postData:vm.search,
                page:page
            }).trigger("reloadGrid");
		}
	}
});




/*获取产品列表*/
function getAppList() {
    $.ajax({
        url: baseURL + "wiztalk/infapp/getAppList",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : 'data',
        //同步
        async: false,
        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {
            console.info(data);
            if(!data.data||data.data.length==0){
                alert("暂无产品信息")
            }
            $.each(data.data, function(i) {
                $('#search_appId').append(
                    "<option value=" +data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });
            $.each(data.data, function(i) {
                $('#appId').append(
                    "<option value=" +data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });
            $('#appId').selectpicker('refresh');
            $('#appId').selectpicker('val', '');
            $('#appId').selectpicker('render');


            $('#search_appId').selectpicker('refresh');
            $('#search_appId').selectpicker('val', '');
            $('#search_appId').selectpicker('render');

            $('#search_appId option:first').prop("selected",true);//默认选中下拉框中的第一个
            var selectedValue=$('#search_appId option:first').text();//获取下拉框中被选中的值
            $('.filter-option.pull-left:first').text(selectedValue);//将被选中的值展示在文本框中

        },
        error : function(data) {
            alert("查询失败" + data);
        }
    })
}