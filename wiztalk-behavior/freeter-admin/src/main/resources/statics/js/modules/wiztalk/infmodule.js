/*获取产品列表*/
function getAppList() {
    $.ajax({
        url: baseURL + "wiztalk/infapp/getAppList",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : 'data',
        //同步
        async: false,
        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {
            console.info(data);
            if(!data.data||data.data.length==0){
                alert("暂无产品信息")
            }
            $('#search_appId').append(
                "<option value=''>所有产品 </option>");
            $.each(data.data, function(i) {
                $('#search_appId').append(
                    "<option value=" +data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });


            $('#appId').append(
                "<option value=''>所有产品 </option>");
            $.each(data.data, function(i) {
                $('#appId').append(
                    "<option value=" +data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });
            $('#appId').selectpicker('refresh');
            $('#appId').selectpicker('val', '');
            $('#appId').selectpicker('render');


            $('#search_appId').selectpicker('refresh');
            $('#search_appId').selectpicker('val', '');
            $('#search_appId').selectpicker('render');

        },
        error : function(data) {

            alert("查询失败" + data);

        }
    })
}

function bindAppId(){
    var selectAppId=vm.infModule.appId;
    if(typeof(selectAppId) == "undefined" || selectAppId  == ''){
        return ;
    }
    $('#appId').find("option[value = '"+selectAppId+"']").attr("selected","selected");
    var selectedValue= $('#appId').find("option[value = '"+selectAppId+"']").text();//获取下拉框中被选中的值
    $('.filter-option.pull-left:last').text(selectedValue);//将被选中的值展示在文本框中
}

$(function () {

    $("#search_appId").selectpicker({
        noneSelectedText : '请选择产品'  ,
        liveSearchPlaceholder : "请输入关键字",
        noneResultsText : "内容无法匹配"
    });

    getAppList();

    $("#jqGrid").jqGrid({
        url: baseURL + 'wiztalk/infmodule/page',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true ,hidden:true},
			{ label: '路径名称', name: 'moduleName', index: 'module_name', width: 80 },
			{ label: '路径备注', name: 'alias', index: 'alias', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		infModule: {},
        search:{},
        module_appId:''
	},
	methods: {
        validateModuleName:function(){
            if($.trim(vm.infModule.moduleName) == ''){
                $('#moduleName').val("路径名称不能为空");
                $('#moduleName')[0].focus();
            }
        },
        validateAlias:function(){
            if($.trim(vm.infModule.alias) == ''){
                $('#alias').val("事件名称不能为空");
                $('#alias')[0].focus();
            }
        },
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.infModule = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
            console.info(id);
			vm.showList = false;
            vm.title = "修改";
            vm.getInfo(id);
            bindAppId();
		},
		saveOrUpdate: function (event) {
			var url = vm.infModule.id == null ? "wiztalk/infmodule/save" : "wiztalk/infmodule/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.infModule),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "wiztalk/infmodule/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
            // $.get(baseURL + "wiztalk/infmodule/info/"+id, function(r){
            //     vm.infModule = r.infModule;
            // });
            $.ajax({
                url: baseURL + "wiztalk/infmodule/info/"+id,//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
                // 数据发送方式
                type : "get",
                // 接受数据格式
                dataType : "json",
                // 要传递的数据
                data : 'data',
                //同步
                async: false,
                // 回调函数，接受服务器端返回给客户端的值，即result值
                success : function(data) {
                    console.info(data);
                    vm.infModule = data.infModule;
                },
                error : function(data) {

                    alert("查询事件详细失败" + data);

                }
            })
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData:vm.search,
                page:page
            }).trigger("reloadGrid");
		}
	}
});