/*获取商城频道列表*/
function getchannelList() {//获取下拉频道列表
    $.ajax({
        url : baseURL +"wiztalk/reporterror/getAppVersion",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式  url: baseURL + 'wiztalk/reporterror/page',
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : 'data',

        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {
            console.info(data);
            if(!data.data||data.data.length==0){
                alert("没有查找到数据")
            }
            $('#search_channelId').append(
                "<option value=''>所有版本 </option>");
            $.each(data.data, function(i) {
                $('#search_channelId').append(
                    "<option value=" + data.data[i].appVersion + ">"
                    + data.data[i].appVersion + "</option>");
            });
            $('#search_channelId').selectpicker('refresh');
            $('#search_channelId').selectpicker('val', '');
            $('#search_channelId').selectpicker('render');

        },
        error : function(data) {

            alert("查询失败" + data);

        }
    })

}




$(function () {
    $("#search_channelId").selectpicker({
        noneSelectedText : '请选择版本'  ,
        liveSearchPlaceholder : "请输入关键字",
        noneResultsText : "内容无法匹配"
    });

    getchannelList();


    $("#jqGrid").jqGrid({
        url: baseURL + 'wiztalk/reporterror/page',
        datatype: "json",
        colModel: [
            { label: '手机型号', name: 'model', index: 'model', width: 20 },
            { label: '异常', name: 'text', index: 'text', width: 140 },
            { label: '上报时间', name: 'time', index: 'time', width: 30 },
            { label: '发生次数', name: 'exceptionTimes', index: 'exceptionTimes', width: 20 },
            { label: '影响用户', name: 'exceptionEffects', index: 'exceptionEffects', width: 20 }

        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        showList: true,
        title: null,
        reportError: {},
		search:{}
    },
    methods: {
        query: function () {
            vm.search.channelId =$('#search_channelId').selectpicker('val');
            vm.reload();
        },

        getInfo: function(id){
            $.get(baseURL + "wiztalk/reporterror/info/"+id, function(r){
                vm.reportError = r.reportError;
            });
        },
        reload: function (event) {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                postData:vm.search,
                page:page
            }).trigger("reloadGrid");
        }
    }
});