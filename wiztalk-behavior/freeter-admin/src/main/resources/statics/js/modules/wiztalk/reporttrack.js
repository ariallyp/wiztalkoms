$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'wiztalk/reporttrack/page',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '页面路径', name: 'eventPath', index: 'event_path', width: 80 }, 			
			{ label: '自定义事件名称', name: 'eventName', index: 'event_name', width: 80 }, 			
			{ label: '文件ID', name: 'fid', index: 'fid', width: 80 }, 			
			{ label: '手机型号', name: 'model', index: 'model', width: 80 }, 			
			{ label: '与手机相关的参数', name: 'mac', index: 'mac', width: 80 }, 			
			{ label: '与手机相关的参数', name: 'imsi', index: 'imsi', width: 80 }, 			
			{ label: '与手机相关的参数', name: 'imei', index: 'imei', width: 80 }, 			
			{ label: '设备名称', name: 'deviceName', index: 'device_name', width: 80 }, 			
			{ label: '设备ID', name: 'deviceId', index: 'device_id', width: 80 }, 			
			{ label: '手机系统', name: 'os', index: 'os', width: 80 }, 			
			{ label: '手机品牌', name: 'vendor', index: 'vendor', width: 80 }, 			
			{ label: '用户ID', name: 'uid', index: 'uid', width: 80 }, 			
			{ label: '网络类型', name: 'netType', index: 'net_type', width: 80 }, 			
			{ label: 'app名称', name: 'appName', index: 'app_name', width: 80 }, 			
			{ label: 'app版本', name: 'appVersion', index: 'app_version', width: 80 }, 			
			{ label: '上报信息的类型', name: 'type', index: 'type', width: 80 }, 			
			{ label: '上报时间', name: 'time', index: 'time', width: 80 }, 			
			{ label: '模块id', name: 'moduleId', index: 'module_id', width: 80 }, 			
			{ label: '到达跟踪分析平台的时间', name: 'reachTime', index: 'reach_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		reportTrack: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.reportTrack = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.reportTrack.id == null ? "wiztalk/reporttrack/save" : "wiztalk/reporttrack/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.reportTrack),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "wiztalk/reporttrack/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "wiztalk/reporttrack/info/"+id, function(r){
                vm.reportTrack = r.reportTrack;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});