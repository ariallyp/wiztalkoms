$(function () {
    $("#bootstrap-table").bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url : baseURL + 'wiztalk/statisdevice/page', // 服务器数据的加载地址
        showRefresh : true,
        showToggle : true,
		showColumns : true,
		showExport :true,
		sortName:"id",// 排序列名称
		sortOrder: "desc", 
		sortable: true,
		toolbar : '#exampleToolbar',
 		striped : true, // 设置为true会有隔行变色效果
		dataType : "json", // 服务器返回的数据类型
		pagination : true, // 设置为true会在底部显示分页条
		// queryParamsType : "limit",
		// //设置为limit则会发送符合RESTFull格式的参数
		singleSelect : false, // 设置为true将禁止多选
		// contentType : "application/x-www-form-urlencoded",
		// //发送到服务器的数据编码类型
		pageSize : 10, // 如果设置了分页，每页数据条数
		pageNumber : 1, // 如果设置了分布，首页页码
		// search : true, // 是否显示搜索框
		sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
		// "server"    
		queryParams : function(params) {
 					return {
							// 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
							limit : params.limit,
						    offset : params.offset,
							username : vm.q.username,
							sidx: params.sort, //排序列名  
							order: params.order //排位命令（desc，asc） 
							/* sort:"userId",
							order:"desc" */
 													};   
 		},
 		
        columns: [		
        {
				checkbox : true
		},	
 
 		{
				field : 'id', // 列字段名
				title : 'id', // 列标题
				sortable: true
		},
			 		
 
 		{
				field : 'deviceModel', // 列字段名
				title : '机型', // 列标题
				sortable: true
		},
			 		
 
 		{
				field : 'userNum', // 列字段名
				title : '用户数', // 列标题
				sortable: true
		},
			 		
 
 		{
				field : 'startTimes', // 列字段名
				title : '启动次数', // 列标题
				sortable: true
		},
			 		
 
 		{
				field : 'startTimesPer', // 列字段名
				title : '启动次数占比', // 列标题
				sortable: true
		},
			 		

		{
				title : '操作',
				field : 'id',
				align : 'center',
				formatter : function(value, row,index) 
				{
					var e = '<a  class="btn btn-primary btn-sm '
						+ ''
					    + '" href="#" mce_href="#" title="编辑" onclick="edit(\''
					    + row.id
					    + '\')"><i class="fa fa-edit "></i></a> ';
					var d = '<a class="btn btn-warning btn-sm '
						+ ''
						+ '" href="#" title="删除"  mce_href="#" onclick="remove(\''
						+ row.id
						+ '\')"><i class="fa fa-remove"></i></a> ';
															 
					return e + d;
																		
				}
		}
        ],
		
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q : {
					username : null
				},
		showList: true,
		title: null,
		statisDevice: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.statisDevice = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            vm.getInfo(id.id);
 		},
		saveOrUpdate: function (event) {
			var url = vm.statisDevice.id == null ? "wiztalk/statisdevice/save" : "wiztalk/statisdevice/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.statisDevice),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
				ids = $.map(ids, function(row) {
						return row.id;
					});
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "wiztalk/statisdevice/delete",
                    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(id){
			$.get(baseURL + "wiztalk/statisdevice/info/"+id, function(r){
                vm.statisDevice = r.statisDevice;
            });
		},
		reload: function (event) {
			vm.showList = true;
			$('#bootstrap-table').bootstrapTable('refresh');
		}
	}
});


		function edit(id) {

			layer.open({
				type : 2,
				title : '编辑',
				maxmin : true,
				//shadeClose : false, // 点击遮罩关闭层
				area : [ '800px', '520px' ],
				content : "statisDeviceEdit.html?id=" + id // iframe的url
			});
		}

		function remove(id) {
			var arr = new Array();
			arr.push(id);
			confirm('确定要删除选中的记录？', function() {

				$.ajax({
					type : "POST",
					url : baseURL + "wiztalk/statisdevice/delete",
					contentType : "application/json",
					data : JSON.stringify(arr),
					success : function(r) {
						if (r.code == 0) {
							alert('操作成功', function() {
								vm.reload();
							});
						} else {
							alert(r.msg);
						}
					}
				});
			});
		}