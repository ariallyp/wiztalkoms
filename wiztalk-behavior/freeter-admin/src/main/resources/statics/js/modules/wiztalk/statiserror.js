
function getchannelList() {//获取下拉频道列表
    $.ajax({
        url : baseURL +"wiztalk/reporterror/getAppVersion",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式  url: baseURL + 'wiztalk/reporterror/page',
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : 'data',

        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {

            if(!data.data||data.data.length==0){
                alert("没有查找到数据")
            }
            console.info(data);
            $('#search_channelId').append(
                "<option value=''>所有版本 </option>");
            $.each(data.data, function(i) {
                $('#search_channelId').append(
                    "<option value=" + data.data[i].appVersion + ">"
                    + data.data[i].appVersion + "</option>");
            });
            $('#search_channelId').selectpicker('refresh');
            $('#search_channelId').selectpicker('val', '');
            $('#search_channelId').selectpicker('render');

        },
        error : function(data) {

            alert("查询失败" + data);

        }
    })

}

$(function () {
    getStatisticDataList();
   /* getErrorData('','','');*/
    $("#search_channelId").selectpicker({
        noneSelectedText : '请选择版本'  ,
        liveSearchPlaceholder : "请输入关键字",
        noneResultsText : "内容无法匹配"
    });

    getchannelList();
});

function getStatisticDataList(){
    var startDate =$('#beginTime').val();
    var endDate =$('#endTime').val();
    var appVersion=$('#search_channelId').val();

    var   objNames="";
    var    objNumbers=""
    var myArray=new Array();

    // 基于准备好的dom，初始化echarts图表
    var myChart = echarts.init(document.getElementById('app4'));
    var container = echarts.init(document.getElementById('container'));

  var  option ={
        title: {
            text: '错误趋势图 '
        },
        tooltip: {},
        legend: {
            data:['错误次数']
        },

        xAxis: {
            data: objNames
        },
        yAxis: {},
        series: [
            {
                name: '错误次数',
                type: 'bar',
                itemStyle: {
                    normal: {
                        /*柱形图上每个柱子随机颜色*/
                        color: function (params){
                            var colorList = ['rgb(21,174,103)','rgb(21,105,164)','rgb(255,255,0)','rgb(234,85,32)'];
                            return colorList[params.dataIndex];
                        },
                        /*柱形图上每个柱子顶端中间显示的数据*/
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{c}'
                        }
                    }
                },
                data: objNumbers
            }
        ]
    };
  var  option2 = {
        title : {
            text: '错误趋势图',
            subtext: '所占比例',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient : 'vertical',
            x : 'left',
            data:objNames
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {
                    show: true,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            x: '25%',
                            width: '50%',
                            funnelAlign: 'left',
                            max: 1548
                        }
                    }
                },
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        series : [
            {
                name:'访问来源',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:myArray

            }
        ]
    };


    $.ajax({
        url: baseURL +"wiztalk/statiserror/getStaticData",
        type: "post",
        data : {startDate:startDate,endDate:endDate,appVersion:appVersion},
        datatype: "json",
        success: function (res) {
            console.info(res);
            for(var i in res){
                objNames=res[i].appNames;
                objNumbers=res[i].appNumbers;;
                 myArray=res[i].appList;
            }
            console.info(myArray);
            option.xAxis.data=objNames;
            option.series[0].data=objNumbers;

            option2.legend.data=objNames;
            option2.series[0].data=myArray;

            myChart.setOption(option);
            container.setOption(option2);

        }
    })
}



var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		statisError: {},
        search:{}
	},
	methods: {
		query: function () {
            vm.search.beginTime =$('#beginTime').val();

            vm.search.endTime =$('#endTime').val();
            vm.search.appVersion=$('#search_channelId').val();
			vm.reload();
		},
		    reload: function (event) {
			vm.showList = true;
			getStatisticDataList();
		}
	}
});