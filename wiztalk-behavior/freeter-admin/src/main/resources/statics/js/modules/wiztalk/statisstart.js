$(function () {
    getStatisticDataList();
    $("#bootstrap-table").bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url : baseURL + 'wiztalk/statisstart/page', // 服务器数据的加载地址
        showRefresh : true,
        showToggle : true,
        showColumns : true,
        showExport :true,
        sortName:"id",// 排序列名称
        sortOrder: "desc",
        sortable: true,
        toolbar : '#exampleToolbar',
        striped : true, // 设置为true会有隔行变色效果
        dataType : "json", // 服务器返回的数据类型
        pagination : true, // 设置为true会在底部显示分页条
        // queryParamsType : "limit",
        // //设置为limit则会发送符合RESTFull格式的参数
        singleSelect : false, // 设置为true将禁止多选
        // contentType : "application/x-www-form-urlencoded",
        // //发送到服务器的数据编码类型
        pageSize : 10, // 如果设置了分页，每页数据条数
        pageNumber : 1, // 如果设置了分布，首页页码
        // search : true, // 是否显示搜索框
        sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
        // "server"
        queryParams : function(params) {
            return {
                // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                limit : params.limit,
                offset : params.offset,
                username : vm.q.username,
                sidx: params.sort, //排序列名
                order: params.order //排位命令（desc，asc）
                /* sort:"userId",
                order:"desc" */
            };
        },

        columns: [


            {
                field : 'id', // 列字段名
                title : '服务数', // 列标题
                sortable: true
            },


            {
                field : 'startTimes', // 列字段名
                title : '启动次数', // 列标题
                sortable: true
            },
            {
                field : 'startTimes', // 列字段名
                title : '当前在线用户', // 列标题
                sortable: true
            }




        ],

    });

});

function getStatisticDataList()
{
    var trainType="";
    var groupBy="";
/*
    var groupBy=document.getElementById("groupBy").value;
*/
    var data1=[];
    var data2=[];
    // 基于准备好的dom，初始化echarts图表
    var myChart = echarts.init(document.getElementById('main'));
    option = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data:['降水量1','折线2']
        },
        xAxis: [
            {
                type: 'category',
                data: data1
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '水量/ml',
                min: 0,
                max: 150,
                interval: 50,
                axisLabel: {
                    formatter: '{value} '
                }
            }
        ],
        series: [

            {
                name:'降水量1',
                type:'bar',
                /*设置柱状图颜色*/
                itemStyle: {
                    normal: {
                        color:'#4bbbee',
                        /*柱形图上每个柱子顶端中间显示的数据*/
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{b}\n{c}'
                        }
                    }
                },
                data: data2
            },
            {
                name:'折线2',
                type:'line',
                itemStyle : {  /*设置折线颜色*/
                    normal : {
                        color:'#c5ee4a'
                    }
                },
                data: data2
            }
        ]
    };

    $.ajax({
        url: baseURL +"wiztalk/statisstart/getStaticData",
        type: "post",
        data: {"trainType":trainType,"groupBy":groupBy},
        datatype: "json",
        success: function (res) {

            for(var i in res){
                data1.push(i);
                data2.push(res[i]);
            }
            myChart.setOption(option);
        }
    })
}

var vm = new Vue({
    el:'#rrapp',
    data:{
        q : {
            username : null
        },
        showList: true,
        title: null,
        statisStart: {}
    },
    methods: {
        query: function () {
            vm.reload();
        },

        reload: function (event) {
            getStatisticDataList();
            vm.showList = true;
            $('#bootstrap-table').bootstrapTable('refresh');
        }
    }
});



