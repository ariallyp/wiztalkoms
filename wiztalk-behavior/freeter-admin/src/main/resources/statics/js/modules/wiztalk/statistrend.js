$(function () {

    getchannelList();
    getStatisticDataList();
    $("#search_channelId").selectpicker({
        noneSelectedText : '请选择版本'  ,
        liveSearchPlaceholder : "请输入关键字",
        noneResultsText : "内容无法匹配"
    });



});


function getStatisticDataList(){
    var startDate =$('#beginTime').val();
    var endDate =$('#endTime').val();
    var appVersion=$('#search_channelId').val();
    var userAddList="";
    var userUserList="";
    var userStartList="";
    var   dateList="";


    // 基于准备好的dom，初始化echarts图表
    var myChart = echarts.init(document.getElementById('app4'));
    var container = echarts.init(document.getElementById('container'));

  var  option = {
        // 定义样式和数据
        title: {
            text: '趋势统计图 '
        },
        backgroundColor: '#FBFBFB',
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['新增', '活跃', '启动']
        },

        calculable: true,


        xAxis: [{
            axisLabel: {
                rotate: 30,
                interval: 0
            },
            axisLine: {
                lineStyle: {
                    color: 'red'
                }
            },
            type: 'category',
            boundaryGap: false,
            data: dateList
        }],
        yAxis: [{

            type: 'value',
            axisLine: {
                lineStyle: {
                    color: '#CECECE'
                }
            }
        }],
        series: [
            {
                name: '新增',
                type: 'line',
                symbol: 'none',
                smooth: 0.3,
                color: ['#66AEDE'],
                data: userAddList
            },
            {
                name: '活跃',
                type: 'line',
                symbol: 'none',
                smooth: 0.5,
                color: ['#3CB371'],
                data: userUserList
            },
            {
                name: '启动',
                type: 'line',
                symbol: 'none',
                smooth: 0.8,
                color: ['#98F5FF'],
                data: userStartList
            }]
    };


    $.ajax({
        url: baseURL +"wiztalk/statisuser/getTrendDayData",
        type: "post",
        data : {startDate:startDate,endDate:endDate,appVersion:appVersion},
        datatype: "json",
        success: function (res) {
            console.info(res);
            for(var i in res){
                dateList=res[i].dateList;
                userAddList=res[i].userAdd;
                userUserList=res[i].userUser;
                userStartList=res[i].userStart;
            }
            option.xAxis[0].data=dateList;
            option.series[0].data=userAddList;
            option.series[1].data=userUserList;
            option.series[2].data=userStartList;
            myChart.setOption(option);

        }

    })


}


function getchannelList() {//获取下拉频道列表
    $.ajax({
        url : baseURL +"wiztalk/statisstart/getAppListData",//写你自己的方法，返回map，我返回的map包含了两个属性：data：集合，total：集合记录数量，所以后边会有data.data的写法。。。
        // 数据发送方式  url: baseURL + 'wiztalk/reporterror/page',
        type : "get",
        // 接受数据格式
        dataType : "json",
        // 要传递的数据
        data : 'data',
        // 回调函数，接受服务器端返回给客户端的值，即result值
        success : function(data) {
            if(!data.data||data.data.length==0){
                alert("没有查找到数据")
            }
            console.info(data);
            $('#search_channelId').append(
                "<option value=''>所有版本 </option>");
            $.each(data.data, function(i) {
                $('#search_channelId').append(
                    "<option value=" + data.data[i].id + ">"
                    + data.data[i].appName + "</option>");
            });
            $('#search_channelId').selectpicker('refresh');
            $('#search_channelId').selectpicker('val', '');
            $('#search_channelId').selectpicker('render');

        },
        error : function(data) {
            alert("查询失败" + data);
        }
    })

}





var vm = new Vue({
	el:'#rrapp',
	data:{
		q : {
					username : null
				},
		showList: true,
		title: null,
		statisStart: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},

		reload: function (event) {
			vm.showList = true;
			$('#bootstrap-table').bootstrapTable('refresh');
            getStatisticDataList();
        }
	}
});


