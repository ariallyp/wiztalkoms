$(function () {
    getStatisticDataList();
    $("#bootstrap-table").bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url : baseURL + 'wiztalk/statisuser/page', // 服务器数据的加载地址
        showRefresh : true,
        showToggle : true,
        showColumns : true,
        showExport :true,
        sortName:"id",// 排序列名称
        sortOrder: "desc",
        sortable: true,
        toolbar : '#exampleToolbar',
        striped : true, // 设置为true会有隔行变色效果
        dataType : "json", // 服务器返回的数据类型
        pagination : false, // 设置为true会在底部显示分页条
        // queryParamsType : "limit",
        // //设置为limit则会发送符合RESTFull格式的参数
        singleSelect : false, // 设置为true将禁止多选
        // contentType : "application/x-www-form-urlencoded",
        // //发送到服务器的数据编码类型
        pageSize : 10, // 如果设置了分页，每页数据条数
        pageNumber : 1, // 如果设置了分布，首页页码
        // search : true, // 是否显示搜索框
        sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
        // "server"
        queryParams : function(params) {
            return {
                // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                limit : params.limit,
                offset : params.offset,
                username : vm.q.username,
                sidx: params.sort, //排序列名
                order: params.order //排位命令（desc，asc）
                /* sort:"userId",
                order:"desc" */
            };
        },

        columns: [
            {
                field : 'userAdd', // 列字段名
                title : '新增用户', // 列标题
                sortable: true
            },

            {
                field : 'userUsed', // 列字段名
                title : '使用用户', // 列标题
                sortable: true
            },
            {
                field : 'userStart', // 列字段名
                title : '启动次数', // 列标题
                sortable: true
            }

        ],

    });
});




function getStatisticDataList()
{
    var dateList="";

    var userAddList="";
    var userAddListYesterday="";
    var userAddListWeek="";
    var userAddListMonth="";

    var userUserList="";
    var userListYesterday="";
    var userListWeek="";
    var userListMonth="";

    var userStartList="";
    var userStartListYesterday="";
    var userStartListWeek="";
    var userStartListMonth="";

    // 基于准备好的dom，初始化echarts图表
    var myChart = echarts.init(document.getElementById('main'));
    var myChart2 = echarts.init(document.getElementById('main2'));
    var myChart3 = echarts.init(document.getElementById('main3'));
    $.ajax({
        url: baseURL +"wiztalk/statisuser/getAllStaticHourData",
        type: "post",
        datatype: "json",
        success: function (res) {
                console.info(res);
            for(var i in res){
                dateList=res[i].dateList;

                 userAddList=res[i].userAdd;
                 userAddListYesterday=res[i].userAddYesterday;
                 userAddListWeek=res[i].userAddWeek;
                 userAddListMonth=res[i].userAddMonth;

                 userUserList=res[i].userUser;
                 userListYesterday=res[i].userListYesterday;
                 userListWeek=res[i].userListWeek;
                 userListMonth=res[i].userListMonth;

                 userStartList=res[i].userStart;
                 userStartListYesterday=res[i].startListYesterday;
                 userStartListWeek=res[i].startListWeek;
                 userStartListMonth=res[i].startListMonth;
            }

            var option = {
                // 定义样式和数据
                title: {
                    text: '新增用户实时统计图 '
                },
                backgroundColor: '#FBFBFB',
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['今天', '昨天', '一周','一月']
                },

                calculable: true,


                xAxis: [{
                    axisLabel: {
                        rotate: 30,
                        interval: 0
                    },
                    axisLine: {
                        lineStyle: {
                            color: 'red'
                        }
                    },
                    type: 'category',
                    boundaryGap: false,
                    data: dateList
                }],
                yAxis: [{

                    type: 'value',
                    axisLine: {
                        lineStyle: {
                            color: '#CECECE'
                        }
                    }
                }],
                series: [
                    {
                    name: '今天',
                    type: 'line',
                    symbol: 'none',
                    smooth: 0.3,
                    color: ['#66AEDE'],
                    data: userAddList
                     },
                    {
                    name: '昨天',
                    type: 'line',
                    symbol: 'none',
                    smooth: 0.5,
                    color: ['#3CB371'],
                    data: userAddListYesterday
                     },
                    {
                    name: '一周',
                    type: 'line',
                    symbol: 'none',
                    smooth: 0.8,
                    color: ['#98F5FF'],
                    data: userAddListWeek
                }, {
                name: '一月',
                    type: 'line',
                    symbol: 'none',
                    smooth: 0.8,
                    color: ['#FFC1C1'],
                    data: userAddListMonth
            }]
            };
            var option2 = {
                // 定义样式和数据
                title: {
                    text: '使用用户实时统计图 '
                },
                backgroundColor: '#FBFBFB',
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['今天', '昨天', '一周','一月']
                },

                calculable: true,


                xAxis: [{
                    axisLabel: {
                        rotate: 30,
                        interval: 0
                    },
                    axisLine: {
                        lineStyle: {
                            color: 'red'
                        }
                    },
                    type: 'category',
                    boundaryGap: false,
                    data: dateList
                }],
                yAxis: [{

                    type: 'value',
                    axisLine: {
                        lineStyle: {
                            color: '#CECECE'
                        }
                    }
                }],
                series: [
                    {
                        name: '今天',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.3,
                        color: ['#66AEDE'],
                        data:   userUserList

                    },
                    {
                        name: '昨天',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.5,
                        color: ['#3CB371'],
                        data:  userListYesterday
                    },
                    {
                        name: '一周',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.8,
                        color: ['#98F5FF'],
                        data: userListWeek
                    }, {
                        name: '一月',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.8,
                        color: ['#FFC1C1'],
                        data: userListMonth
                    }]
            };
            var option3 = {
                // 定义样式和数据
                title: {
                    text: '启动用户实时统计图 '
                },
                backgroundColor: '#FBFBFB',
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['今天', '昨天', '一周','一月']
                },

                calculable: true,


                xAxis: [{
                    axisLabel: {
                        rotate: 30,
                        interval: 0
                    },
                    axisLine: {
                        lineStyle: {
                            color: 'red'
                        }
                    },
                    type: 'category',
                    boundaryGap: false,
                    data: dateList
                }],
                yAxis: [{

                    type: 'value',
                    axisLine: {
                        lineStyle: {
                            color: '#CECECE'
                        }
                    }
                }],
                series: [
                    {
                        name: '今天',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.3,
                        color: ['#66AEDE'],
                        data: userStartList
                    },
                    {
                        name: '昨天',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.5,
                        color: ['#3CB371'],
                        data: userStartListYesterday
                    },
                    {
                        name: '一周',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.8,
                        color: ['#98F5FF'],
                        data: userStartListWeek
                    }, {
                        name: '一月',
                        type: 'line',
                        symbol: 'none',
                        smooth: 0.8,
                        color: ['#FFC1C1'],
                        data: userStartListMonth
                    }]
            };

            myChart.setOption(option);
            myChart2.setOption(option2);
            myChart3.setOption(option3);
        }
    })
}





var vm = new Vue({
    el:'#rrapp',
    data:{
        q : {
            username : null
        },
        showList: true,
        title: null,
        statisStart: {}
    },
    methods: {
        query: function () {
            vm.reload();
        },
        reload: function (event) {
            getStatisticDataList();
            vm.showList = true;
            $('#bootstrap-table').bootstrapTable('refresh');
        }
    }
});



