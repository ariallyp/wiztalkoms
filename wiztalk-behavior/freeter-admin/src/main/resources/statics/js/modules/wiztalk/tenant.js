$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'wiztalk/tenant/page',
        datatype: "json",
        colModel: [			
			{ label: 'tenantId', name: 'tenantId', index: 'tenant_id', width: 50, key: true },
			{ label: '', name: 'code', index: 'code', width: 80 }, 			
			{ label: '', name: 'tenantName', index: 'tenant_name', width: 80 }, 			
			{ label: '', name: 'status', index: 'status', width: 80 }, 			
			{ label: '', name: 'customerId', index: 'customer_id', width: 80 }, 			
			{ label: '', name: 'created', index: 'created', width: 80 }, 			
			{ label: '', name: 'updated', index: 'updated', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		tenant: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.tenant = {};
		},
		update: function (event) {
			var tenantId = getSelectedRow();
			if(tenantId == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(tenantId)
		},
		saveOrUpdate: function (event) {
			var url = vm.tenant.tenantId == null ? "wiztalk/tenant/save" : "wiztalk/tenant/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.tenant),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var tenantIds = getSelectedRows();
			if(tenantIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "wiztalk/tenant/delete",
                    contentType: "application/json",
				    data: JSON.stringify(tenantIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(tenantId){
			$.get(baseURL + "wiztalk/tenant/info/"+tenantId, function(r){
                vm.tenant = r.tenant;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});