package com.freeter.common.util;

public class Constant {

	/*
	 * redis 短信yanzhengma
	 */
	public final static String SMS_CODE_KEY = "code";
	
	public final static String SMS_OVERTIME_KEY = "over";

	public final static String RESET_PASS_SMS_CODE_KEY="resetPassCode";

	public final static String RESET_PASS_SMS_OVERTIME_KEY="resetPassCode";

    /** 超级管理员ID */
    public static final int SUPER_ADMIN = 1;
    /** 数据权限过滤 */
    public static final String SQL_FILTER = "sql_filter";
    /**横杠*/
    public static final String RUNG = "-";
    /**事件停用状态*/
    public static final String INFEVENT_STATUS_DISABLE = "0";
    /**事件启用状态*/
    public static final String INFEVENT_STATUS_ENABLE = "1";
    /**路径停用状态*/
    public static final String INFMODULE_STATUS_DISABLE = "0";
    /**路径启用状态*/
    public static final String INFMODULE_STATUS_ENABLE = "1";
    /**机型停用状态*/
    public static final String INFMODEL_STATUS_DISABLE = "0";
    /**机型启用状态*/
    public static final String INFMODEL_STATUS_ENABLE = "1";

    /** app_id的字段名*/
    public  static final String APPID_COLUMN = "app_id";
    /** event_name的字段名*/
    public  static final String EVENTNAME_COLUMN = "event_name";
    /** event_name的字段名*/
    public static final String MODULENAME_COLUMN = "module_name";
    /** alias的字段名*/
    public static final String ALIAS_COLUMN = "alias";
    /** id的字段名*/
    public  static final String ID_COLUMN = "id";
    /** status的字段名*/
    public static final String STATUS_COLUMN = "status";
    /** model的字段名*/
    public static final String MODEL_COLUMN = "status";

}
