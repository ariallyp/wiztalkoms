/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.freeter.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * redis 初始化
 *
 * @author cyl
 * @since 2018-09-10
 */
@Component
@Order(value = 100)
public class RedisInit implements ApplicationRunner {

    @Autowired
    private RedisTool redisTool;
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        redisTool.initModuleMap();
        redisTool.initEventMap();
        System.out.println("这里做reids的初始化");
    }

}
