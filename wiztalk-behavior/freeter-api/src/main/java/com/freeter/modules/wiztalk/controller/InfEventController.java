package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.freeter.common.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.modules.wiztalk.entity.model.InfEventModel;
import com.freeter.modules.wiztalk.entity.vo.InfEventVO;
import com.freeter.common.utils.MPUtil;


import com.freeter.modules.wiztalk.entity.InfEventEntity;
import com.freeter.modules.wiztalk.service.InfEventService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;




/**
 * 自定义事件表
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 10:03:21
 */
@RestController
@RequestMapping("infevent")

public class InfEventController {
    @Autowired
    private InfEventService infEventService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    public R page(@RequestParam Map<String, Object> params,InfEventModel infEventModel){
 
        EntityWrapper< InfEventEntity> ew = new EntityWrapper< InfEventEntity>();
        InfEventEntity infEvent = new  InfEventEntity( infEventModel);
     	ew.allEq(MPUtil.allEQMapPre( infEvent, "infEvent")); 
    	PageUtils page = infEventService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    public R list(InfEventModel infEventModel){
		ValidatorUtils.validateEntity(infEventModel);
        EntityWrapper< InfEventEntity> ew = new EntityWrapper< InfEventEntity>();
		InfEventEntity infEvent = new  InfEventEntity( infEventModel);
     	ew.allEq(MPUtil.allEQMapPre( infEvent, "infEvent")); 
		List<InfEventVO>  infEventVOList =  infEventService.selectListVO(ew);
		return R.ok("查询自定义事件表成功").put("data", infEventVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    public R query(InfEventModel infEventModel){
		ValidatorUtils.validateEntity(infEventModel);
        EntityWrapper< InfEventEntity> ew = new EntityWrapper< InfEventEntity>();
		InfEventEntity infEvent = new  InfEventEntity( infEventModel);
		ew.allEq(MPUtil.allEQMapPre( infEvent, "infEvent")); 
		InfEventVO  infEventVO =  infEventService.selectVO(ew);
		return R.ok("查询自定义事件表成功").put("data",  infEventVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        InfEventEntity infEvent = infEventService.selectById(id);

        return R.ok().put("infEvent", infEvent);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    public R saveJson(@RequestBody InfEventEntity infEvent){
    	ValidatorUtils.validateEntity(infEvent);
        infEventService.insert(infEvent);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    public R saveForm(InfEventEntity infEvent){
    	ValidatorUtils.validateEntity(infEvent);
        infEventService.insert(infEvent);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    public R updateJson(@RequestBody InfEventEntity infEvent){
        ValidatorUtils.validateEntity(infEvent);
        infEventService.updateAllColumnById(infEvent);//全部更新
        
        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    public R updateForm(InfEventEntity infEvent){
        ValidatorUtils.validateEntity(infEvent);
        infEventService.updateAllColumnById(infEvent);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody String[] ids){
        infEventService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
