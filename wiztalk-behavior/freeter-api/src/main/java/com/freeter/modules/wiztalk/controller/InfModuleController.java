package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.freeter.common.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.modules.wiztalk.entity.model.InfModuleModel;
import com.freeter.modules.wiztalk.entity.vo.InfModuleVO;
import com.freeter.common.utils.MPUtil;


import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import com.freeter.modules.wiztalk.service.InfModuleService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;




/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-12 18:06:44
 */
@RestController
@RequestMapping("infmodule")
public class InfModuleController {
    @Autowired
    private InfModuleService infModuleService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    public R page(@RequestParam Map<String, Object> params,InfModuleModel infModuleModel){
 
        EntityWrapper< InfModuleEntity> ew = new EntityWrapper< InfModuleEntity>();
        InfModuleEntity infModule = new  InfModuleEntity( infModuleModel);
     	ew.allEq(MPUtil.allEQMapPre( infModule, "infModule")); 
    	PageUtils page = infModuleService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    public R list(InfModuleModel infModuleModel){
		ValidatorUtils.validateEntity(infModuleModel);
        EntityWrapper< InfModuleEntity> ew = new EntityWrapper< InfModuleEntity>();
		InfModuleEntity infModule = new  InfModuleEntity( infModuleModel);
     	ew.allEq(MPUtil.allEQMapPre( infModule, "infModule")); 
		List<InfModuleVO>  infModuleVOList =  infModuleService.selectListVO(ew);
		return R.ok("查询页面表，存储页面汉字名称，英文名称，对应的appId等信息。成功").put("data", infModuleVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    public R query(InfModuleModel infModuleModel){
		ValidatorUtils.validateEntity(infModuleModel);
        EntityWrapper< InfModuleEntity> ew = new EntityWrapper< InfModuleEntity>();
		InfModuleEntity infModule = new  InfModuleEntity( infModuleModel);
		ew.allEq(MPUtil.allEQMapPre( infModule, "infModule")); 
		InfModuleVO  infModuleVO =  infModuleService.selectVO(ew);
		return R.ok("查询页面表，存储页面汉字名称，英文名称，对应的appId等信息。成功").put("data",  infModuleVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        InfModuleEntity infModule = infModuleService.selectById(id);

        return R.ok().put("infModule", infModule);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    public R saveJson(@RequestBody InfModuleEntity infModule){
    	ValidatorUtils.validateEntity(infModule);
        infModuleService.insert(infModule);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    public R saveForm(InfModuleEntity infModule){
    	ValidatorUtils.validateEntity(infModule);
        infModuleService.insert(infModule);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    public R updateJson(@RequestBody InfModuleEntity infModule){
        ValidatorUtils.validateEntity(infModule);
        infModuleService.updateAllColumnById(infModule);//全部更新
        
        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    public R updateForm(InfModuleEntity infModule){
        ValidatorUtils.validateEntity(infModule);
        infModuleService.updateAllColumnById(infModule);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody String[] ids){
        infModuleService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
