package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.freeter.common.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.modules.wiztalk.entity.model.ReportErrorModel;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorVO;
import com.freeter.common.utils.MPUtil;


import com.freeter.modules.wiztalk.entity.ReportErrorEntity;
import com.freeter.modules.wiztalk.service.ReportErrorService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;




/**
 * 错误日志表
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:48
 */
@RestController
@RequestMapping("mobile/reportError")
public class ReportErrorController {
    @Autowired
    private ReportErrorService reportErrorService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    public R page(@RequestParam Map<String, Object> params,ReportErrorModel reportErrorModel){
 
        EntityWrapper< ReportErrorEntity> ew = new EntityWrapper< ReportErrorEntity>();
        ReportErrorEntity reportError = new  ReportErrorEntity( reportErrorModel);
     	ew.allEq(MPUtil.allEQMapPre( reportError, "reportError")); 
    	PageUtils page = reportErrorService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    public R list(ReportErrorModel reportErrorModel){
		ValidatorUtils.validateEntity(reportErrorModel);
        EntityWrapper< ReportErrorEntity> ew = new EntityWrapper< ReportErrorEntity>();
		ReportErrorEntity reportError = new  ReportErrorEntity( reportErrorModel);
     	ew.allEq(MPUtil.allEQMapPre( reportError, "reportError")); 
		List<ReportErrorVO>  reportErrorVOList =  reportErrorService.selectListVO(ew);
		return R.ok("查询错误日志表成功").put("data", reportErrorVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    public R query(ReportErrorModel reportErrorModel){
		ValidatorUtils.validateEntity(reportErrorModel);
        EntityWrapper< ReportErrorEntity> ew = new EntityWrapper< ReportErrorEntity>();
		ReportErrorEntity reportError = new  ReportErrorEntity( reportErrorModel);
		ew.allEq(MPUtil.allEQMapPre( reportError, "reportError")); 
		ReportErrorVO  reportErrorVO =  reportErrorService.selectVO(ew);
		return R.ok("查询错误日志表成功").put("data",  reportErrorVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        ReportErrorEntity reportError = reportErrorService.selectById(id);

        return R.ok().put("reportError", reportError);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    public R saveJson(@RequestBody ReportErrorEntity reportError){
    	ValidatorUtils.validateEntity(reportError);
        reportErrorService.insert(reportError);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    public R saveForm(ReportErrorEntity reportError){
    	ValidatorUtils.validateEntity(reportError);
        reportErrorService.insert(reportError);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    public R updateJson(@RequestBody ReportErrorEntity reportError){
        ValidatorUtils.validateEntity(reportError);
        reportErrorService.updateAllColumnById(reportError);//全部更新
        
        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    public R updateForm(ReportErrorEntity reportError){
        ValidatorUtils.validateEntity(reportError);
        reportErrorService.updateAllColumnById(reportError);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody String[] ids){
        reportErrorService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
