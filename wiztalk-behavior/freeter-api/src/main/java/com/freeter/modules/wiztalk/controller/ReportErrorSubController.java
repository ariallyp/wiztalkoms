package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.freeter.common.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.modules.wiztalk.entity.model.ReportErrorSubModel;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorSubVO;
import com.freeter.common.utils.MPUtil;


import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;
import com.freeter.modules.wiztalk.service.ReportErrorSubService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;




/**
 * 错误日志详情表
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:56
 */
@RestController
@RequestMapping("mobile/reportErrorSub")

public class ReportErrorSubController {
    @Autowired
    private ReportErrorSubService reportErrorSubService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    public R page(@RequestParam Map<String, Object> params,ReportErrorSubModel reportErrorSubModel){
 
        EntityWrapper< ReportErrorSubEntity> ew = new EntityWrapper< ReportErrorSubEntity>();
        ReportErrorSubEntity reportErrorSub = new  ReportErrorSubEntity( reportErrorSubModel);
     	ew.allEq(MPUtil.allEQMapPre( reportErrorSub, "reportErrorSub")); 
    	PageUtils page = reportErrorSubService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    public R list(ReportErrorSubModel reportErrorSubModel){
		ValidatorUtils.validateEntity(reportErrorSubModel);
        EntityWrapper< ReportErrorSubEntity> ew = new EntityWrapper< ReportErrorSubEntity>();
		ReportErrorSubEntity reportErrorSub = new  ReportErrorSubEntity( reportErrorSubModel);
     	ew.allEq(MPUtil.allEQMapPre( reportErrorSub, "reportErrorSub")); 
		List<ReportErrorSubVO>  reportErrorSubVOList =  reportErrorSubService.selectListVO(ew);
		return R.ok("查询错误日志详情表成功").put("data", reportErrorSubVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    public R query(ReportErrorSubModel reportErrorSubModel){
		ValidatorUtils.validateEntity(reportErrorSubModel);
        EntityWrapper< ReportErrorSubEntity> ew = new EntityWrapper< ReportErrorSubEntity>();
		ReportErrorSubEntity reportErrorSub = new  ReportErrorSubEntity( reportErrorSubModel);
		ew.allEq(MPUtil.allEQMapPre( reportErrorSub, "reportErrorSub")); 
		ReportErrorSubVO  reportErrorSubVO =  reportErrorSubService.selectVO(ew);
		return R.ok("查询错误日志详情表成功").put("data",  reportErrorSubVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        ReportErrorSubEntity reportErrorSub = reportErrorSubService.selectById(id);

        return R.ok().put("reportErrorSub", reportErrorSub);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    public R saveJson(@RequestBody ReportErrorSubEntity reportErrorSub){
    	ValidatorUtils.validateEntity(reportErrorSub);
        reportErrorSubService.insert(reportErrorSub);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    public R saveForm(ReportErrorSubEntity reportErrorSub){
    	ValidatorUtils.validateEntity(reportErrorSub);
        reportErrorSubService.insert(reportErrorSub);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    public R updateJson(@RequestBody ReportErrorSubEntity reportErrorSub){
        ValidatorUtils.validateEntity(reportErrorSub);
        reportErrorSubService.updateAllColumnById(reportErrorSub);//全部更新
        
        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    public R updateForm(ReportErrorSubEntity reportErrorSub){
        ValidatorUtils.validateEntity(reportErrorSub);
        reportErrorSubService.updateAllColumnById(reportErrorSub);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody String[] ids){
        reportErrorSubService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
