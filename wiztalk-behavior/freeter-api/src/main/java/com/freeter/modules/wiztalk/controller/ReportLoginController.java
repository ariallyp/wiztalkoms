package com.freeter.modules.wiztalk.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.MPUtil;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.ReportLoginEntity;
import com.freeter.modules.wiztalk.entity.model.ReportLoginModel;
import com.freeter.modules.wiztalk.entity.vo.ReportLoginVO;
import com.freeter.modules.wiztalk.service.ReportLoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 登录上报。
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 17:06:02
 */
@RestController
@RequestMapping("reportlogin")

public class ReportLoginController {
    @Autowired
    private ReportLoginService reportLoginService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    public R page(@RequestParam Map<String, Object> params, ReportLoginModel reportLoginModel){
 
        EntityWrapper<ReportLoginEntity> ew = new EntityWrapper<ReportLoginEntity>();
        ReportLoginEntity reportLogin = new ReportLoginEntity( reportLoginModel);
     	ew.allEq(MPUtil.allEQMapPre( reportLogin, "reportLogin"));
    	PageUtils page = reportLoginService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    public R list(ReportLoginModel reportLoginModel){
		ValidatorUtils.validateEntity(reportLoginModel);
        EntityWrapper<ReportLoginEntity> ew = new EntityWrapper<ReportLoginEntity>();
		ReportLoginEntity reportLogin = new ReportLoginEntity( reportLoginModel);
     	ew.allEq(MPUtil.allEQMapPre( reportLogin, "reportLogin"));
		List<ReportLoginVO>  reportLoginVOList =  reportLoginService.selectListVO(ew);
		return R.ok("登录上报。成功").put("data", reportLoginVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    public R query(ReportLoginModel reportLoginModel){
		ValidatorUtils.validateEntity(reportLoginModel);
        EntityWrapper<ReportLoginEntity> ew = new EntityWrapper<ReportLoginEntity>();
		ReportLoginEntity reportLogin = new ReportLoginEntity( reportLoginModel);
		ew.allEq(MPUtil.allEQMapPre( reportLogin, "reportLogin"));
		ReportLoginVO reportLoginVO =  reportLoginService.selectVO(ew);
		return R.ok("登录上报。成功").put("data",  reportLoginVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        ReportLoginEntity reportLogin = reportLoginService.selectById(id);

        return R.ok().put("reportLogin", reportLogin);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    public R saveJson(@RequestBody ReportLoginEntity reportLogin){
    	ValidatorUtils.validateEntity(reportLogin);
        reportLoginService.insert(reportLogin);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    public R saveForm(ReportLoginEntity reportLogin){
    	ValidatorUtils.validateEntity(reportLogin);
        reportLoginService.insert(reportLogin);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    public R updateJson(@RequestBody ReportLoginEntity reportLogin){
        ValidatorUtils.validateEntity(reportLogin);
        reportLoginService.updateAllColumnById(reportLogin);//全部更新
        
        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    public R updateForm(ReportLoginEntity reportLogin){
        ValidatorUtils.validateEntity(reportLogin);
        reportLoginService.updateAllColumnById(reportLogin);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody String[] ids){
        reportLoginService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
