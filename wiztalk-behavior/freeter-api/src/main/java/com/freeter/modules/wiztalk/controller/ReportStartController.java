package com.freeter.modules.wiztalk.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.common.utils.MPUtil;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import com.freeter.common.validator.ValidatorUtils;
import com.freeter.modules.wiztalk.entity.ReportStartEntity;
import com.freeter.modules.wiztalk.entity.model.ReportStartModel;
import com.freeter.modules.wiztalk.entity.vo.ReportStartVO;
import com.freeter.modules.wiztalk.service.ReportStartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 启动start表
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 17:44:22
 */
@RestController
@RequestMapping("reportstart")

public class ReportStartController {
    @Autowired
    private ReportStartService reportStartService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    public R page(@RequestParam Map<String, Object> params, ReportStartModel reportStartModel){
 
        EntityWrapper< ReportStartEntity> ew = new EntityWrapper< ReportStartEntity>();
        ReportStartEntity reportStart = new  ReportStartEntity( reportStartModel);
     	ew.allEq(MPUtil.allEQMapPre( reportStart, "reportStart"));
    	PageUtils page = reportStartService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    public R list(ReportStartModel reportStartModel){
		ValidatorUtils.validateEntity(reportStartModel);
        EntityWrapper< ReportStartEntity> ew = new EntityWrapper< ReportStartEntity>();
		ReportStartEntity reportStart = new  ReportStartEntity( reportStartModel);
     	ew.allEq(MPUtil.allEQMapPre( reportStart, "reportStart"));
		List<ReportStartVO>  reportStartVOList =  reportStartService.selectListVO(ew);
		return R.ok("查询启动start表成功").put("data", reportStartVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    public R query(ReportStartModel reportStartModel){
		ValidatorUtils.validateEntity(reportStartModel);
        EntityWrapper< ReportStartEntity> ew = new EntityWrapper< ReportStartEntity>();
		ReportStartEntity reportStart = new  ReportStartEntity( reportStartModel);
		ew.allEq(MPUtil.allEQMapPre( reportStart, "reportStart"));
		ReportStartVO reportStartVO =  reportStartService.selectVO(ew);
		return R.ok("查询启动start表成功").put("data",  reportStartVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        ReportStartEntity reportStart = reportStartService.selectById(id);

        return R.ok().put("reportStart", reportStart);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    public R saveJson(@RequestBody ReportStartEntity reportStart){
    	ValidatorUtils.validateEntity(reportStart);
        reportStartService.insert(reportStart);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    public R saveForm(ReportStartEntity reportStart){
    	ValidatorUtils.validateEntity(reportStart);
        reportStartService.insert(reportStart);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    public R updateJson(@RequestBody ReportStartEntity reportStart){
        ValidatorUtils.validateEntity(reportStart);
        reportStartService.updateAllColumnById(reportStart);//全部更新
        
        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    public R updateForm(ReportStartEntity reportStart){
        ValidatorUtils.validateEntity(reportStart);
        reportStartService.updateAllColumnById(reportStart);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody String[] ids){
        reportStartService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
