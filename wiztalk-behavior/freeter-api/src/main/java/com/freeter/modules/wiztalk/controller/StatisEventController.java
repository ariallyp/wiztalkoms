package com.freeter.modules.wiztalk.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.freeter.common.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.freeter.modules.wiztalk.entity.model.StatisEventModel;
import com.freeter.modules.wiztalk.entity.vo.StatisEventVO;
import com.freeter.common.utils.MPUtil;


import com.freeter.modules.wiztalk.entity.StatisEventEntity;
import com.freeter.modules.wiztalk.service.StatisEventService;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;




/**
 * 自定义事件的统计结果表
 *api接口
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 11:48:55
 */
@RestController
@RequestMapping("statisevent")

public class StatisEventController {
    @Autowired
    private StatisEventService statisEventService;
 
	 /**
     * 列表
     */
    @GetMapping("/page")
    public R page(@RequestParam Map<String, Object> params,StatisEventModel statisEventModel){
 
        EntityWrapper< StatisEventEntity> ew = new EntityWrapper< StatisEventEntity>();
        StatisEventEntity statisEvent = new  StatisEventEntity( statisEventModel);
     	ew.allEq(MPUtil.allEQMapPre( statisEvent, "statisEvent")); 
    	PageUtils page = statisEventService.queryPage(params, ew);
        return R.ok().put("data",  page.getList());
        
    }
	
	
	
    /**
     * 查询
     */
    @GetMapping("/list")
    public R list(StatisEventModel statisEventModel){
		ValidatorUtils.validateEntity(statisEventModel);
        EntityWrapper< StatisEventEntity> ew = new EntityWrapper< StatisEventEntity>();
		StatisEventEntity statisEvent = new  StatisEventEntity( statisEventModel);
     	ew.allEq(MPUtil.allEQMapPre( statisEvent, "statisEvent")); 
		List<StatisEventVO>  statisEventVOList =  statisEventService.selectListVO(ew);
		return R.ok("查询自定义事件的统计结果表成功").put("data", statisEventVOList);
    }

	 /**
     * 查询
     */
    @GetMapping("/query")
    public R query(StatisEventModel statisEventModel){
		ValidatorUtils.validateEntity(statisEventModel);
        EntityWrapper< StatisEventEntity> ew = new EntityWrapper< StatisEventEntity>();
		StatisEventEntity statisEvent = new  StatisEventEntity( statisEventModel);
		ew.allEq(MPUtil.allEQMapPre( statisEvent, "statisEvent")); 
		StatisEventVO  statisEventVO =  statisEventService.selectVO(ew);
		return R.ok("查询自定义事件的统计结果表成功").put("data",  statisEventVO);
    }
	

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
        StatisEventEntity statisEvent = statisEventService.selectById(id);

        return R.ok().put("statisEvent", statisEvent);
    }

    /**
     * 保存
     */
    @PostMapping("/save/json")
    public R saveJson(@RequestBody StatisEventEntity statisEvent){
    	ValidatorUtils.validateEntity(statisEvent);
        statisEventService.insert(statisEvent);
        return R.ok();
    }
    
    /**
     * 保存
     */
    @PostMapping("/save/form")
    public R saveForm(StatisEventEntity statisEvent){
    	ValidatorUtils.validateEntity(statisEvent);
        statisEventService.insert(statisEvent);

        return R.ok();
    }

    /**
     * 修改（参数：json）
     */
    @PostMapping("/update/json")
    public R updateJson(@RequestBody StatisEventEntity statisEvent){
        ValidatorUtils.validateEntity(statisEvent);
        statisEventService.updateAllColumnById(statisEvent);//全部更新
        
        return R.ok();
    }


    /**
     * 修改（参数：传统表单）
     */
    @PostMapping("/update/form")
    public R updateForm(StatisEventEntity statisEvent){
        ValidatorUtils.validateEntity(statisEvent);
        statisEventService.updateAllColumnById(statisEvent);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
        statisEventService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
