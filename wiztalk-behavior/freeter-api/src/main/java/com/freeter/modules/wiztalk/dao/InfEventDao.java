package com.freeter.modules.wiztalk.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.freeter.modules.wiztalk.entity.InfEventEntity;
import com.freeter.modules.wiztalk.entity.view.InfEventView;
import com.freeter.modules.wiztalk.entity.vo.InfEventVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 自定义事件表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 10:03:21
 */
public interface InfEventDao extends BaseMapper<InfEventEntity> {
	
	List<InfEventVO> selectListVO(@Param("ew") Wrapper<InfEventEntity> wrapper);
	
	InfEventVO selectVO(@Param("ew") Wrapper<InfEventEntity> wrapper);
	
	
	List<InfEventView> selectListView(@Param("ew") Wrapper<InfEventEntity> wrapper);

	List<InfEventView> selectListView(Pagination page,@Param("ew") Wrapper<InfEventEntity> wrapper);
	
	InfEventView selectView(@Param("ew") Wrapper<InfEventEntity> wrapper);

    List<InfEventEntity> selectListEntity();
}
