package com.freeter.modules.wiztalk.dao;

import com.freeter.modules.wiztalk.entity.ReportErrorEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorVO;
import com.freeter.modules.wiztalk.entity.view.ReportErrorView;


/**
 * 错误日志表
 * 
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:48
 */
public interface ReportErrorDao extends BaseMapper<ReportErrorEntity> {
	
	List<ReportErrorVO> selectListVO(@Param("ew") Wrapper<ReportErrorEntity> wrapper);
	
	ReportErrorVO selectVO(@Param("ew") Wrapper<ReportErrorEntity> wrapper);
	
	
	List<ReportErrorView> selectListView(@Param("ew") Wrapper<ReportErrorEntity> wrapper);

	List<ReportErrorView> selectListView(Pagination page,@Param("ew") Wrapper<ReportErrorEntity> wrapper);
	
	ReportErrorView selectView(@Param("ew") Wrapper<ReportErrorEntity> wrapper);
}
