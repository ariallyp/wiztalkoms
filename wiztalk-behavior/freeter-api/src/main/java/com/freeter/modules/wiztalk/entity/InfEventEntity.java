package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;



/**
 * 自定义事件表
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 10:03:21
 */
@TableName("wztk_inf_event")
@ApiModel(value = "InfEvent")
public class InfEventEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public InfEventEntity() {
		
	}
	
	public InfEventEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 事件id
	 */
	
	@TableId 					
	@ApiModelProperty(value = "事件id",hidden = true)
	private String id;
	
	/**
	 * 事件名称
	 */
						
	@ApiModelProperty(value = "事件名称")
	private String eventName;
	
	/**
	 * 事件别名
	 */
						
	@ApiModelProperty(value = "事件别名")
	private String alias;
	
	/**
	 * 产品编号
	 */
						
	@ApiModelProperty(value = "产品编号")
	private String appId;
	
	/**
	 * 创建时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 			
	@TableField(fill = FieldFill.INSERT) 
	@ApiModelProperty(value = "创建时间",hidden = true)
	private Date createTime;
	
	/**
	 * 设置：事件id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：事件id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：事件名称
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * 获取：事件名称
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * 设置：事件别名
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * 获取：事件别名
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * 设置：产品编号
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
