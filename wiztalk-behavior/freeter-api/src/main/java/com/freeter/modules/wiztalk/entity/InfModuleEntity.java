package com.freeter.modules.wiztalk.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;



/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 * 数据库通用操作实体类（普通增删改查）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-12 18:06:44
 */
@TableName("wztk_inf_module")
@ApiModel(value = "InfModule")
public class InfModuleEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public InfModuleEntity() {
		
	}
	
	public InfModuleEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	
	@TableId 					
	@ApiModelProperty(value = "",hidden = true)
	private String id;
	
	/**
	 * 页面对应的类名
	 */
						
	@ApiModelProperty(value = "页面对应的类名")
	private String moduleName;
	
	/**
	 * 页面的中文名称
	 */
						
	@ApiModelProperty(value = "页面的中文名称")
	private String alias;
	
	/**
	 * 创建时间
	 */
					
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 			
	@TableField(fill = FieldFill.INSERT) 
	@ApiModelProperty(value = "创建时间",hidden = true)
	private Date createTime;
	
	/**
	 * 产品id
	 */
						
	@ApiModelProperty(value = "产品id")
	private String appId;
	
	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：页面对应的类名
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	/**
	 * 获取：页面对应的类名
	 */
	public String getModuleName() {
		return moduleName;
	}
	/**
	 * 设置：页面的中文名称
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * 获取：页面的中文名称
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：产品id
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
}
