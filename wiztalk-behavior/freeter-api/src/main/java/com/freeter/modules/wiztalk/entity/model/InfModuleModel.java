package com.freeter.modules.wiztalk.entity.model;

import com.freeter.modules.wiztalk.entity.InfModuleEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-12 18:06:44
 */
@ApiModel(value = "InfModuleModel")
public class InfModuleModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 页面对应的类名
	 */
	
	@ApiModelProperty(value = "页面对应的类名") 
	private String moduleName;
		
	/**
	 * 页面的中文名称
	 */
	
	@ApiModelProperty(value = "页面的中文名称") 
	private String alias;
			
	/**
	 * 产品id
	 */
	
	@ApiModelProperty(value = "产品id") 
	private String appId;
				
	
	/**
	 * 设置：页面对应的类名
	 */
	 
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	/**
	 * 获取：页面对应的类名
	 */
	public String getModuleName() {
		return moduleName;
	}
				
	
	/**
	 * 设置：页面的中文名称
	 */
	 
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	/**
	 * 获取：页面的中文名称
	 */
	public String getAlias() {
		return alias;
	}
						
	
	/**
	 * 设置：产品id
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
			
}
