package com.freeter.modules.wiztalk.entity.model;

public class ReportCrashModel {

    public String clientData;
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getClientData() {
        return clientData;
    }

    public void setClientData(String clientData) {
        this.clientData = clientData;
    }



}
