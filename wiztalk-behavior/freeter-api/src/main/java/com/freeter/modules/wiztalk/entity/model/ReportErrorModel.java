package com.freeter.modules.wiztalk.entity.model;

import com.freeter.modules.wiztalk.entity.ReportErrorEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 错误日志表
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:48
 */
@ApiModel(value = "ReportErrorModel")
public class ReportErrorModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 手机型号
	 */
	
	@ApiModelProperty(value = "手机型号") 
	private String model;
		
	/**
	 * 与手机相关的参数
	 */
	
	@ApiModelProperty(value = "与手机相关的参数") 
	private String mac;
		
	/**
	 * 与手机相关的参数
	 */
	
	@ApiModelProperty(value = "与手机相关的参数") 
	private String imsi;
		
	/**
	 * 与手机相关的参数
	 */
	
	@ApiModelProperty(value = "与手机相关的参数") 
	private String imei;
		
	/**
	 * 设备名称
	 */
	
	@ApiModelProperty(value = "设备名称") 
	private String deviceName;
		
	/**
	 * 设备ID
	 */
	
	@ApiModelProperty(value = "设备ID") 
	private String deviceId;
		
	/**
	 * 手机系统
	 */
	
	@ApiModelProperty(value = "手机系统") 
	private String os;
		
	/**
	 * 手机品牌
	 */
	
	@ApiModelProperty(value = "手机品牌") 
	private String vendor;
		
	/**
	 * 用户ID
	 */
	
	@ApiModelProperty(value = "用户ID") 
	private String uid;
		
	/**
	 * 网络类型
	 */
	
	@ApiModelProperty(value = "网络类型") 
	private String netType;
		
	/**
	 * 产品id
	 */
	
	@ApiModelProperty(value = "产品id") 
	private String appId;
		
	/**
	 * 产品版本号
	 */
	
	@ApiModelProperty(value = "产品版本号") 
	private String appVersion;
		
	/**
	 * 上报错误的类型 Exception的类型名称
	 */
	
	@ApiModelProperty(value = "上报错误的类型 Exception的类型名称") 
	private String type;
		
	/**
	 * 上报时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "上报时间") 
	private Date time;
		
	/**
	 * 到达跟踪分析平台的时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	@ApiModelProperty(value = "到达跟踪分析平台的时间") 
	private Date reachTime;
		
	/**
	 * 手机的ip地址
	 */
	
	@ApiModelProperty(value = "手机的ip地址") 
	private String ip;
				
	
	/**
	 * 设置：手机型号
	 */
	 
	public void setModel(String model) {
		this.model = model;
	}
	
	/**
	 * 获取：手机型号
	 */
	public String getModel() {
		return model;
	}
				
	
	/**
	 * 设置：与手机相关的参数
	 */
	 
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	/**
	 * 获取：与手机相关的参数
	 */
	public String getMac() {
		return mac;
	}
				
	
	/**
	 * 设置：与手机相关的参数
	 */
	 
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	
	/**
	 * 获取：与手机相关的参数
	 */
	public String getImsi() {
		return imsi;
	}
				
	
	/**
	 * 设置：与手机相关的参数
	 */
	 
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	/**
	 * 获取：与手机相关的参数
	 */
	public String getImei() {
		return imei;
	}
				
	
	/**
	 * 设置：设备名称
	 */
	 
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	/**
	 * 获取：设备名称
	 */
	public String getDeviceName() {
		return deviceName;
	}
				
	
	/**
	 * 设置：设备ID
	 */
	 
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	/**
	 * 获取：设备ID
	 */
	public String getDeviceId() {
		return deviceId;
	}
				
	
	/**
	 * 设置：手机系统
	 */
	 
	public void setOs(String os) {
		this.os = os;
	}
	
	/**
	 * 获取：手机系统
	 */
	public String getOs() {
		return os;
	}
				
	
	/**
	 * 设置：手机品牌
	 */
	 
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	/**
	 * 获取：手机品牌
	 */
	public String getVendor() {
		return vendor;
	}
				
	
	/**
	 * 设置：用户ID
	 */
	 
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	/**
	 * 获取：用户ID
	 */
	public String getUid() {
		return uid;
	}
				
	
	/**
	 * 设置：网络类型
	 */
	 
	public void setNetType(String netType) {
		this.netType = netType;
	}
	
	/**
	 * 获取：网络类型
	 */
	public String getNetType() {
		return netType;
	}
				
	
	/**
	 * 设置：产品id
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品id
	 */
	public String getAppId() {
		return appId;
	}
				
	
	/**
	 * 设置：产品版本号
	 */
	 
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	/**
	 * 获取：产品版本号
	 */
	public String getAppVersion() {
		return appVersion;
	}
				
	
	/**
	 * 设置：上报错误的类型 Exception的类型名称
	 */
	 
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * 获取：上报错误的类型 Exception的类型名称
	 */
	public String getType() {
		return type;
	}
				
	
	/**
	 * 设置：上报时间
	 */
	 
	public void setTime(Date time) {
		this.time = time;
	}
	
	/**
	 * 获取：上报时间
	 */
	public Date getTime() {
		return time;
	}
				
	
	/**
	 * 设置：到达跟踪分析平台的时间
	 */
	 
	public void setReachTime(Date reachTime) {
		this.reachTime = reachTime;
	}
	
	/**
	 * 获取：到达跟踪分析平台的时间
	 */
	public Date getReachTime() {
		return reachTime;
	}
				
	
	/**
	 * 设置：手机的ip地址
	 */
	 
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	/**
	 * 获取：手机的ip地址
	 */
	public String getIp() {
		return ip;
	}
			
}
