package com.freeter.modules.wiztalk.entity.model;

import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 错误日志详情表
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:56
 */
@ApiModel(value = "ReportErrorSubModel")
public class ReportErrorSubModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 错误内容
	 */
	
	@ApiModelProperty(value = "错误内容") 
	private String text;
				
	
	/**
	 * 设置：错误内容
	 */
	 
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * 获取：错误内容
	 */
	public String getText() {
		return text;
	}
			
}
