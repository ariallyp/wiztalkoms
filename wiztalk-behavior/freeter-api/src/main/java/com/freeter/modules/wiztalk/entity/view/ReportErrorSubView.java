package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 错误日志详情表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:56
 */
@TableName("wztk_report_error_sub")
@ApiModel(value = "ReportErrorSub")
public class ReportErrorSubView  extends ReportErrorSubEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ReportErrorSubView(){
	}
 
 	public ReportErrorSubView(ReportErrorSubEntity reportErrorSubEntity){
 	try {
			BeanUtils.copyProperties(this, reportErrorSubEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
