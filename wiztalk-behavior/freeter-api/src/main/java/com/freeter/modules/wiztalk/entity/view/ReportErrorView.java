package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.ReportErrorEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 错误日志表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:48
 */
@TableName("wztk_report_error")
@ApiModel(value = "ReportError")
public class ReportErrorView  extends ReportErrorEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ReportErrorView(){
	}
 
 	public ReportErrorView(ReportErrorEntity reportErrorEntity){
 	try {
			BeanUtils.copyProperties(this, reportErrorEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
