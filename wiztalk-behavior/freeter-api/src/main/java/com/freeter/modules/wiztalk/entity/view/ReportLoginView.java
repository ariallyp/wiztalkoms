package com.freeter.modules.wiztalk.entity.view;

import com.baomidou.mybatisplus.annotations.TableName;
import com.freeter.modules.wiztalk.entity.ReportLoginEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;


/**
 * 登录上报信息实时表，只存放昨天和今天两天的数据，零点的时候清除前天的数据，并移存到对应的历史表中。
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 17:06:02
 */
@TableName("wztk_report_login")
@ApiModel(value = "ReportLogin")
public class ReportLoginView  extends ReportLoginEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ReportLoginView(){
	}
 
 	public ReportLoginView(ReportLoginEntity reportLoginEntity){
 	try {
			BeanUtils.copyProperties(this, reportLoginEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
