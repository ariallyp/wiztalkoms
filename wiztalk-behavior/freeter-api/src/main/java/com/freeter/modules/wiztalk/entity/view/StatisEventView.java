package com.freeter.modules.wiztalk.entity.view;

import com.freeter.modules.wiztalk.entity.StatisEventEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 自定义事件的统计结果表
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 11:48:55
 */
@TableName("wztk_statis_event")
@ApiModel(value = "StatisEvent")
public class StatisEventView  extends StatisEventEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public StatisEventView(){
	}
 
 	public StatisEventView(StatisEventEntity statisEventEntity){
 	try {
			BeanUtils.copyProperties(this, statisEventEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
