package com.freeter.modules.wiztalk.entity.vo;

import com.freeter.modules.wiztalk.entity.InfEventEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
 

/**
 * 自定义事件表
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 10:03:21
 */
@ApiModel(value = "InfEventVO")
public class InfEventVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 事件名称
	 */
	
	@ApiModelProperty(value = "事件名称") 
	private String eventName;
		
	/**
	 * 事件别名
	 */
	
	@ApiModelProperty(value = "事件别名") 
	private String alias;
		
	/**
	 * 产品编号
	 */
	
	@ApiModelProperty(value = "产品编号") 
	private String appId;
					
	
	/**
	 * 设置：事件名称
	 */
	 
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	/**
	 * 获取：事件名称
	 */
	public String getEventName() {
		return eventName;
	}
				
	
	/**
	 * 设置：事件别名
	 */
	 
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	/**
	 * 获取：事件别名
	 */
	public String getAlias() {
		return alias;
	}
				
	
	/**
	 * 设置：产品编号
	 */
	 
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/**
	 * 获取：产品编号
	 */
	public String getAppId() {
		return appId;
	}
					
}
