package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.InfEventEntity;
import com.freeter.modules.wiztalk.entity.view.InfEventView;
import com.freeter.modules.wiztalk.entity.vo.InfEventVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 自定义事件表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-13 10:03:21
 */
public interface InfEventService extends IService<InfEventEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<InfEventVO> selectListVO(Wrapper<InfEventEntity> wrapper);
   	
   	InfEventVO selectVO(@Param("ew") Wrapper<InfEventEntity> wrapper);
   	
   	List<InfEventView> selectListView(Wrapper<InfEventEntity> wrapper);
   	
   	InfEventView selectView(@Param("ew") Wrapper<InfEventEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<InfEventEntity> wrapper);

    List<InfEventEntity> selectListEntity();
}

