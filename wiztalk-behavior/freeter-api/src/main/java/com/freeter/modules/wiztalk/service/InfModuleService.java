package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import com.freeter.modules.wiztalk.entity.view.InfModuleView;
import com.freeter.modules.wiztalk.entity.vo.InfModuleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 页面表，存储页面汉字名称，英文名称，对应的appId等信息。
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-12 18:06:44
 */
public interface InfModuleService extends IService<InfModuleEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<InfModuleVO> selectListVO(Wrapper<InfModuleEntity> wrapper);
   	
   	InfModuleVO selectVO(@Param("ew") Wrapper<InfModuleEntity> wrapper);
   	
   	List<InfModuleView> selectListView(Wrapper<InfModuleEntity> wrapper);
   	
   	InfModuleView selectView(@Param("ew") Wrapper<InfModuleEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<InfModuleEntity> wrapper);

    List<InfModuleEntity> selectListEntity();
}

