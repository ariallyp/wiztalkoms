package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.ReportErrorEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.ReportErrorView;


/**
 * 错误日志表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:48
 */
public interface ReportErrorService extends IService<ReportErrorEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ReportErrorVO> selectListVO(Wrapper<ReportErrorEntity> wrapper);
   	
   	ReportErrorVO selectVO(@Param("ew") Wrapper<ReportErrorEntity> wrapper);
   	
   	List<ReportErrorView> selectListView(Wrapper<ReportErrorEntity> wrapper);
   	
   	ReportErrorView selectView(@Param("ew") Wrapper<ReportErrorEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<ReportErrorEntity> wrapper);
}

