package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;
import java.util.List;
import java.util.Map;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorSubVO;
import org.apache.ibatis.annotations.Param;
import com.freeter.modules.wiztalk.entity.view.ReportErrorSubView;


/**
 * 错误日志详情表
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-19 16:06:56
 */
public interface ReportErrorSubService extends IService<ReportErrorSubEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ReportErrorSubVO> selectListVO(Wrapper<ReportErrorSubEntity> wrapper);
   	
   	ReportErrorSubVO selectVO(@Param("ew") Wrapper<ReportErrorSubEntity> wrapper);
   	
   	List<ReportErrorSubView> selectListView(Wrapper<ReportErrorSubEntity> wrapper);
   	
   	ReportErrorSubView selectView(@Param("ew") Wrapper<ReportErrorSubEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<ReportErrorSubEntity> wrapper);
}

