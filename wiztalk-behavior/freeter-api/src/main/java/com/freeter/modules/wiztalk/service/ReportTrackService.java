package com.freeter.modules.wiztalk.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.freeter.common.utils.PageUtils;
import com.freeter.modules.wiztalk.entity.ReportTrackEntity;
import com.freeter.modules.wiztalk.entity.view.ReportTrackView;
import com.freeter.modules.wiztalk.entity.vo.ReportTrackVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 埋点上报信息和自定义事件上报信息实时表,以type的字段来区分是埋点信息还是自定义事件信息。
 * 也只放昨天和今天的信息，零点清理前台的信息，并移存到对应的历史表。
 *
 * @author Arial
 * @email Arial@foxmail.com
 * @date 2018-09-10 17:06:02
 */
public interface ReportTrackService extends IService<ReportTrackEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ReportTrackVO> selectListVO(Wrapper<ReportTrackEntity> wrapper);
   	
   	ReportTrackVO selectVO(@Param("ew") Wrapper<ReportTrackEntity> wrapper);
   	
   	List<ReportTrackView> selectListView(Wrapper<ReportTrackEntity> wrapper);
   	
   	ReportTrackView selectView(@Param("ew") Wrapper<ReportTrackEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params, Wrapper<ReportTrackEntity> wrapper);


    void batchInsert(List<ReportTrackEntity> entityList);
}

