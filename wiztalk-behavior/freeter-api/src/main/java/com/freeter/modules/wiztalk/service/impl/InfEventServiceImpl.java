package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.InfEventDao;
import com.freeter.modules.wiztalk.entity.InfEventEntity;
import com.freeter.modules.wiztalk.entity.view.InfEventView;
import com.freeter.modules.wiztalk.entity.vo.InfEventVO;
import com.freeter.modules.wiztalk.service.InfEventService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("infEventService")
public class InfEventServiceImpl extends ServiceImpl<InfEventDao, InfEventEntity> implements InfEventService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<InfEventEntity> page = this.selectPage(
                new Query<InfEventEntity>(params).getPage(),
                new EntityWrapper<InfEventEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Wrapper<InfEventEntity> wrapper) {
        Page<InfEventView> page = new Query<InfEventView>(params).getPage();
        page.setRecords(baseMapper.selectListView(page, wrapper));
        PageUtils pageUtil = new PageUtils(page);
        return pageUtil;

    }

    @Override
    public List<InfEventVO> selectListVO(Wrapper<InfEventEntity> wrapper) {
        return baseMapper.selectListVO(wrapper);
    }

    @Override
    public InfEventVO selectVO(Wrapper<InfEventEntity> wrapper) {
        return baseMapper.selectVO(wrapper);
    }

    @Override
    public List<InfEventView> selectListView(Wrapper<InfEventEntity> wrapper) {
        return baseMapper.selectListView(wrapper);
    }

    @Override
    public InfEventView selectView(Wrapper<InfEventEntity> wrapper) {
        return baseMapper.selectView(wrapper);
    }


    @Override
    public List<InfEventEntity> selectListEntity() {return baseMapper.selectListEntity(); }

}
