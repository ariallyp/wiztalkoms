package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.InfModuleDao;
import com.freeter.modules.wiztalk.entity.InfModuleEntity;
import com.freeter.modules.wiztalk.entity.view.InfModuleView;
import com.freeter.modules.wiztalk.entity.vo.InfModuleVO;
import com.freeter.modules.wiztalk.service.InfModuleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("infModuleService")
public class InfModuleServiceImpl extends ServiceImpl<InfModuleDao, InfModuleEntity> implements InfModuleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<InfModuleEntity> page = this.selectPage(
                new Query<InfModuleEntity>(params).getPage(),
                new EntityWrapper<InfModuleEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<InfModuleEntity> wrapper) {
		  Page<InfModuleView> page =new Query<InfModuleView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<InfModuleVO> selectListVO( Wrapper<InfModuleEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public InfModuleVO selectVO( Wrapper<InfModuleEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<InfModuleView> selectListView(Wrapper<InfModuleEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public InfModuleView selectView(Wrapper<InfModuleEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

    @Override
	public List<InfModuleEntity>selectListEntity(){
        return baseMapper.selectListEntity();
    }

}
