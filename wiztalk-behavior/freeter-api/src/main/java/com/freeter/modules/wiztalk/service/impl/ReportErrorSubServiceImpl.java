package com.freeter.modules.wiztalk.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;

import com.freeter.modules.wiztalk.dao.ReportErrorSubDao;
import com.freeter.modules.wiztalk.entity.ReportErrorSubEntity;
import com.freeter.modules.wiztalk.service.ReportErrorSubService;
import com.freeter.modules.wiztalk.entity.vo.ReportErrorSubVO;
import com.freeter.modules.wiztalk.entity.view.ReportErrorSubView;


@Service("reportErrorSubService")
public class ReportErrorSubServiceImpl extends ServiceImpl<ReportErrorSubDao, ReportErrorSubEntity> implements ReportErrorSubService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ReportErrorSubEntity> page = this.selectPage(
                new Query<ReportErrorSubEntity>(params).getPage(),
                new EntityWrapper<ReportErrorSubEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ReportErrorSubEntity> wrapper) {
		  Page<ReportErrorSubView> page =new Query<ReportErrorSubView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<ReportErrorSubVO> selectListVO( Wrapper<ReportErrorSubEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ReportErrorSubVO selectVO( Wrapper<ReportErrorSubEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ReportErrorSubView> selectListView(Wrapper<ReportErrorSubEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ReportErrorSubView selectView(Wrapper<ReportErrorSubEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
