package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.ReportLoginDao;
import com.freeter.modules.wiztalk.entity.ReportLoginEntity;
import com.freeter.modules.wiztalk.entity.view.ReportLoginView;
import com.freeter.modules.wiztalk.entity.vo.ReportLoginVO;
import com.freeter.modules.wiztalk.service.ReportLoginService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("reportLoginService")
public class ReportLoginServiceImpl extends ServiceImpl<ReportLoginDao, ReportLoginEntity> implements ReportLoginService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ReportLoginEntity> page = this.selectPage(
                new Query<ReportLoginEntity>(params).getPage(),
                new EntityWrapper<ReportLoginEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ReportLoginEntity> wrapper) {
		  Page<ReportLoginView> page =new Query<ReportLoginView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<ReportLoginVO> selectListVO(Wrapper<ReportLoginEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ReportLoginVO selectVO(Wrapper<ReportLoginEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ReportLoginView> selectListView(Wrapper<ReportLoginEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ReportLoginView selectView(Wrapper<ReportLoginEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
