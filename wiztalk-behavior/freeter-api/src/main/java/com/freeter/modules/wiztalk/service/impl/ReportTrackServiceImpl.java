package com.freeter.modules.wiztalk.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.util.RedisTool;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;
import com.freeter.modules.wiztalk.dao.ReportTrackDao;
import com.freeter.modules.wiztalk.entity.ReportTrackEntity;
import com.freeter.modules.wiztalk.entity.view.ReportTrackView;
import com.freeter.modules.wiztalk.entity.vo.ReportTrackVO;
import com.freeter.modules.wiztalk.service.ReportTrackService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;


@Service("reportTrackService")
public class ReportTrackServiceImpl extends ServiceImpl<ReportTrackDao, ReportTrackEntity> implements ReportTrackService {
    @Autowired
    private RedisTool redisTool;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ReportTrackEntity> page = this.selectPage(
                new Query<ReportTrackEntity>(params).getPage(),
                new EntityWrapper<ReportTrackEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Wrapper<ReportTrackEntity> wrapper) {
        Page<ReportTrackView> page = new Query<ReportTrackView>(params).getPage();
        page.setRecords(baseMapper.selectListView(page, wrapper));
        PageUtils pageUtil = new PageUtils(page);
        return pageUtil;

    }

    @Override
    public List<ReportTrackVO> selectListVO(Wrapper<ReportTrackEntity> wrapper) {
        return baseMapper.selectListVO(wrapper);
    }

    @Override
    public ReportTrackVO selectVO(Wrapper<ReportTrackEntity> wrapper) {
        return baseMapper.selectVO(wrapper);
    }

    @Override
    public List<ReportTrackView> selectListView(Wrapper<ReportTrackEntity> wrapper) {
        return baseMapper.selectListView(wrapper);
    }

    @Override
    public ReportTrackView selectView(Wrapper<ReportTrackEntity> wrapper) {
        return baseMapper.selectView(wrapper);
    }

    /**
     * 批量插入埋点信息表
     *
     * @param entityList 前台传来的数据
     */
    @Override
    @Transactional
    public void batchInsert(List<ReportTrackEntity> entityList) {
        injectAttr(entityList);
        if(!entityList.isEmpty()){
            baseMapper.batchInsert(entityList, "wztk_report_track");
        }
    }

    /**
     * @param entityList 前台传来的数据
     */
    private void injectAttr(List<ReportTrackEntity> entityList) {
        for (ReportTrackEntity entity : entityList) {
            entity.setId(UUID.randomUUID().toString());
            if(StringUtils.isNotBlank(entity.getAppId())){
                if(StringUtils.isNotBlank(entity.getEventPath())){
                    entity.setModuleId(redisTool.getModuleId(entity.getAppId(),entity.getEventPath()));
                }
                if(StringUtils.isNotBlank(entity.getEventName())){
                    entity.setEventId(redisTool.getEventId(entity.getAppId(),entity.getEventName()));
                }
            }

        }
    }
}
