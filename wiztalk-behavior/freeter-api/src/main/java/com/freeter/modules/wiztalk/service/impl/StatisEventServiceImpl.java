package com.freeter.modules.wiztalk.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.freeter.common.utils.PageUtils;
import com.freeter.common.utils.Query;

import com.freeter.modules.wiztalk.dao.StatisEventDao;
import com.freeter.modules.wiztalk.entity.StatisEventEntity;
import com.freeter.modules.wiztalk.service.StatisEventService;
import com.freeter.modules.wiztalk.entity.vo.StatisEventVO;
import com.freeter.modules.wiztalk.entity.view.StatisEventView;


@Service("statisEventService")
public class StatisEventServiceImpl extends ServiceImpl<StatisEventDao, StatisEventEntity> implements StatisEventService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<StatisEventEntity> page = this.selectPage(
                new Query<StatisEventEntity>(params).getPage(),
                new EntityWrapper<StatisEventEntity>()
        );

        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<StatisEventEntity> wrapper) {
		  Page<StatisEventView> page =new Query<StatisEventView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;

 	}
    
    @Override
	public List<StatisEventVO> selectListVO( Wrapper<StatisEventEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public StatisEventVO selectVO( Wrapper<StatisEventEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<StatisEventView> selectListView(Wrapper<StatisEventEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public StatisEventView selectView(Wrapper<StatisEventEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

}
